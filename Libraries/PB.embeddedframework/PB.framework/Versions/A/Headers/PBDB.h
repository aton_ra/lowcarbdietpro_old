//
//  SQLiteDB.h
//  OS4
//
//  Created by Dmytro Mikheiev on 10/9/10.
//  Copyright 2010 POLAR-B. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SKDatabase.h"

@interface PBDB : NSObject {
	
	SKDatabase *_db;
	
}
@property (nonatomic, readonly) SKDatabase *db;

+ (PBDB *)instance;
/*
 * load db file
 */
- (void) chargeWithFileName:(NSString *) fileName;
/*
 * will remove old database from documents and load new one
 */
- (void) rechargeWithFileName:(NSString *) fileName;
@end