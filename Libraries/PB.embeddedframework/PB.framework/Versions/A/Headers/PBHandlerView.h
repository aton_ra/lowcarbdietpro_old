//
//  HandlerView.h
//   framework
//
//  Created by Dmitry Mikheev on 3/9/10.
//  Copyright 2010. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum{
	PBHandlerView_AnimationType_None = 0,
	PBHandlerView_AnimationType_PageUnCurl,
	PBHandlerView_AnimationType_SuckEffect,
	PBHandlerView_AnimationType_SpewEffect,
	PBHandlerView_AnimationType_CameraIris,
	PBHandlerView_AnimationType_CameraIrisHollowOpen,
	PBHandlerView_AnimationType_CameraIrisHollowClose,
	PBHandlerView_AnimationType_OglFlip,
	PBHandlerView_AnimationType_Flip,
	PBHandlerView_AnimationType_Fade,
	PBHandlerView_AnimationType_MoveIn,
	PBHandlerView_AnimationType_Push,
	PBHandlerView_AnimationType_Reveal,
	PBHandlerView_AnimationType_PageCurl,
    PBHandlerView_AnimationType_RippleEffect,
    //----- effects below may not work, requires dances with tambourine  ------
    PBHandlerView_AnimationType_GenieEffect,
	PBHandlerView_AnimationType_UnGenieEffect,
	PBHandlerView_AnimationType_Twist,
	PBHandlerView_AnimationType_Tubey,
	PBHandlerView_AnimationType_Swirl,
	PBHandlerView_AnimationType_CharminUltra,
	PBHandlerView_AnimationType_ZoomyIn,
	PBHandlerView_AnimationType_ZoomyOut
}PBHandlerView_AnimationType;

typedef enum{
	PBHandlerView_AnimationSubType_FromRight = 0,
	PBHandlerView_AnimationSubType_FromLeft = 1,
	PBHandlerView_AnimationSubType_FromTop = 2,
	PBHandlerView_AnimationSubType_FromBottom = 3
}PBHandlerView_AnimationSubType;

@class PBHandlerView;
@protocol PBHandlerViewDelegate
@optional
- (void) viewAnimationDidStart:(PBHandlerView *) sender;
- (void) viewAnimationDidFinish:(PBHandlerView *) sender;

@end

@interface PBHandlerView : UIView {
	UIView *_activeView;
	UIViewController *_currentViewController;
	NSMutableArray *_viewControllerStackMutableArray;
	double _animationDuration;
	id _delegate;
    id _userInfo;
}
@property(nonatomic, assign) double animationDuration;
@property(nonatomic, assign) id delegate;
@property(nonatomic,readonly) UIView *activeView;
@property(nonatomic, retain) id userInfo;

- (UIView*) getActiveView;

- (void) pushView:(UIView *) view animationType:(PBHandlerView_AnimationType) animationType subType:(PBHandlerView_AnimationSubType) subType;
- (void) pushView:(UIView *) view animationType:(PBHandlerView_AnimationType) animationType;
//- (void) pushView:(UIView *) view animation:(CATransition *) animation;

- (void) popView:(UIView *) view animationType:(PBHandlerView_AnimationType) animationType subType:(PBHandlerView_AnimationSubType) subType;
- (void) popView:(UIView *) view animationType:(PBHandlerView_AnimationType) animationType;
//- (void) popView:(UIView *) view animation:(CATransition *) animation;

@end
