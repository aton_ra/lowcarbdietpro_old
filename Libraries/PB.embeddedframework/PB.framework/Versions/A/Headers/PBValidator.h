//
//  PBValidator.h
//  POLAR-B framework
//
//  Created by Dmitry Mikheev on 4/23/10.
//  Copyright 2010 POLAR-B. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum{
	PBValidator_EmailValidation = 0, //mail@mail.com
	PBValidator_IntegerValidation = 1, // 32146
	PBValidator_FloatValidation = 2, // 1345.98765
	PBValidator_MoneyValidation = 3, // 1345.98
	PBValidator_ZipValidation = 4, // 5 digits
	PBValidator_PhoneValidation = 5, // xxx-xxx-xxxx
	PBValidator_PasswordValidation = 6, // 6 characters minimum
	PBValidator_URLValidation = 7 // starting with http://
} PBValidatorType;
@interface PBValidator : UITextField {
	
}
+ (BOOL) isValid:(NSString *)string
withValidationType:(PBValidatorType) validation;
+ (BOOL)isValid:(NSString *) string
	  withRegex:(NSString *) regex;
@end
