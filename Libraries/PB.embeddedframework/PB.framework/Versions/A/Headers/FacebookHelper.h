//
//  FacebookHelper.h
//  LSATProctor
//
//  Created by Dmitry Mikheev on 5/16/10.
//  Copyright 2010 POLAR-B. All rights reserved.
//
/*********** sample - how to use facebook attachments **************************
 
 NSDictionary *mediaItemDictionary = [NSMutableDictionary	dictionaryWithObjectsAndKeys:
 @"image", @"type",
 @"http://url of the image",@"src",
 @"http://url to redirect user if he clicks on image",@"href",
 nil];  						   
 NSMutableDictionary *attachmentDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSArray arrayWithObject:mediaItemDictionary],@"media", nil];
 
 [attachmentDictionary setValue:@"description" forKey:@"description"];
 [attachmentDictionary setValue:@"caption" forKey:@"caption"];
 [attachmentDictionary setValue:@"http://google.com?q=link_for_name" forKey:@"href"];
 [attachmentDictionary setValue:@"name of the post" forKey:@"name"];
 
 */

#import <Foundation/Foundation.h>
#import "Facebook.h"

typedef enum{
	FacebookHelper_Responce_Success = 0,
	FacebookHelper_Responce_Fail = 1,
	FacebookHelper_Responce_Canceled = 2,
	FacebookHelper_Responce_AlreadyShared = 3,
	FacebookHelper_Responce_UserConnected = 4,
	FacebookHelper_Responce_UserWillConnect = 5,
	FacebookHelper_Responce_DataWillPost = 6,
	FacebookHelper_Responce_UserDisconnected = 7
}FacebookHelper_Responce;

@protocol FacebookHelperDelegate
@required
- (void) responceWithStatus:(FacebookHelper_Responce)status;
@end

@interface FacebookHelper : NSObject <FBSessionDelegate, FBRequestDelegate> {
	Facebook *_facebookSession;
	
	NSString *_facebookPostMessage;
	NSDictionary *_attachmentDictionary;
	NSObject<FacebookHelperDelegate> *_facebookDelegate;
	NSArray *_facebookPermissionsArray;
	BOOL _facebookConnectedBool;
}
@property(nonatomic, retain) NSArray *facebookPermissionsArray;
@property(readonly) Facebook *facebookSession;

+ (FacebookHelper *)instance;


- (void) chargeFacebook;
- (BOOL) isFacebookConnected;
- (void) logoutFacebook;
- (void) logoutFacebook:(NSObject<FacebookHelperDelegate> *) delegate;
- (void) loginFacebook:(NSObject<FacebookHelperDelegate> *) delegate;
- (void) postFacebookMessage:(NSString *) message
					delegate:(id) delegate;
- (void)postFacebookMessage:(NSString *) message
				 attachment:(NSDictionary *) attachment
				   delegate:(NSObject<FacebookHelperDelegate> *) delegate;
- (void) _facebookPostData;
@end
