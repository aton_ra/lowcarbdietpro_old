//
//  VKHelper.h
//  VKapi
//
//  Created by Dmytro Mikheiev on 9/9/11.
//  Copyright 2011 POLAR-B. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VKLoginDialog.h"
#import "PBService.h"

typedef enum{
	VK_Responce_Success = 0,
	VK_Responce_Fail = 1,
	VK_Responce_Canceled = 2,
	VK_Responce_UserConnected = 3,
	VK_Responce_UserWillConnect = 4,
	VK_Responce_DataWillPost = 5
}VK_Responce;

typedef enum{
	VKRights_Messages = 1,
	VKRights_Friends = 2,
	VKRights_Photos = 4,
	VKRights_AudioRecords = 8,
	VKRights_VideRecords = 16,
	VKRights_Propositions = 32,
	VKRights_Questions = 64,
	VKRights_WiKiPages = 128,
	VKRights_AddLinkToLeftMenu = 256,
	VKRights_AddLinksToWall = 512,
	VKRights_UserStatus = 1024,
	VKRights_UserNotes = 2048,
	VKRights_ExtendedMessages = 4096,
	VKRights_ExtendedWall = 8192,
	VKRights_AdCabinet = 32768,
	VKRights_UserDocuments = 131072,
	VKRights_UserGroups = 262144
}VKRights;

@class VKHelper;
@protocol VKDelegate
@required
- (void) responceFromVKwithStatus:(VK_Responce)status data:(id)data;
@end

@interface VKHelper : NSObject <UIWebViewDelegate, VKDialogDelegate> {
	NSMutableArray *_rightsArray;
	UIWebView *_autoLoginWebView;
	PBService *_apiService;
	NSObject <VKDelegate> *_delegate;
	
	NSMutableDictionary *_postAttributesDictionary;
	VKLoginDialog *_loginPopup;
    NSDictionary *_lastRequestDictionary;
}
@property(nonatomic, readonly) NSString *userId;

+ (VKHelper *)instance;
/*
 * re-saves OAuth attributes (session key, session id... etc). required for internal use by login popup
 */
- (void)setOAuthAttributes:(NSDictionary *)attributes;
/*
 * creates login link. required for internal use by login popup
 */
- (NSString *)formLoginUrlString;
/*
 * performs autologin if user was logged in previously
 */
- (void)charge;
/*
 * add rights. by default we have only VKRights_Messages and VKRights_ExtendedWall
 */
- (void)addRights:(VKRights) right;
/*
 * check if session active
 */
- (BOOL)isLoggedIn;
/*
 * clear cookies and session attributes
 */
- (void)logout;
/*
 * login
 */
- (void)login:(NSObject <VKDelegate> *)delegate;
/*
 * post message to user's wall
 */
- (void)postVKMessage:(NSString *)message
			 delegate:(NSObject <VKDelegate> *)delegate;
/*
 * post message to user's wall with attributes like - image, link ...
 */
- (void)postVKMessage:(NSString *)message
		   attributes:(NSDictionary *)attributes
			 delegate:(NSObject <VKDelegate> *)delegate;
/*
 * call VK API method. with parameters dictionary
 */
- (void)call:(NSString *)apiMethod
    delegate:(NSObject <VKDelegate> *)delegate;
/*
 * call VK API method. with parameters dictionary
 */
- (void)call:(NSString *)apiMethod
withParameters:(NSDictionary *)parameters
    delegate:(NSObject <VKDelegate> *)delegate;
@end
