//
//  PBColorPickerView.h
//  Neat
//
//  Created by Dmytro Mikheiev on 7/25/12.
//  Copyright (c) 2012 POLAR-B. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PBColorPickerView;
@protocol PBColorPickerViewDelegate
@optional
- (void)colorDidChange:(UIColor *)color sender:(PBColorPickerView *)sender;
@end

@interface PBColorPickerView : UIView <UIGestureRecognizerDelegate>{
    NSObject<PBColorPickerViewDelegate> *_delegate;
	
	UIView *_circleHandlerView;
	UIView *_glassHandlerView;
	UIView *_coloredView;
	UIImageView *_glassBackgroundImageView;
	UIImageView *_circleFaceImageView;
    UIImageView *_coneImageView;
    
	NSData *_circlePixelData;
	UIImage *_circleImage;
	UIColor *_selectedColor;
	
	CGPoint _previousTranslation;
    NSString *_keyString;
}
/*
 * color picker always has a static size (228,228) so there is only way to init it using position point (left/top corner)
 */
- (id)initWithPosition:(CGPoint)position;
/*
 * picker delegate
 */
@property(nonatomic, assign) NSObject<PBColorPickerViewDelegate> *delegate;
/*
 * the key - used to remember previous selection of the color picker. 
 */
@property(nonatomic, assign) NSString *key;
/*
 * currently selected color
 */
@property(nonatomic, readonly) UIColor *selectedColor;
/*
 * circled image with size 240x240. May contain transparent pixels
 */
@property(nonatomic, assign) UIImage *colorCircle;
/*
 * set selected color by it's position
 */
- (void)setSelectedColorWithPoint:(CGPoint)location;
/*
 * set selected color in the color picker that is not yet created
 */
+ (void)setSelectedColorWithPoint:(CGPoint)location
                           forKey:(NSString *)key;
@end
