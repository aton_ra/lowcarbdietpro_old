//
//  PBAudioRecordManager.h
//  OS4
//
//  Created by Dmytro Mikheiev on 3/5/11.
//  Copyright 2011 POLAR-B. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreAudio/CoreAudioTypes.h>


@interface PBAudioRecorderManager : NSObject {
	NSMutableDictionary *_recordStackDictionary;
}
+ (PBAudioRecorderManager *)instance;
/*
 * create recorder session with file in bundle
 */
- (void)chargeRecorder:(NSString *) key;
- (void)unchargeRecorder;
- (void)unchargeRecorderWithPrefix:(NSString *) prefixKey;
- (void)unchargeRecorder:(NSString *) key;
- (void)startRecorder:(NSString *) key;
- (void)stopRecorder;
- (void)stopRecorder:(NSString *) key;
- (void)pauseRecorder:(NSString *) key;
- (void)setRecorderDelegate:(id<AVAudioRecorderDelegate>) delegate 
						key:(NSString *) key;
- (void)setUserInfo:(id) userInfo
				key:(NSString *) key;
- (id)getUserInfo:(NSString *) key;
- (NSString *)getKeyByAudioRecorder:(AVAudioRecorder *) recorder;
- (AVAudioRecorder *)getAudioRecorderByKey:(NSString *) key;
- (BOOL)recorderExists:(NSString *) key;
- (NSArray *)allKeys;
- (NSString *)patchByKey:(NSString *) key;
- (NSData *)dataByKey:(NSString *) key;
- (void)saveToFile:(NSString *)patch
			   key:(NSString *) key;
@end
