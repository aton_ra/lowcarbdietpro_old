//
//  PBDefaultLoadingPopup.h
//  PB
//
//  Created by Dmitry Mikheev on 11/8/12.
//
//

#import "PBPopupView.h"

@interface PBDefaultLoadingPopup : PBPopupView

@property(retain, nonatomic)UILabel *loadingLabel;

@end
