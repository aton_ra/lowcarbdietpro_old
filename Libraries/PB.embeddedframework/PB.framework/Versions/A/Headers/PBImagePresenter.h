//
//  PBImagePresenter.h
//  OS4
//
//  Created by Dmytro Mikheiev on 5/24/11.
//  Copyright 2011 POLAR-B. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "iCarousel.h"
#import "PBService.h"

@class PBImagePresenter;

@protocol PBImagePresenterDelegate
@optional
- (void)imagePresenterWillShowImage:(PBImagePresenter *)sender;
- (void)imagePresenterDidShowImage:(PBImagePresenter *)sender;
- (void)imagePresenterWillHideImage:(PBImagePresenter *)sender;
- (void)imagePresenterDidHideImage:(PBImagePresenter *)sender;
@end

@interface PBImagePresenter : NSObject <UIScrollViewDelegate,iCarouselDataSource,iCarouselDelegate>{
	UIWindow *_window;
    
    UIView *_view;
    UIView *_rotationView;
    UIView *_backgroundView;

    iCarousel *_carousel;
    iCarouselType _animationType;
	
	NSArray *_imagesArray;
    
	NSObject<PBImagePresenterDelegate> *_delegate;
	int _currentPageInt;
    
	UIInterfaceOrientationMask _previousOrientation;
    UIColor *_backgroundColor;
    
    UIInterfaceOrientationMask _supportedOrientationMask;
    
    NSString *_noImageUrl;
    NSString *_cacheGroup;
    PBCacheStorage _cacheStorage;
    PBServiceCache _cachePolicy;
}
/*
 * specify cache group for current image.
 * Groups may be useful if you want to delete some of the cached images later, from memory or disk
 */
@property(nonatomic, retain) NSString *cacheGroup;
/*
 * image storage: Disk or Memory. Default - Disk
 */
@property(nonatomic, assign) PBCacheStorage cacheStorage;
/*
 * behaviour of the cache. By default cache will not be used at all
 */
@property(nonatomic, assign) PBServiceCache cachePolicy;
/*
 * URL of the image that will be shown if the main image do not exists
 */
@property(nonatomic, retain) NSString *noImageUrl;

@property(nonatomic, retain) UIView *view;
@property(nonatomic, assign) iCarouselType animationType;
@property(nonatomic, assign) NSObject<PBImagePresenterDelegate> *delegate;
/*
 * background view color
 */
@property(nonatomic, retain) UIColor *backgroundColor;
/*
 * PBImagePresenter is singleton class
 */
+ (PBImagePresenter *)instance;
/*
 * charge presenter. should be called in AppDelegate
 */
- (void)chargeWithWindow:(UIWindow *)window;

/*
 * show images. image will flies up from specified rect
 */
- (void)presentImages:(NSArray *)images
             fromRect:(CGRect)rect;

/*
 * show images. image will flies up from view
 */
- (void)presentImages:(NSArray *)images
    activeImageNumber:(int)imageNumber
             fromView:(UIView*)view;
/*
 * show array of images by their URIs. images will flies up from specified rect. activeImageNumber - the index of an image that will be selected by default
 */
- (void)presentImages:(NSArray *)images
			 fromRect:(CGRect)rect
	activeImageNumber:(int)imageNumber
supportedOrientationMask:(UIInterfaceOrientationMask)supportedOrientationMask;

/*
 * close presenter manually
 */
- (IBAction)close;

-(void)viewDidAppear;
@end
