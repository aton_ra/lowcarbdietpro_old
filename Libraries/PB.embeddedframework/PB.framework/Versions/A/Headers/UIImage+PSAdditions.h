//
//  UIImage+PSAdditions.h
//  TweetoritesApp
//
//  Created by Andrew Wooster on 9/13/09.
//  Copyright 2009 Planetary Scale LLC. All rights reserved.
//  Worldwide, transferrable, non-exclusive license granted to Divvyshot, Inc.
//

#import <UIKit/UIKit.h>

@interface UIImage (PSAdditions)
/*
 * Thread-safe image resizer which takes into account UIImageOrientation
 */
- (UIImage *)resizeToSize:(CGSize)newSize;
- (UIImage *)resizeToSize:(CGSize)newSize fromCamera:(BOOL)isFromITouchCamera;
@end
