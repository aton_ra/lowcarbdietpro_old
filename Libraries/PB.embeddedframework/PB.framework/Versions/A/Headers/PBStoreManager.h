//
//  PBStoreManager.h
//  OS4
//
//  Created by Dmytro Mikheiev on 4/19/11.
//  Copyright 2011 POLAR-B. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

@class PBStoreManager;
@protocol PBStoreManagerDelegate
@optional
/*
 * payment was successful
 */
- (void)transactionCompleted:(PBStoreManager *)sender;
/*
 * payment wasn't successful due to some reason
 */
- (void)transactionFailed:(PBStoreManager *)sender;
/*
 * payment process was interrupted by user
 */
- (void)transactionCanceled:(PBStoreManager *)sender;
/*
 * payment process started
 */
- (void)transactionStarted:(PBStoreManager *)sender;
/*
 * payment can not be finished coz user has no permissions
 */
- (void)transactionNotAuthorized:(PBStoreManager *)sender;
/*
 * loading of the products information from the app store did start
 */
- (void)productsLoadingStarted:(PBStoreManager *)sender;
/*
 * products loaded from the app store. you may read product details (prices, names etc..) from "productsDetails" array
 */
- (void)productsLoadingFinished:(PBStoreManager *)sender products:(NSArray *)products;
@end

@interface PBStoreManager : NSObject <SKProductsRequestDelegate, SKPaymentTransactionObserver> {
	NSMutableArray *_productsDetailsArray;
    NSObject<PBStoreManagerDelegate> *_delegate;
    NSString *_depositedProductIdString;
}
@property(nonatomic, assign) NSObject<PBStoreManagerDelegate> *delegate;
@property(nonatomic, readonly) NSArray *productsDetails;

+ (PBStoreManager *)instance;
/*
 * Charge with array of NSString id's of products. 
 * StoreManager will start fetching product details right away
 * this method required only if you need to show some details about product (title, description, price).
 * There is no need to call it if you only need to buy the product
 */
- (void)chargeWithProducts:(NSArray *)products;
/*
 * Initiate purchase transaction
 */
- (void)buyProduct:(NSString*) productId;
@end
