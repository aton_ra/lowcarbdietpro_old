//
//  PBLoadingImageView.h
//  POLAR-B framework
//
//  Created by Dmitry Mikheev on 4/13/10.
//  Copyright 2010 POLAR-B. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PBService.h"

@interface PBImageView : UIImageView {
	NSString *_imageUrl, *_noImageUrl;
    PBService *_imageService;
	UIActivityIndicatorView *_loadingIndicator;
    NSObject *_userInfo;
    NSString *_cacheGroup;
    PBCacheStorage _cacheStorage;
    PBServiceCache _cachePolicy;
    BOOL _isShowGifAnimation;
}
/*
 * Is Show gif animation. Default Yes
 */
@property(nonatomic, readwrite) BOOL isShowGifAnimation;
/*
 * URL of the main image. It can be external http:// or https:// URL, or local bundle:// or documents:// or temp://
 * !!! right after setting this parameter - the image will start to load. Be sure to set this parameter at very last stage
 */
@property(nonatomic, retain) NSString *imageUrl;
/*
 * URL of the image that will be shown if the main image do not exists
 */
@property(nonatomic, retain) NSString *noImageUrl;
/*
 * indicator that will be visible while loading
 */
@property(nonatomic, readonly) UIActivityIndicatorView *loadingIndicator;
/*
 * just a place to store any variable attached to this object
 */
@property(nonatomic, retain) NSObject *userInfo;
/*
 * specify cache group for current image. 
 * Groups may be useful if you want to delete some of the cached images later, from memory or disk
 */
@property(nonatomic, retain) NSString *cacheGroup;
/*
 * image storage: Disk or Memory. Default - Disk
 */
@property(nonatomic, assign) PBCacheStorage cacheStorage;
/*
 * behaviour of the cache. By default cache will not be used at all
 */
@property(nonatomic, assign) PBServiceCache cachePolicy;

/*
 * YES if image was successefuly loaded
 */
@property(nonatomic, readonly) BOOL wasImageSuccessfullyLoaded;

@end

