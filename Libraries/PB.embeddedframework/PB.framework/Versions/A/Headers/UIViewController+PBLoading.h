//
//  UIViewController+PBLoading.h
//  PB
//
//  Created by Dmitry Mikheev on 11/8/12.
//
//

#import <UIKit/UIKit.h>

@interface UIViewController (PBLoading)
/*
 * Show loading popup created from specified nib
 */
- (void)showLoading:(NSString *)nibName;
/*
 * Show default loading popup
 */
- (void)showLoading;
/*
 * Show default loading popup With title
 */

- (void)showLoadingWithTitle:(NSString*)titleString;

/*
 * Hide loading popup
 */
- (void)hideLoading;
@end
