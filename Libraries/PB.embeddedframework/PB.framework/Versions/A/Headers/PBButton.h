//
//  PBButton.h
//
//  Created by POLAR-B team on 11/13/12.
//  Copyright 2010 POLAR-B. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreGraphics/CoreGraphics.h>

@interface PBButton : UIButton{
    NSArray  *_normalGradientColors;
    NSArray  *_normalGradientLocations;
    NSArray  *_highlightGradientColors;
    NSArray  *_highlightGradientLocations;
    
    CGFloat         _cornerRadius;
    CGFloat         _strokeWeight;
    UIColor         *_strokeColor;
    UIColor         *_color;
    
@private
    CGGradientRef   normalGradient;
    CGGradientRef   highlightGradient;
}
@property (nonatomic) CGFloat cornerRadius;
@property (nonatomic) CGFloat strokeWeight;
@property (nonatomic, retain) UIColor *strokeColor;
@property (nonatomic, retain) UIColor *color;

@end
