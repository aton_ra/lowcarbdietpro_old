//
//  BPLabel.h
//  OS4
//
//  Created by Dmytro Mikheiev on 11/3/10.
//  Copyright 2010 POLAR-B. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum{
    PBLineType_Upper,
    PBLineType_Lower,
    PBLineType_Middle,
    PBLineType_DiagonalLeft,
    PBLineType_DiagonalRight
}PBLineType;

@interface PBLabel : UILabel {
    NSMutableArray *_linesArray;
}
- (void)adjustToFitSize:(CGSize) size
          usingFontName:(NSString *) fontName;
- (void)addLine:(PBLineType)type
          color:(UIColor *)color
           dash:(BOOL)dash
      thickness:(int)thickness;
- (void)addLine:(PBLineType)type;
- (void)removeLines;
@end

