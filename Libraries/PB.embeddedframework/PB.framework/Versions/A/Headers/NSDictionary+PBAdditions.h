//
//  NSDictionary+URL.h
//  PBFramework
//
//  Created by Dmytro Mikheiev on 5/18/12.
//  Copyright (c) 2012 POLAR-B. All rights reserved.
//

@interface NSDictionary (PBAdditions)

/* 
 * return's all the keys as a parameters for url
 */
- (NSString *)urlString;
/* 
 * parse parameters from url and returns a dictionary
 */
+ (NSDictionary *)dictionaryWithUrlString:(NSString *)urlString;
@end