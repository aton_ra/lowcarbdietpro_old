//
//  Services.h
//
//  Created by Dmitry Mikheev on 9/9/09.
//  Copyright (c) 2012 POLAR-B. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIFormDataRequest.h"
#import "ASIDownloadCache.h"
#import "PBCache.h"

typedef enum {
	PBServiceResponceType_String = 0,
	PBServiceResponceType_Data = 1,
	PBServiceResponceType_XML = 2,
	PBServiceResponceType_JSON = 3,
	PBServiceResponceType_Image = 4
}PBServiceResponceType;

typedef enum {
    PBServiceCache_DoNotUseCache = 0,
    PBServiceCache_UseCacheIfSourceNotAvailable,
    PBServiceCache_UseSourceIfCacheNotAvailable,
    PBServiceCache_UseCache
}PBServiceCache;

typedef enum {
    PBServiceXMLParser_PBXMLParser,
    PBServiceXMLParser_XMLReader
}PBServiceXMLParserType;

typedef enum {
    PBServiceJSONParser_NSParser,
    PBServiceJSONParser_JSONKIT
}PBServiceJSONParserType;

@protocol ServicesDelegate;
@interface PBService : NSObject <ASIHTTPRequestDelegate> {
	id _delegate;
	SEL _successSelector;
	SEL _errorSelector;
	ASIFormDataRequest *_request;
	PBServiceCache _cachePolicy;
	NSObject *_responce;
	PBServiceResponceType _responceType;
	NSObject *_userInfo;
    NSString *_cacheGroup;
    NSString *_url;
    PBCacheStorage _cacheStorage;
    PBServiceXMLParserType _xmlParserType;
    PBServiceJSONParserType _jsonParserType;
    NSBlockOperation *_readFromCacheOperation;
}
/*
 * is needed to use post param for cache 
 * default no
 */
@property(nonatomic, assign) BOOL isNeededToUsePostParamForCache;

/*
 * blok for success
 */
@property(nonatomic, copy) void(^success)(PBService*);
/*
 * blok for error 
 */
@property(nonatomic, copy) void(^error)(PBService*);
/*
 * Default value PBServiceXMLParser_PBXMLParser
 */
@property(nonatomic, assign) PBServiceXMLParserType xmlParserType;
/*
 * Default value PBServiceJSONParser_JSONKIT
 */
@property(nonatomic, assign) PBServiceJSONParserType jsonParserType;
/*
 * Specify behaviour of the cache
 */
@property(nonatomic, assign) PBServiceCache cachePolicy;
/*
 * specify cache group for current service request.
 * Groups may be useful if you want delete some of the cached requests later, from memory or disk
 */
@property(nonatomic, retain) NSString *cacheGroup;
/*
 * Specify cache storage: Disk or Memory. Default is Disk
 */
@property(nonatomic, assign) PBCacheStorage cacheStorage;
/*
 * This object contains parsed responce obtained after sent request
 */
@property(nonatomic, readonly) id responce;
/*
 * ASIFormDataRequest request that was used to obtain responce from server.
 * May be useful if you want to discover more info about received responce, like a Status Code.. etc.
 */
@property(nonatomic, readonly) ASIFormDataRequest *request;
/*
 * URL that was used for the request
 */
@property(nonatomic, readonly) NSString *url;
/*
 * Just a place to store any variable attached to this object
 */
@property(nonatomic, retain) NSObject *userInfo;
/*
 * Init Service with minimum parameters required for each request
 */
- (id) initWithResponder:(id) delegate
		 successSelector:(SEL) successSelector
		   errorSelector:(SEL) errorSelector
                     url:(NSString *)url;
/*
 * Stop loading request
 */
- (void) cancelRequest;
/*
 * Check if cache available for current URL
 */
- (BOOL) cacheAvailable;
/*
 * Remove cache for current URL
 */
- (void)resetCache;
/*
 * Load content from specified URL and parse it
 */
- (void) getXML;
- (void) getJSON;
- (void) getString;
- (void) getData;
- (void) getImage;
- (void) get:(PBServiceResponceType)type;
- (void) get:(PBServiceResponceType)type isAsynchronous:(BOOL)isAsynchronous;
@end
