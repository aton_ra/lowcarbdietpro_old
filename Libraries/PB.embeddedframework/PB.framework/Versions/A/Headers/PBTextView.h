//
//  PBTextView.h
//  OS4
//
//  Created by Dmytro Mikheiev on 5/13/11.
//  Copyright 2011 POLAR-B. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    PBTextBorderStyleNone = 0,
    PBTextBorderStyleRoundedRect
} PBTextBorderStyle;

@interface PBTextView : UIView <UITextViewDelegate>{
	UITextView *_textView;
	UILabel *_placeholderLabel;
	UIImageView *_backgroundBorderImageView;
	
	id<UITextViewDelegate> _delegate;
	PBTextBorderStyle _borderStyle;
}
/*
 * set placeholder text, like in UITextField
 */
@property(nonatomic, retain) NSString *placeholder;
/*
 * all the events from UITextView will be redirected to specified delegate
 */
@property(nonatomic, assign) id<UITextViewDelegate> delegate;
/*
 * RoundedRect - will look the same as UITextField default look
 */
@property(nonatomic, assign) PBTextBorderStyle borderStyle;
/*
 * set/read text to/from UITextView
 */ 
@property(nonatomic, assign) NSString *text;
/*
 * inner UITextView object
 */
@property(nonatomic, readonly) UITextView *textView;

@end
