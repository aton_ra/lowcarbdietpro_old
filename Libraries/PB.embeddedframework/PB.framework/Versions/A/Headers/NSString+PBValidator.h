//
//  NSString+PBValidator.h
//  PB
//
//  Created by POLAR15 on 11/8/12.
//
//

#import <Foundation/Foundation.h>
#import "PBValidator.h"

@interface NSString (PBValidator)
- (BOOL)valid:(PBValidatorType)type;
@end
