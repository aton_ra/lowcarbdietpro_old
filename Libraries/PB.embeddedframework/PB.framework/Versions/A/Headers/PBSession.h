//  POLAR-B framework
//
//  Created by Dmytro Mikheiev on 5/25/10.
//  Copyright 2010 POLAR-B. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PBSession : NSMutableDictionary {
	NSMutableDictionary *_dictionary;
}
@property(nonatomic, retain) NSMutableDictionary *dictionary;

+ (PBSession *)instance;
@end
