//
//  PBSlideView.h
//  unittest
//
//  Created by polar12 on 12/5/12.
//  Copyright (c) 2012 home. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum{
    OpenSlideDirectionUp,
    OpenSlideDirectionRight,
    OpenSlideDirectionDown,
    OpenSlideDirectionLeft
}OpenSlideDirection;

@class PBSlideView;
@protocol PBSlideViewDelegate <NSObject>
@optional
// 0 - fully close   100 - fully open
-(void)slideView:(PBSlideView*)sender didChangeOpeningPanelValueInPercent:(float)percent;

-(void)slideView:(PBSlideView*)sender willOpenPanelWithDuration:(NSTimeInterval)duration;
-(void)slideView:(PBSlideView*)sender willClosePanelWithDuration:(NSTimeInterval)duration;

-(void)didOpenPanel:(PBSlideView*)sender;
-(void)didClosePanel:(PBSlideView*)sender;
@end

@interface PBSlideView : UIView<UIGestureRecognizerDelegate>{
	OpenSlideDirection _slideDirection;
    float _animationDuration;
    CGPoint _previousTouchPoint;
    BOOL _isPreviousStateOpen;
    NSObject<PBSlideViewDelegate> *_delegate;
    UIView *_contentView;
    UIView *_controlView;
    
    //Gesture Recognizers for controll view
    UIPanGestureRecognizer      *_panGesture;
    UITapGestureRecognizer      *_tapGesture;
}

@property (readonly, nonatomic) UIView *contentView;
@property (retain, nonatomic) IBOutlet UIView *controlView;
@property (assign, nonatomic) OpenSlideDirection slideDirection;
@property (assign, nonatomic) IBOutlet NSObject<PBSlideViewDelegate> *delegate;
@property (retain, nonatomic) IBOutletCollection(UIView) NSMutableArray *followingViewsArray;


-(void)openSlideView:(BOOL)animated;
-(void)closeSlideView:(BOOL)animated;
-(IBAction)switchState;

@end
