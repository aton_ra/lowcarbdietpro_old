//
//  NSNumber+PBAdditions.h
//  OS4
//
//  Created by Dmytro Mikheiev on 9/15/10.
//  Copyright 2010 POLAR-B. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSNumber (PBAdditions)
+ (NSNumber *) randomNumberInRange:(NSRange) range;
+ (NSNumber *) randomNumberInRange:(NSRange) range
			   withNumberException:(NSNumber *) exception;
+ (NSNumber *) randomNumberInRange:(NSRange) range
			  withNumberExceptions:(NSArray *) exceptions;
- (NSNumber*)numberRoundingDecimals:(int)decimals;

@end
