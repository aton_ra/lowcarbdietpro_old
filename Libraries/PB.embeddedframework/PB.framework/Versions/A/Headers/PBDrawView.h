//
//  DrawView.h
//  EcceHomo
//
//  Created by Dmytro Mikheiev on 9/2/12.
//  Copyright (c) 2012 POLAR-B. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PBDrawLayerView.h"

typedef enum{
    PBDrawViewStyle_Image, // private use only
    PBDrawViewStyle_Pencil,
    PBDrawViewStyle_Line,
    PBDrawViewStyle_Ellipse,
    PBDrawViewStyle_Eraser,
    PBDrawViewStyle_EllipseFill,
    PBDrawViewStyle_Rectangle,
    PBDrawViewStyle_RectangleFill,
    PBDrawViewStyle_Spray
}PBDrawViewStyle;

@interface PBDrawView : UIView <PBDrawLayerViewDelegate>{
    PBDrawLayerView *_activeLayerView;
    PBDrawLayerView *_passiveLayerView;
    
    NSMutableArray *_drawStackArray;
    NSMutableArray *_redoDrawStackArray;
    NSMutableDictionary *_activePrimitive;
    UIPanGestureRecognizer *_panRecognizer;
    CGContextRef _passiveContext;

    int _undoSteps;
    PBDrawViewStyle _drawType;
    float _drawWidth;
    float _drawOpacity;
    CGLineCap _drawCap;
    CGLineJoin _drawJoin;
    UIColor *_drawColor;
}
/*
 * the steps number user may click undo button. 
 * The greated number - the slower drawing will work.
 * By default - 5
 */
@property(nonatomic, assign) int undoSteps;
/*
 * set active draw type
 */
@property(nonatomic, assign) PBDrawViewStyle drawType;
/*
 * line width
 * by default - 2
 */
@property(nonatomic, assign) float drawWidth;
/*
 * line opacity. Can be set using UIColor also. 
 * By default - 1
 */
@property(nonatomic, assign) float drawOpacity;
/*
 * start and end  points of the line. 
 * By default - kCGLineCapRound
 */
@property(nonatomic, assign) CGLineCap drawCap;
/*
 * joints between the lines.
 * By default - kCGLineJoinRound
 */
@property(nonatomic, assign) CGLineJoin drawJoin;
/*
 * active color
 * by default - black
 */
@property(nonatomic, retain) UIColor *drawColor;

/*
 * undo last change
 */
- (void)undo;
/*
 * redo last undo step
 */
- (void)redo;
/*
 * clear drawing area
 */
- (void)clear;
/*
 * load image. Image will be added with original size at left/top corner
 */
- (void)addImage:(UIImage *)image;
/*
 * load image. Image will be added with original size at specified position
 */
- (void)addImage:(UIImage *)image
       withPoint:(CGPoint)point;
/*
 * load image. Image will be added at specified position and will be streached to specified size
 */
- (void)addImage:(UIImage *)image
        withRect:(CGRect)rect;
/*
 * returns an image from the current context
 */
- (UIImage *)getImage;
/*
 * returns image merged with specified background image
 */
- (UIImage *)getImageJoinedWith:(UIImage *)backgroundImage
                       withRect:(CGRect)rect;
@end
