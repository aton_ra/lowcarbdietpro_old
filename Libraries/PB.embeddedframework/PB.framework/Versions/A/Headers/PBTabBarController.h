//
//  PBTabBarControllerViewController.h
//
//  Created by Alexey Kubas on 04.12.12.
//  Copyright (c) 2012 Polar-B. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PBTabBarController;

@protocol PBTabBarControllerDelegate <NSObject>
@optional
- (void)tabBarController:(PBTabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController;
@end

@interface PBTabBarController : UIViewController


//
//
//
@property (nonatomic, readwrite, assign) id<PBTabBarControllerDelegate> delegate;

//
//
//
@property (nonatomic, strong) IBOutletCollection(UIViewController) NSArray *viewControllers;

//
//
//
@property (nonatomic, readwrite, assign) UIViewController *selectedViewController;
//
//
//
@property (nonatomic, readwrite, assign) NSUInteger selectedIndex;

//
//
//
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *itemsCollections;

//
//
//
@property (strong, nonatomic) IBOutlet UIView* contentView;

//
//
//
- (IBAction)setSelectedIndexFromTag:(id)sender;

@end
