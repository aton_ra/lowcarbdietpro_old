//
//  PBLocationManager.h
//  PBFramework
//
//  Created by POLAR-B team on 4/28/12.
//  Copyright (c) 2012 POLAR-B. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol PBLocationManagerDelegate <NSObject>
- (void)locationUpdated:(CLLocation *)location;
- (void)locationUpdatedWithError:(NSError *)error;
@end

@interface PBLocationManager : NSObject <CLLocationManagerDelegate>{
    CLLocationManager * _locationManager;
    NSObject<PBLocationManagerDelegate> *_delegate;
    double _meditationTimeDouble;
    NSDate *_startGPSDate;
    NSMutableArray *_locationStackArray;
}
+ (PBLocationManager *)instance;
@property(nonatomic, readonly) CLLocationManager *locationManager;
@property(nonatomic, assign) NSObject<PBLocationManagerDelegate> *delegate;

/*
 * get location with best accuracy. GPS will think 2 seconds
 */
- (void)getLocation;
/*
 * get location with meditationTime - the time to give GPS to think. in seconds
 */
- (void)getLocation:(double)meditationTime;
@end
