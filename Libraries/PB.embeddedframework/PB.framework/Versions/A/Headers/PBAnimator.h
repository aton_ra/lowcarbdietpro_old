//
//  PBAnimator.h
//  project
//
//  Created by Dmytro Mikheiev on 9/13/10.
//  Copyright 2010 POLAR-B. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum{
	PBAnimatorState_Started = 0,	
	PBAnimatorState_Finished = 1,
	PBAnimatorState_Stoped = 2,
	PBAnimatorState_Paused = 3
}PBAnimatorState;

@class PBAnimator;
@protocol PBAnimatorDelegate <NSObject>
@optional
- (void)animatorStateDidChange:(PBAnimator *) sender state:(PBAnimatorState) state;
- (void)animatorCyclePassed:(PBAnimator *) sender cycle:(int) cycle;
- (void)animatorSpriteShowed:(PBAnimator *) sender sprite:(UIImage *) sprite spriteIndex:(int) index;
@end

@interface PBAnimator : NSObject {
	NSTimer *_mainTimer;
	PBAnimatorState _lastState;
	int _currentFrameIndex;
	int _currentIteration;
	UIImageView *_targetImageView;
	NSMutableArray *_imageDataArray;
	id<PBAnimatorDelegate> _delegate;
	double _framesPerSecond;
	int _repeatCount;
	id _userInfo;
}
@property(nonatomic, assign) id delegate;
@property(nonatomic, readonly) double framesPerSecond;
@property(nonatomic, assign) int repeatCount;
@property(nonatomic, assign) UIImageView *targetImageView;
@property(nonatomic, retain) id userInfo;

/*
 * Specify target image view later...
 */
- (id) initWithFramesPerSecond:(int) framesPerSecond
                   repeatCount:(int) repeatCount;
/*
 * init with default values repeatCount = 1, framesPerSecond = 24
 */
- (id) initWithTargetImage:(UIImageView *) targetImageView;
/*
 * init with default value repeatCount = 1
 */
- (id) initWithTargetImage:(UIImageView *) targetImageView
		   framesPerSecond:(int) framesPerSecond;
/*
 * init
 */
- (id) initWithTargetImage:(UIImageView *) targetImageView
		   framesPerSecond:(int) framesPerSecond
			   repeatCount:(int) repeatCount;

/*
 * add image by with it's data
 */
- (void) addSpriteFromData:(NSData *) data;
/*
 * add image by it's name in bundle
 */
- (void) addSpriteFromName:(NSString *) name;
/*
 * add delay to animation flow
 */
- (void) addDelay:(double) seconds;
/*
 * previously added sprites will be doubled and mirrored
 */
- (void) addMirrorEffect;
/*
 * add selector that will be called while animation flow. selector will be called using delegate property. 
 * property attribute will be pushed to selector. It means that selector should look like 'methodname:(id) parameter'
 */
- (void) addAction:(SEL) sel
		 parameter:(id) parameter;

- (void) start;
- (void) stop;
- (void) pause;

- (void) _showNextFrame;
@end
