//
//  NSNumberPBAdditions.h
//  OS4
//
//  Created by Dmytro Mikheiev on 9/15/10.
//  Copyright 2010 POLAR-B. All rights reserved.
//

@interface NSArray (Additions)
- (BOOL) containsString:(NSString *) val;
- (BOOL) containsNumber:(NSNumber *) val;
@end
