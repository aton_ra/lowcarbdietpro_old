//
//  NSString+md5.h
//  PushNotifications
//
//  Created by Yecine Dhouib & Taras Kalapun on 09/12/09.
//  Copyright 2009 SBW PARIS. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface NSString (md5)

- (NSString *)md5;

@end
