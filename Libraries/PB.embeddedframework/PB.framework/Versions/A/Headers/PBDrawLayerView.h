//
//  PBDrawLayerView.h
//  EcceHomo
//
//  Created by POLAR15 on 9/3/12.
//  Copyright (c) 2012 POLAR-B. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol PBDrawLayerViewDelegate
@required
- (void)drawLayerRect:(CGRect)rect sender:(UIView *)sender;
@end

@interface PBDrawLayerView : UIView{
    NSObject<PBDrawLayerViewDelegate> *_delegate;
}
@property(nonatomic, assign) NSObject<PBDrawLayerViewDelegate> *delegate;
@end
