//
//  PBPopup.h
//  OS4
//
//  Created by Dmytro Mikheiev on 2/18/11.
//  Copyright 2011 POLAR-B. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PBPopupView;
@protocol PBPopupViewDelegate
@optional
- (void)popupClickedBackground:(PBPopupView *)sender;
- (void)popupDidHide:(PBPopupView *) popup;
- (void)popupDidShow:(PBPopupView *) popup;
@end

@interface PBPopupView : UIView {
	NSObject *_userInfo;
    NSObject<PBPopupViewDelegate> *_eventsDelegate;
}
@property(retain) NSObject *userInfo;
@property(assign) NSObject<PBPopupViewDelegate> *eventsDelegate;
/*
 * A short way to show a popup. The same like to call: [[[PBPopup alloc] init] autorelease] show];
 */
+ (PBPopupView *)show;
/*
 * A short way to show a popup. The same like to call: [[[PBPopup alloc] initWithNibName:@"..."] autorelease] show];
 */
+ (PBPopupView *)show:(NSString *)nibName;
/*
 * Create popup with Xib file
 */
- (id)initWithNib:(NSString *)nibName;
/*
 * Show popup. The same operation may be accomplished calling [[PBPopupManager instance] showPopup:..] from outside
 */
- (void)show;
/*
 * Hide popup. The same operation may be accomplished calling [[PBPopupManager instance] hidePopup] from outside
 */
- (void)hide;
@end
