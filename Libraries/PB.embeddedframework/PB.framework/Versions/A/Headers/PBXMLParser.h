//
//  PBXMLParser.h
//  OS4
//
//  Created by Dmytro Mikheiev on 7/22/10.
//  Copyright 2010 POLAR-B. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PBXMLParser : NSXMLParser <NSXMLParserDelegate>{
@private
  id              _rootObject;
  BOOL            _treatDuplicateKeysAsArrayItems;
  NSMutableArray* _objectStack;
}
@property (nonatomic, readonly) id    rootObject;
@property (nonatomic, assign)   BOOL  treatDuplicateKeysAsArrayItems;

@end

@interface NSDictionary (PBXMLAdditions)
- (NSString*)nameForXMLNode;
- (NSString*)typeForXMLNode;
- (id)objectForXMLNode;
- (NSArray*)arrayForKey:(id)key;

@end
