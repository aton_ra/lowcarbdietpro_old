@interface NSMutableArray (StackAdditions)

- (id)pop;
- (void)push:(id)obj;

@end

