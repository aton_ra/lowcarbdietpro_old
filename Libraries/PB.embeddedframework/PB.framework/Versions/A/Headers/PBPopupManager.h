//
//  PBPopupManager.h
//  OS4
//
//  Created by Dmytro Mikheiev on 2/18/11.
//  Copyright 2011 POLAR-B. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PBPopupView.h"

@interface PBPopupManager : NSObject {
	UIWindow *_mainWindow;
	PBPopupView *_activePopup;
	PBPopupView *_activePopupDeposited;
	UIView *_backgroundView;
	UIButton *_backgroundButton;
	CGFloat _currentPopupRadiansFloat;
	BOOL _shouldHideBackground;
    CAAnimation *_animationToListen;
}
@property(readonly) PBPopupView *activePopup;
@property(nonatomic,assign) BOOL shouldHideBackground;

+ (PBPopupManager *)instance;
/*
 * charge with window where popups will appears
 */
- (void)chargeWithWindow:(UIWindow *)window;
/*
 * charge with default main window. may crash sometimes. not tested for 100%
 */
- (void)charge;
/*
 * show popup. if another popup a shown it will hide with animation and new popup will be shown
 */
- (void)showPopup:(PBPopupView *)popup;
/*
 * show popup with feedback delegate
 */
- (void)showPopup:(PBPopupView *)popup
		 delegate:(NSObject<PBPopupViewDelegate> *) delegate;
/*
 * hide popup
 */
- (void)hidePopup;
/*
 * hide popup with feedback delegate
 */
- (void)hidePopup:(NSObject<PBPopupViewDelegate> *) delegate;
/*
 * change orientation. the best way to use it is to embed it into 
 * "application:(UIApplication *)application willChangeStatusBarOrientation:(UIInterfaceOrientation)newStatusBarOrientation" 
 * method of the app delegate
 */
- (void)rotateToOrientation:(UIInterfaceOrientation)toOrientation fromOrientation:(UIInterfaceOrientation)fromOrientation;
@end
