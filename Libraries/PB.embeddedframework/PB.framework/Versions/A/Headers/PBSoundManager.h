//
//  SoundManager.h
//  OS4
//
//  Created by Dmytro Mikheiev on 11/3/10.
//  Copyright 2010 POLAR-B. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface PBSoundManager : NSObject {
	NSMutableDictionary *_soundStackDictionary;
}
+ (PBSoundManager *)instance;

/*
 * create sound session with file patch
 */
- (void) chargeSoundWithPath:(NSString *) path key:(NSString *) key;
/*
 * create sound session with file patch
 */
- (void) chargeSoundWithPath:(NSString *) path key:(NSString *) key numberOfLoops:(int) numberOfLoops;
/*
 * create sound session with file in bundle
 */
- (void) chargeSoundWithName:(NSString *) bundleName key:(NSString *) key;
/*
 * create sound session with file in bundle and loops number (loops = 0 means infinite loops)
 */
- (void) chargeSoundWithName:(NSString *) bundleName key:(NSString *) key numberOfLoops:(int) numberOfLoops;
/*
 * create sound session with data
 */
- (void) chargeSoundWithData:(NSData *) sound key:(NSString *) key;
/*
 * create sound session with data and loops number (loops = 0 means infinite loops)
 */
- (void) chargeSoundWithData:(NSData *) sound key:(NSString *) key numberOfLoops:(int) numberOfLoops;
/*
 * remove all sounds from stack
 */
- (void) unchargeSound;
/*
 * remove all sounds starting with prefix
 */
- (void) unchargeSoundWithPrefix:(NSString *) prefixKey;
/*
 * remove sound session
 */
- (void) unchargeSound:(NSString *) key;
/*
 * set volume for all sound sessions
 */
- (void) setSoundVolume:(float) volume;
/*
 * set volume for sessions starting with prefix
 */
- (void) setSoundVolumeWithPrefix:(float) volume
						prefixKey:(NSString *) prefixKey;
/*
 * set volume for sound by key
 */
- (void) setSoundVolume:(float) volume
					key:(NSString *) key;
/*
 * play all sounds
 */
- (void) playSound;
/*
 * play sound taht starts with prefix
 */
- (void) playSoundWithPrefix:(NSString *) prefixKey;
/*
 * play sound by key
 */
- (void) playSound:(NSString *) key;
/*
 * stop all sounds
 */
- (void) stopSound;
/*
 * stop sound taht starts with prefix
 */
- (void) stopSoundWithPrefix:(NSString *) prefixKey;
/*
 * stop sound by key
 */
- (void) stopSound:(NSString *) key;
/*
 * pause all sounds
 */
- (void) pauseSound;
/*
 * pause sound taht starts with prefix
 */
- (void) pauseSoundWithPrefix:(NSString *) prefixKey;
/*
 * pause sound by key
 */
- (void) pauseSound:(NSString *) key;
/*
 * set delegate to get events from AVAudioPlayer object
 */
- (void) setSoundDelegate:(id<AVAudioPlayerDelegate>) delegate 
					  key:(NSString *) key;
/*
 * store some info related to sound 
 */
- (void) setUserInfo:(id) userInfo
				 key:(NSString *) key;
/*
 * read some info related to sound
 */
- (id) getUserInfo:(NSString *) key;
/*
 * get sound session key using player object
 */
- (NSString *) getKeyByAudioPlayer:(AVAudioPlayer *) player;
/*
 * get player object using key
 */
- (AVAudioPlayer *) getAudioPlayerByKey:(NSString *) key;
/*
 * check whether sound session with such key exists
 */
- (BOOL) soundExists:(NSString *) key;
/*
 * return array with all available sound keys
 */
- (NSArray *) allKeys;
@end
