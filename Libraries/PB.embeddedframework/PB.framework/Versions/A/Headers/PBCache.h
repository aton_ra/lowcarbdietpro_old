//
//  PBCache.h
//  PB
//
//  Created by Dmytro Mikheiev on 7/17/12.
//  Copyright (c) 2012 POLAR-B. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum {
    PBCacheStorage_Disk = 0,
    PBCacheStorage_Memory
}PBCacheStorage;
@interface PBCache : NSObject{
    NSMutableDictionary *_memoryStorageDictionary;
}
+ (PBCache *)instance;

/*
 * Remove a group of cache
 */
- (void)removeCacheGroup:(NSString *)group;
- (void)removeCacheGroup:(NSString *)group
                 storage:(PBCacheStorage)storage;

/*
 * Remove cache by URL
 */
- (void)removeCache:(NSString *)url;
- (void)removeCache:(NSString *)url
            storage:(PBCacheStorage)storage;
- (void)removeCache:(NSString *)url
              group:(NSString *)group
            storage:(PBCacheStorage)storage;

/*
 * Get cache by URL
 */
- (id)getCache:(NSString *)url;
- (id)getCache:(NSString *)url
       storage:(PBCacheStorage)storage;
- (id)getCache:(NSString *)url
         group:(NSString *)group
       storage:(PBCacheStorage)storage;

/*
 * Add data to cache
 */
- (void)addCache:(NSData *)cache
          forUrl:(NSString *)url;
- (void)addCache:(NSData *)cache
          forUrl:(NSString *)url
         storage:(PBCacheStorage)storage;
- (void)addCache:(NSData *)cache
          forUrl:(NSString *)url
           group:(NSString *)group
         storage:(PBCacheStorage)storage;

/*
 * Check if cache available
 */
- (BOOL)cacheAvailableForUrl:(NSString *)url;
- (BOOL)cacheAvailableForUrl:(NSString *)url
                     storage:(PBCacheStorage)storage;
- (BOOL)cacheAvailableForUrl:(NSString *)url
                       group:(NSString *)group
                     storage:(PBCacheStorage)storage;
@end
