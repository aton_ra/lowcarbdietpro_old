//
//  UIImagePBAdditions.h
//  OS4
//
//  Created by Dmytro Mikheiev on 11/27/10.
//  Copyright 2010 POLAR-B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (PBAdditions)
+ (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize;
@end
