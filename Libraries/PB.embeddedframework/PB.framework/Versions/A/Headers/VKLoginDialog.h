//
//  VKLoginDialog.h
//  VKapi
//
//  Created by Dmytro Mikheiev on 9/9/11.
//  Copyright 2011 POLAR-B. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VKDialog.h"

@interface VKLoginDialog : VKDialog <UIWebViewDelegate> {
	UIWebView *_webView;
	UIView *_blockerView;
	UIImageView *_backgroundView;
}
- (id)initWithDialogDelegate:(NSObject <VKDialogDelegate>*)delegate;
@end
