//
//  NSMutableArray+PBAdditions.h
//  OS4
//
//  Created by Dmytro Mikheiev on 9/15/10.
//  Copyright 2010 POLAR-B. All rights reserved.
//

@interface NSMutableArray (PBAdditions)
- (BOOL) removeString:(NSString *) val;
- (BOOL) removeNumber:(NSNumber *) val;
@end
