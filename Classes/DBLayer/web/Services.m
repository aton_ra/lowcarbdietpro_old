//
//  Services.m
//
//
//  Created by polar12 on 12/7/12.
//  Copyright (c) 2012 Polar-B. All rights reserved.
//

#import "Services.h"

@implementation Services


+(Services*)getFriendsService{
    Services *service = [[Services alloc] initWithResponder:nil successSelector:nil errorSelector:nil url:@"bundle://usersMailTo.json"];   
    return service;
}
@end
