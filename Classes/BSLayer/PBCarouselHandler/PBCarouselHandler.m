//
//  PBCarouselHandler.m
//  DukanDiet
//
//  Created by polar12 on 7/8/13.
//  Copyright (c) 2013 POLAR-B. All rights reserved.
//

#import "PBCarouselHandler.h"

@interface PBCarouselHandler()

-(IBAction) selectNext:(id)sender;
-(IBAction) selectPrevious:(id)sender;


@property (nonatomic, assign) NSInteger selectedValue;


@end

@implementation PBCarouselHandler

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(IBAction) selectNext:(id)sender{
    
    if(_selectedValue < 1){
        ++_selectedValue;
    }else{
        
    }
    
    [self.delegate selectItemAtIndex: _selectedValue];
}

-(IBAction) selectPrevious:(id)sender{
    
    if(_selectedValue > 0){
        --_selectedValue;
    }
    
    [self.delegate selectItemAtIndex: _selectedValue];
}


#pragma mark Set Selected Item
-(void) selectItemAtIndex:(NSUInteger)index{
    
    _selectedValue = index;
    
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
