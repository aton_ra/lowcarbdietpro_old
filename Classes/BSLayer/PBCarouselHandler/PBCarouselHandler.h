//
//  PBCarouselHandler.h
//  DukanDiet
//
//  Created by polar12 on 7/8/13.
//  Copyright (c) 2013 POLAR-B. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PBCarouselHandler;

@protocol PBCarouselHandlerControlDelegate <NSObject>

- (void)selectItemAtIndex:(NSUInteger)index;

@end

@interface PBCarouselHandler : UIControl

@property (nonatomic, strong) id <PBCarouselHandlerControlDelegate> delegate;
@property (nonatomic, strong) UILabel *numberOfItemLabel;

-(void) selectItemAtIndex:(NSUInteger)index;

@end
