//
//  RateAppPopup.h
//  Full Marathon Trainer PRO
//
//  Created by polar12 on 9/13/13.
//  Copyright (c) 2013 POLAR-B. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RatePopupDelegate

-(void)clickedButtonAtIndex:(NSInteger)index;

@end

@interface RateAppPopup : PBPopupView

@property (nonatomic, assign) NSObject <RatePopupDelegate> *eventDelegate;
@property (retain, nonatomic) IBOutlet UIButton *yesButton;
@property (retain, nonatomic) IBOutlet UIButton *noButton;

@property (retain, nonatomic) IBOutlet UITextView *textView;
- (IBAction)yesButtonClick:(id)sender;
- (IBAction)noButtonClick:(id)sender;

-(id)initWithDelegate:(NSObject<RatePopupDelegate>*)delegate;

@end
