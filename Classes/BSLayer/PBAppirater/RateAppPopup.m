//
//  RateAppPopup.m
//  Full Marathon Trainer PRO
//
//  Created by polar12 on 9/13/13.
//  Copyright (c) 2013 POLAR-B. All rights reserved.
//

#import "RateAppPopup.h"

@implementation RateAppPopup

-(id)initWithDelegate:(NSObject<RatePopupDelegate>*)delegate
{
    
    NSArray *nibArray = nil;
    nibArray = [[NSBundle mainBundle] loadNibNamed:@"RateAppPopup" owner:self options:nil];
    UIView *nibView = nil;
    if([nibArray count] > 0){
        self.eventDelegate = delegate;
        nibView = [nibArray objectAtIndex:0];
        nibView.layer.cornerRadius = 10;
        nibView.layer.borderWidth = 3;
        nibView.layer.borderColor = [UIColor brownColor].CGColor ;
        nibView.layer.masksToBounds = YES;
        
        self = [self initWithFrame:CGRectMake(0, 0, nibView.frame.size.width, nibView.frame.size.height)];
        if(self)
            [self addSubview:nibView];
    }
    return self;

}

#pragma mark - 
#pragma mark Button Action

- (IBAction)yesButtonClick:(id)sender {
    [self.eventDelegate clickedButtonAtIndex:1];
    [self hide];
}

- (IBAction)noButtonClick:(id)sender {
    [self.eventDelegate clickedButtonAtIndex:0];
    [self hide];
}

@end
