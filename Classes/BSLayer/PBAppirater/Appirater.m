//
//  PBAppirater.m
//  QuoteMeWallpapers
//
//  Created by Dmitry Mikheev on 7/25/12.
//  Copyright (c) 2012 POLAR-B. All rights reserved.
//

#import "Appirater.h"
#import <SystemConfiguration/SCNetworkReachability.h>
#include <netinet/in.h>


#define kPBSettings_Private_PBAppirater_CurentLaunchCount                     @"kPBSettings_Private_PBAppirater_CurentLaunchCount"
#define kPBSettings_Private_PBAppirater_DeclinedToRate                        @"kPBSettings_Private_PBAppirater_DeclinedToRate"
#define kPBSettings_Private_PBAppirater_RatedCurrentVersion                   @"kPBSettings_Private_PBAppirater_RatedCurrentVersion"

NSString *templateReviewURL = /*@"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=APP_ID"*/@"http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=APP_ID&onlyLatestVersion=true&pageNumber=0&sortOrdering=1&type=Purple+Software";

@interface Appirater ()
+ (Appirater*)instance;
- (BOOL)connectedToNetwork;
- (void)showRateAppPopup;
- (BOOL)ratingConditionsHaveBeenMet;
- (void)incrementUseCount;
- (void)incrementAndRate;
@end

@implementation Appirater

@synthesize ratingAlert = _ratingAlert;
@synthesize appId = _appId;

#pragma mark
#pragma mark Public interface
//------------charge
+(void)chargeWithAppId:(NSString*)appId{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0),
                   ^{
                       //[[Appirater instance] incrementAndRate];
                       [Appirater instance].appId = appId;
                   });
}

+(void)showRatingPopup{
    
    [[Appirater instance] rateApp];
}

#pragma mark
#pragma mark Privat interface
- (BOOL)connectedToNetwork{
    return [[Reachability reachabilityForInternetConnection] currentReachabilityStatus] != NotReachable;
}

- (BOOL)ratingConditionsHaveBeenMet {
    
	if([[NSUserDefaults standardUserDefaults] integerForKey:kPBSettings_Private_PBAppirater_CurentLaunchCount]>=PBAppirater_LaunchCountForShowing &&
       ![[NSUserDefaults standardUserDefaults]boolForKey:kPBSettings_Private_PBAppirater_DeclinedToRate]  &&
       ![[NSUserDefaults standardUserDefaults]boolForKey:kPBSettings_Private_PBAppirater_RatedCurrentVersion]){
        return YES;
    }
    
    return NO;
}

-(BOOL)isAppStillNotAlreadyRated{
    if(![[NSUserDefaults standardUserDefaults]boolForKey:kPBSettings_Private_PBAppirater_DeclinedToRate]  &&
       ![[NSUserDefaults standardUserDefaults]boolForKey:kPBSettings_Private_PBAppirater_RatedCurrentVersion]){
        return YES;
    }
    return NO;
}

- (void)incrementUseCount {
	// get the app's version
    
	int previouCount = [[NSUserDefaults standardUserDefaults] integerForKey: kPBSettings_Private_PBAppirater_CurentLaunchCount];
    NSLog(@"Previous count: %d",previouCount);
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInt:++previouCount] forKey:kPBSettings_Private_PBAppirater_CurentLaunchCount];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)rateApp{
    if ([self isAppStillNotAlreadyRated] &&
		[self connectedToNetwork])
	{
        dispatch_async(dispatch_get_main_queue(),
                       ^{
                           [self showRateAppPopup];
                       });
	}
}

- (void)incrementAndRate{
	[self incrementUseCount];
	
	if ([self ratingConditionsHaveBeenMet] &&
		[self connectedToNetwork])
	{
        dispatch_async(dispatch_get_main_queue(),
                       ^{
                           [self showRateAppPopup];
                       });
	}
}

+ (Appirater*)instance {
	static Appirater *appirater = nil;
	if (appirater == nil){
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            appirater = [[Appirater alloc] init];
        });
	}
	
	return appirater;
}

+ (void)rateApp {
#if TARGET_IPHONE_SIMULATOR
	NSLog(@"APPIRATER NOTE: iTunes App Store is not supported on the iOS simulator. Unable to open App Store page.");
#else
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:YES] forKey:kPBSettings_Private_PBAppirater_RatedCurrentVersion];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //[[PBSettings instance]setPrivateSetting:NSBool(YES) forKey:kPBSettings_Private_PBAppirater_RatedCurrentVersion];
	NSString *reviewURL = [templateReviewURL stringByReplacingOccurrencesOfString:@"APP_ID" withString:[NSString stringWithFormat:@"%@", [Appirater instance].appId]];
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:reviewURL]];
#endif
}

- (void)showRateAppPopup {
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInt:0] forKey:kPBSettings_Private_PBAppirater_CurentLaunchCount];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    RateAppPopup *rateApp = [[[RateAppPopup alloc] initWithDelegate:self] autorelease];
    [rateApp show];
}

#pragma mark - PBPopupView delegate

-(void)clickedButtonAtIndex:(NSInteger)index{
    switch (index) {
		case 0:{
			// they don't want to rate it
            //[[PBSettings instance]setPrivateSetting:NSBool(YES) forKey:kPBSettings_Private_PBAppirater_DeclinedToRate];
			break;
		}
		case 1:{
			// they want to rate it
			[Appirater rateApp];
			break;
		}
		default:
			break;
	}
}

#pragma mark - AlertView delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	switch (buttonIndex) {
		case 0:{
			// they don't want to rate it
            [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:YES] forKey:kPBSettings_Private_PBAppirater_DeclinedToRate];
            [[NSUserDefaults standardUserDefaults] synchronize];
			break;
		}
		case 1:{
			// they want to rate it
			[Appirater rateApp];
			break;
		}
		default:
			break;
	}
}
@end
