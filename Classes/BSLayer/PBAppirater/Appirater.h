//
//  PBAppirater.h
//  QuoteMeWallpapers
//
//  Created by Dmitry Mikheev on 7/25/12.
//  Copyright (c) 2012 POLAR-B. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RateAppPopup.h"


//---Launch Count For Showing
#define PBAppirater_LaunchCountForShowing 5

#pragma mark Strings
//--- Your app's name.
#define APPIRATER_APP_NAME				[[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleNameKey]

//---This is the message your users will see
#define APPIRATER_MESSAGE				[NSString stringWithFormat:@"If you enjoy using %@, would you mind taking a moment to rate it? It won't take more than a minute. Thanks for your support!", APPIRATER_APP_NAME]

//---This is the title of the message alert that users will see.
#define APPIRATER_MESSAGE_TITLE         [NSString stringWithFormat:@"Rate %@", APPIRATER_APP_NAME]

//---The text of the button that rejects reviewing the app.
#define APPIRATER_CANCEL_BUTTON			@"No, Thanks"

//---Text of button that will send user to app review page.
#define APPIRATER_RATE_BUTTON			[NSString stringWithFormat:@"Rate %@", APPIRATER_APP_NAME]

//---Text for button to remind the user to review later.
#define APPIRATER_RATE_LATER			@"Remind me later"



#pragma mark interface
@interface Appirater : NSObject <UIAlertViewDelegate, RatePopupDelegate>{
    NSString *_appId;
    UIAlertView		*_ratingAlert;
}
@property(nonatomic, retain) UIAlertView *ratingAlert;
@property(nonatomic,retain) NSString* appId;

+(void)chargeWithAppId:(NSString*)appId;
+(void)showRatingPopup;

@end
