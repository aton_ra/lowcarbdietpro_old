//
//  Helper.h
//
//
//  Created by polar4 on 12/12/12.
//  Copyright (c) 2012 Polar-B. All rights reserved.
//
#import "MKStoreManager.h"

#define kPBSettings_AppLink                     @"AppLink"
#define kPBSettings_Public_EmailAddress			@"Emailaddress"
#define kPBSettings_Public_About			    @"About"
#define kPBEventOrientationChanged			    @"EventOrientationChanged"
#define kPBSettings_Public_ProductName			@"ProductName"
#define kPBSettings_Public_AppSoftwareId		@"AppSoftwareId"
#define kPBSettings_Public_Key                  @"Key"
#define kPBSettings_Public_TextForVebView       @"TermsInfoHtml"
#define kPBSettings_Public_MyBody       		@"FriendEmailBody"
#define kPBSettings_Public_ChimpKitApiKey       @"ChimpKitApiKey"
#define kPBSettings_Public_ChimpKitListId      	@"ChimpKitListId"
#define kPBSettings_Public_HasLaunchedOnce      @"HasLaunchedOnce"

#define kPBSettings_Public_ParseAppID           @"ParseAppID"
#define kPBSettings_Public_ParseClientKey       @"ParseClientKey"

#define kPBSettings_Public_NeetShowRatePopup    @"NeedShowRatePopup"
#define kPBSettings_Public_LastUpdateDate       @"LastUpdateDate"

#define isInternetConnectionAvailable [Reachability reachabilityForInternetConnection].currentReachabilityStatus != NotReachable

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#define kPBSettings_Public_FlurryKey			@"FlurryAnalyticsKey"
#define IsiOS7 [[[UIDevice currentDevice] systemVersion]doubleValue] >= 7.0
//////////////////////////////////////////(Flurry events)/////////////////////////////////////////
#define kPB_FAEvent_RecipeOpened                        @"_Recipe opened"
#define kPB_FAEvent_AddedToFavorite                     @"_Recipe added to favorite"
#define kPB_FAEvent_RemoveFromFavorite                  @"_Recipe removed from favorite"

#define kPB_FAEvent_FavoriteFilterActivated             @"_Favorite filter activated"
#define kPB_FAEvent_FavoriteFilterDeActivated             @"_Favorite filter deactivated"

#define kPB_FAEvent_WriteReviewLinkClicked              @"_Write review link clicked"
#define kPB_FAEvent_NagScreenClicked                    @"_Nag Screen clicked"
#define kPB_FAEvent_TabBarButtonClicked                 @"_Tab bar button clicked"
#define kPB_FAEvent_MoreAppsBannerClicked               @"_MoreApps banner clicked"
#define kPB_FAEvent_SignUpForUpdatesClicked             @"_Sign up for updates clicked"
#define kPB_FAEvent_AddwhirlClicked                     @"_AddWhirl banner clicked"
#define kPB_FAEvent_PhaseFilterButtonClicked            @"_Phase filter button clicked"
#define kPB_FAEvent_TermsAndConditionsClicked           @"_Terms and conditions clicked"
#define kPB_FAEvent_ContactUsClicked                    @"_Contact us clicked"
#define kPB_FAEvent_TellFriendClicked                   @"_Tell a friend clicked"
#define kPB_FAEvent_UpdateRecipesClicked                @"_Update recipes clicked"
#define kPB_FAEvent_MealTypeButtonClick                 @"_Meal type filter button clicked"
#define kPB_FAEvent_CategoryFilterButtonClicked         @"_Category filter button clicked"
#define kPB_FAEvent_AddToShoppingListButtonClicked      @"_Add to shopping list button clicked"
#define kPB_FAEvent_SendEmailRecipeButtonClicked        @"_Send email to friend button clicked"

#define kPB_FAEvent_CleanseCalendarShown                @"_Cleanse Calendar was shown"
#define kPB_FAEvent_IntroductionToPaleoDiet_Shown       @"_'An introduction to Paleo' paragraph was shown"
#define kPB_FAEvent_WhatBenefitsCanIExpect_Shown        @"_'What Benefits can i Expect?' paragraph was shown"
#define kPB_FAEvent_WhatIsPaleo_Shown                   @"_'What is Paleo?' paragraph was shown"
#define kPB_FAEvent_WhichFoodsShouldEliminate_Shown     @"_'Which foods should i Elliminate' paragraph was shown"
#define kPB_FAEvent_WhichFoodsShouldEat_Shown           @"_'Which Foods should i Eat?' paragraph was shown"
#define kPB_FAEvent_FoodLists_Shown                     @"_'Food Lists' paragraph was shown"
#define kPB_FAEvent_AboutSixWeekPlan_Shown              @"_'About the six week plan' paragraph was shown"

#define kPBSettings_Public_NumberOfWatchedRecipes       @"NumberOfWatchedRecipes"
//---------------------------------------(Purchases)------------------------------------------

#define kPB_PurchaseProductIdEmailRecipe    @"AbilityToEmailRecipes"
#define kPB_PurchaseProductIdShoppingCart   @"AbilityToUseShoppingCart"

#define isEmailRecipePurchased      [MKStoreManager isFeaturePurchased:[[PBSettings instance] app:kPB_PurchaseProductIdEmailRecipe]]
#define isShoppingCardPurchased     [MKStoreManager isFeaturePurchased:[[PBSettings instance] app:kPB_PurchaseProductIdShoppingCart]]

/////////////////////////////////////////////////////////////////
#define trackAction(actionName)  [Flurry logEvent:actionName]; NSLog(@"Trace action with name: %@",actionName);
#define trackActionWithParameters(actionName, parameters)  [Flurry logEvent:actionName withParameters:parameters]; NSLog(@"Trace action with name: %@\n and parameters :%@",actionName,parameters);

#define IsIpad  UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad