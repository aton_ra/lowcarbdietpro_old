//
//  PBParsecomManager.h
//  PaleoDiet
//
//  Created by polar12 on 8/20/13.
//  Copyright (c) 2013 Polar-B. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface PBParsecomManager : NSObject

+(PBParsecomManager*)sharedManager;

- (NSArray *)lookupAllForClass:(NSString *)className;
- (void)updateDataFromParse:(NSString *)className onComplete:(void (^)(BOOL success, NSString *message))completionBlock;

@end
