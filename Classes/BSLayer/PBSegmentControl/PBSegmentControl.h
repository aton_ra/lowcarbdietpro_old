//
//  PBSegmentControl.h
//  DukanDiet
//
//  Created by polar12 on 7/2/13.
//  Copyright (c) 2013 POLAR-B. All rights reserved.
//


#import <UIKit/UIKit.h>

@class PBSegmentControl;

@protocol PBSegmentControlDelegate <NSObject>
- (void)selectSegmentAtIndex:(NSInteger *)index;

@end

@interface PBSegmentControl : UIControl

@property (nonatomic, strong) id <PBSegmentControlDelegate> delegate;
@property (nonatomic, retain) IBOutletCollection(UIButton) NSArray *segmentControlButtons;

@property (nonatomic, assign) int activeSegmentTag;

@end
