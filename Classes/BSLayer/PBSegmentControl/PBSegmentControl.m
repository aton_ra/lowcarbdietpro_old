//
//  PBSegmentControl.m
//  DukanDiet
//
//  Created by polar12 on 7/2/13.
//  Copyright (c) 2013 POLAR-B. All rights reserved.
//

#import "PBSegmentControl.h"

@interface PBSegmentControl()

-(IBAction) selectIndexByTag:(id)sender;


@end

@implementation PBSegmentControl

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    
        // Initialization code
    }
    return self;
}

-(IBAction) selectIndexByTag:(UIView*)sender{
    
    self.activeSegmentTag = sender.tag;
    //
    [self sendActionsForControlEvents:UIControlEventValueChanged];

    //[self.delegate selectSegmentAtIndex: activeSegment.tag  - 1];
    
}

-(void)setActiveSegmentTag:(int)activeSegmentTag{
    // set state
    for (UIButton *btn in _segmentControlButtons) {
        BOOL isActiveItem = (btn.tag == activeSegmentTag);
        [btn setUserInteractionEnabled:!isActiveItem];
        [btn setSelected:isActiveItem];
    }
    _activeSegmentTag = activeSegmentTag;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
