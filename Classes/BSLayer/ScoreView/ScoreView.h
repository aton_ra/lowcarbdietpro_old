//
//  ScoreView.h
//  DukanDiet
//
//  Created by polar12 on 6/12/13.
//  Copyright (c) 2013 POLAR-B. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ScoreViewDelegate <NSObject>
//- (void)selectItemAtIndex:(NSInteger *)index;
- (void)pointsDidChanged;

@end

@interface ScoreView : UIControl
@property (assign, nonatomic) id<ScoreViewDelegate> delegate;
@property (assign, nonatomic) NSInteger value;
@property (assign, nonatomic) NSMutableString *dateLabelText;

@end

