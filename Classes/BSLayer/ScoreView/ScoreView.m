//
//  ScoreView.m
//  DukanDiet
//
//  Created by polar12 on 6/12/13.
//  Copyright (c) 2013 POLAR-B. All rights reserved.
//

#import "ScoreView.h"

@interface ScoreView ()

@property (strong,nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UITextField *scoreTextField;

-(IBAction) plus;
-(IBAction) minus;

@end

@implementation ScoreView


-(void) awakeFromNib{
    
    [self initialize];
}

-(void)initialize{

    // set current date to label
    //[self addCurrentDateDB];
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    NSString *dateString = [dateFormat stringFromDate:date];
    [self.dateLabel setText:dateString];
    
    if(IsIpad){
        [_scoreTextField setEnabled:NO];
        CGRect frameRect = _scoreTextField.frame;
        frameRect.size.height = 80;
        _scoreTextField.frame = frameRect;
        
    }
    else{
        [_scoreTextField setEnabled:NO];
        CGRect frameRect = _scoreTextField.frame;
        frameRect.size.height = 40;
        _scoreTextField.frame = frameRect;
    }
    
    // set value for score value
   
    [self refreshValueLabel];
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)orientation duration:(NSTimeInterval)duration {
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        
    }
}
-(IBAction) plus{
    
    self.value++;
    [self sendActionsForControlEvents:UIControlEventValueChanged];
}

-(IBAction) minus{
    
    self.value--;
    [self sendActionsForControlEvents:UIControlEventValueChanged];
}

- (void)refreshValueLabel {
    self.scoreTextField.text = [NSString stringWithFormat:@"%d", self.value];
}

-(void)setValue:(NSInteger )value{
    _value = value;
    [self refreshValueLabel];
    [self updatePointsDB];
}

-(void) setDateLabelText:(NSMutableString *)dateLabelText
{
    self.dateLabel.text = dateLabelText;
}

-(void) updatePointsDB
{
    NSArray *_dataSourceArray;
    
    NSString *query = [[@"SELECT * FROM Points WHERE Date = '" stringByAppendingString: [self.dateLabel text]] stringByAppendingString:@"'"];
    
    // select data from table for current date
    _dataSourceArray = [[PBDB instance].db lookupAllForSQL:query] ;
    
    NSDictionary *insertedData = [[NSDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%@",[self.dateLabel text]], @"Date", [self.scoreTextField text], @"Points", nil];
    
    // if no data in db for current date -> insert it, else update
    if(_dataSourceArray.count == 0)
    {
        [[PBDB instance].db insertDictionary:insertedData forTable:@"Points"];
    }
    else
        [[PBDB instance].db updateDictionary:insertedData forTable:@"Points" where:[[@"Date = '" stringByAppendingString:[self.dateLabel text]]stringByAppendingString:@"'" ]];
    if([[NSUserDefaults standardUserDefaults] boolForKey:kPBSettings_Public_NeetShowRatePopup]){
        [self.delegate pointsDidChanged];
    }
}

@end
