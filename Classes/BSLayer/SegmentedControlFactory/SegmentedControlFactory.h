//
//  SegmentedControlFactory.h
//  RecipeBook
//
//  Created by Dmitry Mikheev on 3/16/12.
//  Copyright (c) 2012 POLAR-B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SegmentedControlFactory : UIViewController{

}
+(UISegmentedControl*)mealTypeSegmentedControlForFilter;
+(UISegmentedControl*)phaseSegmentedControlForFilter;


@end
