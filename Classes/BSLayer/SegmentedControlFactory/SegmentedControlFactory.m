//
//  SegmentedControlFactory.m
//  RecipeBook
//
//  Created by Dmitry Mikheev on 3/16/12.
//  Copyright (c) 2012 POLAR-B. All rights reserved.
//

#import "SegmentedControlFactory.h"

@interface SegmentedControlFactory ()

@end

@implementation SegmentedControlFactory

- (id) init{
	if(self = [super init]){
		
	}
	return self;
}

+(UISegmentedControl*)mealTypeSegmentedControlForFilter{
    UISegmentedControl* segmentedControl = [[[NSBundle mainBundle] loadNibNamed:@"SegmentedControlForFilter" owner:self options:nil] objectAtIndex:0];
    
    //set width
    [segmentedControl setWidth:30 forSegmentAtIndex:0];
    [segmentedControl setWidth:80 forSegmentAtIndex:1];
    [segmentedControl setWidth:65 forSegmentAtIndex:2];
    [segmentedControl setWidth:65 forSegmentAtIndex:3];
    [segmentedControl setWidth:65 forSegmentAtIndex:4];
    
    //set offset
    if([[[UIDevice currentDevice]systemVersion] isEqualToString:@"5.1"]){
        [segmentedControl setContentOffset:CGSizeMake(1, 0) forSegmentAtIndex:0];
        [segmentedControl setContentOffset:CGSizeMake(11, 0) forSegmentAtIndex:1];
        [segmentedControl setContentOffset:CGSizeMake(8, 0) forSegmentAtIndex:2];
        [segmentedControl setContentOffset:CGSizeMake(8, 0) forSegmentAtIndex:3];
        [segmentedControl setContentOffset:CGSizeMake(8, 0) forSegmentAtIndex:4];
    }
    
    return segmentedControl;
}

+(UISegmentedControl*)phaseSegmentedControlForFilter{
    UISegmentedControl* segmentedControl = [[[NSBundle mainBundle] loadNibNamed:@"SegmentedControlForFilter" owner:self options:nil] objectAtIndex:1];
    
    //set width
    [segmentedControl setWidth:30 forSegmentAtIndex:0];
    [segmentedControl setWidth:45 forSegmentAtIndex:1];
    [segmentedControl setWidth:45 forSegmentAtIndex:2];
    [segmentedControl setWidth:95 forSegmentAtIndex:3];
    [segmentedControl setWidth:90 forSegmentAtIndex:4];
    
    //set offset
    if([[[UIDevice currentDevice]systemVersion] isEqualToString:@"5.1"]){
        [segmentedControl setContentOffset:CGSizeMake(1, 0) forSegmentAtIndex:0];
        [segmentedControl setContentOffset:CGSizeMake(1, 0) forSegmentAtIndex:1];
        [segmentedControl setContentOffset:CGSizeMake(1, 0) forSegmentAtIndex:2];
        [segmentedControl setContentOffset:CGSizeMake(2, 0) forSegmentAtIndex:3];
        [segmentedControl setContentOffset:CGSizeMake(2, 0) forSegmentAtIndex:4];
    }
    
    return [segmentedControl autorelease];
}


@end
