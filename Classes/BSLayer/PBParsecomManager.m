//
//  PBParsecomManager.m
//  PaleoDiet
//
//  Created by polar12 on 8/20/13.
//  Copyright (c) 2013 Polar-B. All rights reserved.
//

#import "PBParsecomManager.h"

@implementation PBParsecomManager

static PBParsecomManager *_sharedManager;

+ (PBParsecomManager*)sharedManager {

    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = [[self alloc] init];
    });
    return _sharedManager;
}

- (NSArray *)lookupAllForClass:(NSString *)className{
    PFQuery *query = [PFQuery queryWithClassName:className];
     NSArray *objects = [query findObjects];
    return objects;
}

- (void)updateDataFromParse:(NSString *)className onComplete:(void (^)(BOOL success, NSString *message))completionBlock{
    
    //[[PBDB instance].db deleteWhere:@"1" forTable:@"Recipes"];
        if([Reachability reachabilityForInternetConnection].currentReachabilityStatus != NotReachable){
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                __block BOOL IsUpdated = NO;

                NSFileManager *fileManager = [NSFileManager defaultManager];
                NSError *error;
                
                
                NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                NSString* file = [documentsPath stringByAppendingPathComponent:@"lastUpdate.strings"];
                
                if(![[NSFileManager defaultManager] fileExistsAtPath:file]){
                    NSString *resourcePath = [[NSBundle mainBundle] pathForResource:@"lastUpdate" ofType:@"strings"];
                    [fileManager copyItemAtPath:resourcePath toPath:file error:&error];
                }
                
                [PFUser enableAutomaticUser];
                
                PFACL *defaultACL = [PFACL ACL];
                [defaultACL setPublicReadAccess:YES];
                [PFACL setDefaultACL:defaultACL withAccessForCurrentUser:YES];
                
                NSDictionary *lastUpdate;
                if([[NSFileManager defaultManager] fileExistsAtPath:file]){
                    lastUpdate = [NSDictionary dictionaryWithContentsOfFile:file];
                }else{
                    NSString *fname = [[NSBundle mainBundle] pathForResource:@"lastUpdate" ofType:@"strings"];
                    lastUpdate = [NSDictionary dictionaryWithContentsOfFile:fname];
                }
                
                NSString *str = [lastUpdate valueForKey:@"LastUpdate"];
                
                PFQuery *query = [PFQuery queryWithClassName:@"LastUpdateDate"];
                
                [query setLimit:1];
                    NSArray *obj = [[NSArray alloc] initWithArray:[query findObjects]];
                         
                     if(obj.count > 0){
                         NSString *date1 = [NSString stringWithString:[[obj objectAtIndex:0] valueForKey:@"LastUpdate"]];
                         if(![str isEqualToString:date1]){
                             
                             PFQuery *query = [PFQuery queryWithClassName:@"Recipes"];
                             [query setLimit:1000];
                             
                                 NSMutableArray *pfObjects = [[NSMutableArray alloc] initWithArray:[query findObjects]];
                                 NSMutableArray *dbObjects = [[NSMutableArray alloc] initWithArray:[[PBDB instance].db lookupAllForSQL:@"SELECT * FROM Recipes"]];
                                 
                                 BOOL isItemExist = NO;
                         
                                 if(pfObjects.count > 0){ // update recipe
                                     for (NSDictionary *dbDic in dbObjects) {
                                         NSMutableDictionary *mutDic = [[NSMutableDictionary alloc] initWithDictionary:dbDic];
                                         isItemExist = NO;
                                         for (PFObject *pfObj in pfObjects) {
                                             if([[mutDic valueForKey:@"objectId"] isEqualToString: [pfObj valueForKey:@"objectId"]]){
                                                 
                                                 isItemExist = YES;
                                                 // update recipe
                                                NSNumber * num = [NSNumber numberWithDouble:[[pfObj valueForKey:@"updatedAt"] timeIntervalSince1970]];

                                                 if(![[mutDic objectForKey:@"updatedAt"] isEqualToString:[num stringValue]]){
                                                    
                                                     PFFile *theImage = [pfObj valueForKey:@"Image"];
                                                     NSString *imageName = [theImage name];
                                                     NSData *imageData = [theImage getData];
                                                     UIImage *image = [UIImage imageWithData:imageData];
                                                     [self removeImageWithFileName:[mutDic valueForKey:@"Image"]];
                                                     [self saveImage:image withFileName:imageName];
                                            
                                                     [mutDic setValue:[pfObj valueForKey:@"DishName"] forKey:@"DishName"];
                                                     [mutDic setValue:imageName forKey:@"Image"];
                                                     [mutDic setValue:[pfObj valueForKey:@"DetailDescription"] forKey:@"DetailDescription"];
                                                     [mutDic setValue:[pfObj valueForKey:@"Ingredients"] forKey:@"Ingredients"];
                                                     [mutDic setValue:[pfObj valueForKey:@"Preparation"] forKey:@"Preparation"];
                                                     [mutDic setValue:[pfObj valueForKey:@"NutritionalInfo"] forKey:@"NutritionalInfo"];
                                                     [mutDic setValue:[pfObj valueForKey:@"PreparationTimeInMins"] forKey:@"PreparationTimeInMins"];
                                                     [mutDic setValue:[pfObj valueForKey:@"ServingsCount"] forKey:@"ServingsCount"];
                                                     [mutDic setValue:[pfObj valueForKey:@"MealType"] forKey:@"MealType"];
         
                                                     [mutDic setValue:[pfObj valueForKey:@"objectId"] forKey:@"objectId"];
                                                     [mutDic setValue:[num stringValue] forKey:@"updatedAt"];
                                                     [[PBDB instance].db updateDictionary:mutDic forTable:@"Recipes" where: [[@"objectId = '" stringByAppendingString:[mutDic valueForKey: @"objectId"]]stringByAppendingString:@"'" ]];
                                                     IsUpdated = YES;
                                                 }
                                                 [pfObjects removeObject:pfObj];
                                                 break;
                                             }
                                         }
                                         // this item was deleted from parse.com -> delete if from DB
                                         if(!isItemExist){
                                             [[PBDB instance].db deleteWhere:[[@"DishName = '" stringByAppendingString:[mutDic valueForKey: @"DishName"]]stringByAppendingString:@"'" ] forTable:@"Recipes"];
                                         }
                                     }
                                 }
                                 
                                 // if in pfObjects exist objects - insert them to DB - they are new
                                 NSMutableDictionary *dbDic = [[NSMutableDictionary alloc] init];
                                 NSLog(@"Count of new objects: %d",[pfObjects count]);
                                 for (PFObject *pfObj in pfObjects){
                                     
                                     PFFile *theImage = [pfObj objectForKey:@"Image"];
                                     NSString *imageName =  [theImage name]/*[pfObj valueForKey:@"DishName"]*/;
                                     NSData *imageData = [theImage getData];
                                     UIImage *image = [UIImage imageWithData:imageData];
                                     
                                     [self saveImage:image withFileName:imageName];
                                     
                                     NSNumber *num = [NSNumber numberWithDouble:[[pfObj valueForKey:@"updatedAt"] timeIntervalSince1970]];
                                     [dbDic setValue:[pfObj valueForKey:@"DishName"] forKey:@"DishName"];
                                     [dbDic setValue:imageName forKey:@"Image"];
                                     [dbDic setValue:[pfObj valueForKey:@"DetailDescription"] forKey:@"DetailDescription"];
                                     [dbDic setValue:[pfObj valueForKey:@"Ingredients"] forKey:@"Ingredients"];
                                     [dbDic setValue:[pfObj valueForKey:@"NutritionalInfo"] forKey:@"NutritionalInfo"];
                                     [dbDic setValue:[pfObj valueForKey:@"Preparation"] forKey:@"Preparation"];
                                     [dbDic setValue:[pfObj valueForKey:@"PreparationTimeInMins"] forKey:@"PreparationTimeInMins"];
                                     [dbDic setValue:[pfObj valueForKey:@"ServingsCount"] forKey:@"ServingsCount"];
                                     [dbDic setValue:[pfObj valueForKey:@"MealType"] forKey:@"MealType"];

                                     [dbDic setValue:[pfObj valueForKey:@"objectId"] forKey:@"objectId"];
                                     [dbDic setValue:[num stringValue] forKey:@"updatedAt"];
                                     [[PBDB instance].db insertDictionary:dbDic forTable:@"Recipes"];
                                     IsUpdated = YES;
                                 }
                                 
                                 NSLog(@"Recipes Finished");

                             NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                             NSString* file = [documentsPath stringByAppendingPathComponent:@"lastUpdate.strings"];
                             NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:date1, @"LastUpdate", nil];
                             [dic writeToFile:file atomically:YES];
                             NSDictionary *lastUpdate2 = [NSDictionary dictionaryWithContentsOfFile:file];
                             
                             NSLog(@"Last update from documents 2: %@",[lastUpdate2 valueForKey:@"LastUpdate"]);
                            
                             //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Msg" message:date1 delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                             //[alert show];
                             
                             if(completionBlock){
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     
                                     if(IsUpdated){
                                         completionBlock(YES, date1);//@"Data base succesfull updated"
                                     }else{
                                         completionBlock(NO, date1);//@"Data base is up to date"
                                     }
                                 });
                             }
                         }
                         else{
                             
                             NSLog(@"Up to date");
                             /*if(completionBlock){
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                    completionBlock(YES, @"DB is up to date");
                                 });
                             }*/
                         }
                     }
            });
        }else{
            NSLog(@"No internet connection !");
    }
}

-(void) removeImageWithFileName: (NSString*)imageName{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    [[NSFileManager defaultManager] removeItemAtPath:[documentsPath stringByAppendingPathComponent:imageName] error:nil];
}

-(BOOL) saveImage:(UIImage *)image withFileName:(NSString *)imageName{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
    
    if ([[imageName lowercaseString] containsString:@"png"]) {
        [UIImagePNGRepresentation(image) writeToFile:[documentsPath stringByAppendingPathComponent:imageName] atomically:YES];
    } else if ([[imageName lowercaseString] containsString:@"jpg"]) {
        [UIImageJPEGRepresentation(image, 1.0) writeToFile:[documentsPath stringByAppendingPathComponent:imageName] atomically:YES];
    } else {
        NSLog(@"Image Save Failed\nExtension: is not recognized");
        return NO;
    }
    
    return YES;
}

@end
