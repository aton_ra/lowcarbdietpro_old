//
//  ImageGridCell.m
//  SelectionDelegateExample
//
//  Created by orta therox on 06/11/2012.
//  Copyright (c) 2012 orta therox. All rights reserved.
//

#import "ImageGridCell.h"

//180 на 131
static UIEdgeInsets ContentInsets = { .top = 20, .left = 6, .right = 6, .bottom = 0 };
CGSize cellImageSize = { .height = 131, .width = 180 };
static CGFloat SubTitleLabelHeight = 31;
static CGFloat SubTitleLabelWidth = 131;

#define kBoarderWidth 2.0
#define kCornerRadius 0.0

@implementation ImageGridCell

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        _cellView = [[UIView alloc] init];
        _cellView.backgroundColor = [UIColor blackColor];
        
        _backImage = [[UIImageView alloc] init];
        _backImage.contentMode = UIViewContentModeScaleAspectFit;
        _backImage.backgroundColor = [UIColor clearColor];
        //UIViewContentModeScaleFit
        _image = [[UIImageView alloc] init];
        _image.contentMode = UIViewContentModeScaleToFill;
        _image.backgroundColor = [UIColor clearColor];
        
        _label = [[UILabel alloc] init];
        _label.textColor = [UIColor blackColor];
        _label.textAlignment = NSTextAlignmentCenter;
        _label.backgroundColor = [UIColor clearColor];
        [_label setFont:[UIFont systemFontOfSize:13]];
        _label.lineBreakMode = NSLineBreakByWordWrapping;
        _label.numberOfLines = 0;
        
        _starButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_starButton setImage:[UIImage imageNamed:@"star_frame_foto"] forState:UIControlStateNormal];
        [_starButton setBackgroundColor:[UIColor clearColor]];
        
        [self.contentView addSubview:_cellView];
        [self.contentView addSubview:_image];
        [self.contentView addSubview:_backImage];
        [self.contentView addSubview:_label];
        [self.contentView addSubview:_starButton];
    }
    return self;
}

- (void)layoutSubviews {
    
    //CGFloat imageHeight = CGRectGetHeight(self.bounds) - ContentInsets.top - SubTitleLabelHeight - ContentInsets.bottom;
    //CGFloat imageWidth = CGRectGetWidth(self.bounds) - ContentInsets.left - ContentInsets.right;
    
    // 183*126
    _image.frame = CGRectMake(ContentInsets.left+9, ContentInsets.top, cellImageSize.width-15, cellImageSize.height-15);
    _label.frame = CGRectMake(ContentInsets.left, _image.frame.size.height-2, /*cellImageSize.width-cellImageSize.width/4*/SubTitleLabelWidth, SubTitleLabelHeight);
    _cellView.frame = CGRectMake(10, ContentInsets.top-2, cellImageSize.width-5, cellImageSize.height-8);
    _backImage.frame = CGRectMake(0, ContentInsets.top-10, cellImageSize.width+15, cellImageSize.height+5);
    _starButton.frame = CGRectMake(cellImageSize.width-13 , ContentInsets.top-5, 24, 24);
}

- (void)setHighlighted:(BOOL)highlighted {
    //NSLog(@"Cell %@ highlight: %@", _label.text, highlighted ? @"ON" : @"OFF");
   /* if (highlighted) {
        _label.backgroundColor = [UIColor whiteColor];
        
    }
    else {
        _label.backgroundColor = [UIColor whiteColor];
    }*/
}

@end
