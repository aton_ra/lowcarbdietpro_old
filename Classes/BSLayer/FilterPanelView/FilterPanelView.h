//
//  CategoriesView.h
//  OS4
//
//  Created by Dmytro Mikheiev on 3/16/11.
//  Copyright 2011 POLAR-B. All rights reserved.
//

#import <Foundation/Foundation.h>

#define FilterPanelView_indent_iphone				32

typedef enum{
	FilterRecipeTypeAll = 0,
	FilterRecipeTypeFavorite = 1,
	FilterRecipeTypeNotFavorite = 2
}FilterRecipeType;

typedef enum{
	FilterMealTypeAll = 0,
	FilterMealTypeBreakfast = 1,
    FilterMealTypeLunch = 2,
   	FilterMealTypeDinner = 3,
   	FilterMealTypeSnack = 4,
}FilterMealType;

/*typedef enum{
	MainIngredientAll = 0,
	MainIngredientPork = 1,
    MainIngredientFruit = 2,
   	MainIngredientBeef = 3,
   	MainIngredientVeg = 4,
    MainIngredientEggs = 5,
    MainIngredientNutsAndSeeds = 6,
    MainIngredientChicken = 7,
    MainIngredientLamb = 8,
    MainIngredientFishAndSeafood = 9,
    MainIngredientDressing = 10,
    MainIngredientDuck = 11,
}FilterMainIngredient;

typedef enum{
	EffectAll = 0,
	EffectProteinKick = 1,
    EffectCalcium = 2,
   	EffectRehydrate = 3,
   	EffectAntyOxidant = 4,
    EffectIndulge = 5,
    EffectVitaminE = 6,
}FilterEffect;

typedef enum{
	CuisineAll = 0,
	CuisineClassic = 1,
    CuisineBistro = 2,
   	CuisineMexican = 3,
   	CuisineAmerican = 4,
    CuisineMiddleEastern = 5,
    CuisineAsian = 6,
}FilterCuisine;

typedef enum{
	CookingMethod30MinMeal = 0,
	CookingMethodSuperQuick = 1,
    CookingMethodNoCook = 2,
   	CookingMethodSnake = 3,
   	CookingMethodSlowSimmer = 4,
    CookingMethodFastPanFry = 5,
    CookingMethodQuickBake = 6,
    CookingMethodSlowBake = 7,
    CookingMethodGrill = 8,
}FilterCookingMethod;*/

@protocol FilterPanelViewDelegate
@required
- (void)filterChanged;
@end

@interface FilterPanelView : UIView <UIGestureRecognizerDelegate> {
	id<FilterPanelViewDelegate> _delegate;
	
	UIImageView *_bottomPanelImageView;
	UIImageView *_arrowImageView;
    
    UISwitch *_starredSwitch;
	UISlider *_durationSlider;
    UILabel *_sliderLabel;
   	UISegmentedControl *_mealTypeSegment;
    UISegmentedControl *_phaseSegment;
    
	UIView *_handlerView;
	UIImageView *_backgroundImageView;
	UIButton *_panelButton;
	
	int _previousMoveTop;
	BOOL _isTopDirection;
	NSTimer *_scrollTimer;
    int _previousSliderValue;
}
- (id)initWithPanelDelegate:(id<FilterPanelViewDelegate>)delegate;

- (FilterRecipeType)getRecipeType;
- (FilterMealType)getMealType;
- (int)getDuration;

@end
