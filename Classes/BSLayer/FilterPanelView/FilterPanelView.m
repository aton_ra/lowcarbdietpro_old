//
//  CategoriesView.m
//  OS4
//
//  Created by Dmytro Mikheiev on 3/16/11.
//  Copyright 2011 POLAR-B. All rights reserved.
//

#import "FilterPanelView.h"

@implementation FilterPanelView
- (id)initWithPanelDelegate:(id<FilterPanelViewDelegate>)delegate{
	if(self = [super initWithFrame:CGRectMake(0, 0, 320, FilterPanelView_indent_iphone)]){
		_delegate = delegate;
		
		self.clipsToBounds = YES;
		_handlerView = [[UIView alloc] initWithFrame:CGRectMake(0,FilterPanelView_indent_iphone,320,0)];
		[self addSubview:_handlerView];
		
		_backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pattern_for_drop_panel_open.jpg"]];
		_backgroundImageView.frame = CGRectMake(0,0,320,0);
		[_handlerView addSubview:_backgroundImageView];
		
		_bottomPanelImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"down_panel.png"]];
		_bottomPanelImageView.contentMode = UIViewContentModeScaleAspectFit;
		[_handlerView addSubview:_bottomPanelImageView];
		
		_arrowImageView = [[UIImageView alloc] init];
		_arrowImageView.image = [UIImage imageNamed:@"arrow_down_panel.png"];
		_arrowImageView.contentMode = UIViewContentModeCenter;
		[_handlerView addSubview:_arrowImageView];
		
		_panelButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[_panelButton addTarget:self action:@selector(panelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
		[_handlerView addSubview:_panelButton];
        
		UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dragArrow:)];
		[panGesture setMaximumNumberOfTouches:2];
		[panGesture setDelegate:self];
		[_panelButton addGestureRecognizer:panGesture];
		RELEASE_SAFELY(panGesture);
		
		int categoryViewHeight = 260;
		_handlerView.frame = CGRectMake(0,-categoryViewHeight,320,categoryViewHeight+FilterPanelView_indent_iphone);
		_backgroundImageView.height = categoryViewHeight;
		_bottomPanelImageView.frame = CGRectMake(0,_handlerView.height-34,320,34);
		_arrowImageView.frame = CGRectMake(320/2-7,_handlerView.height-FilterPanelView_indent_iphone+5, 15, 14);
		_panelButton.frame = CGRectMake(140,_handlerView.height-40,40,44);
		
		//******************* creating components *****************
		UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,5,320,30)];
		titleLabel.text = @"Filtered Recipes";
		titleLabel.backgroundColor = [UIColor clearColor];
		titleLabel.textAlignment = UITextAlignmentCenter;
		titleLabel.font = [UIFont boldSystemFontOfSize:18];
		[_handlerView addSubview:titleLabel];
		RELEASE_SAFELY(titleLabel);
		
		UILabel *mealTypeLabel = [[UILabel alloc] initWithFrame:CGRectMake(10,40,100,30)];
		mealTypeLabel.text = @"By meal type:";
		mealTypeLabel.backgroundColor = [UIColor clearColor];
		mealTypeLabel.font = [UIFont boldSystemFontOfSize:14];
		[_handlerView addSubview:mealTypeLabel];
		RELEASE_SAFELY(mealTypeLabel);
		
        UILabel *durationLabel = [[UILabel alloc] initWithFrame:CGRectMake(10,110,200,30)];
		durationLabel.text = @"Preparation Time:";
		durationLabel.backgroundColor = [UIColor clearColor];
		durationLabel.font = [UIFont boldSystemFontOfSize:14];
		[_handlerView addSubview:durationLabel];
		RELEASE_SAFELY(durationLabel);
        
        UILabel *userTypeLabel = [[UILabel alloc] initWithFrame:CGRectMake(10,180,100,30)];
		userTypeLabel.text = @"Starred:";
		userTypeLabel.backgroundColor = [UIColor clearColor];
		userTypeLabel.font = [UIFont boldSystemFontOfSize:14];
		[_handlerView addSubview:userTypeLabel];
		RELEASE_SAFELY(userTypeLabel);
        
        _starredSwitch = [[UISwitch alloc] init];
        _starredSwitch.top = 210;
        _starredSwitch.centerX = 320/2;
        [_starredSwitch addTarget:self action:@selector(switchValueChanged:) forControlEvents:UIControlEventValueChanged];
        [_handlerView addSubview:_starredSwitch];
        
        // mealType filter
		_mealTypeSegment = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"All",@"Breakfast",@"Lunch",@"Dinner",@"Snack", nil]];
        _mealTypeSegment.segmentedControlStyle = UISegmentedControlStyleBar;
        _mealTypeSegment.tintColor = [UIColor darkGrayColor];
        _mealTypeSegment.selectedSegmentIndex = 0;
        
        [_mealTypeSegment setWidth:30 forSegmentAtIndex:0];
        [_mealTypeSegment setWidth:80 forSegmentAtIndex:1];
        [_mealTypeSegment setWidth:65 forSegmentAtIndex:2];
        [_mealTypeSegment setWidth:65 forSegmentAtIndex:3];
        [_mealTypeSegment setWidth:65 forSegmentAtIndex:4];
        
		_mealTypeSegment.frame = CGRectMake(5, 70, 300, 30);
        [_mealTypeSegment addTarget:self action:@selector(filterValueChanged:) forControlEvents:UIControlEventValueChanged];
		[_handlerView addSubview:_mealTypeSegment];
        
        _sliderLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 140, 50, 25)];
        _sliderLabel.backgroundColor = [UIColor clearColor];
        _sliderLabel.textAlignment = UITextAlignmentRight;
        _sliderLabel.font = [UIFont boldSystemFontOfSize:12];
        _sliderLabel.text = @"All";
        [_handlerView addSubview:_sliderLabel];
        
        NSArray *responseArray = [[NSMutableArray alloc]initWithArray:[[PBDB instance].db lookupAllForSQL:@"SELECT * FROM RecipesNew"]];
        NSInteger maxPreparationTime = 0;
        
        for (NSDictionary *dic in responseArray) {
            if(maxPreparationTime < [[dic valueForKey:@"PreparationTimeInMins"] intValue])
                maxPreparationTime = [[dic valueForKey:@"PreparationTimeInMins"] intValue];
        }
    
        //set preparationTimeFilter slider
        _durationSlider = [[UISlider alloc] initWithFrame:CGRectMake(70, 140, 240, 25)];
        _durationSlider.maximumValue = maxPreparationTime;
        _durationSlider.minimumValue = 0;
        [_durationSlider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
		[_handlerView addSubview:_durationSlider];
	}
	return self;
}

- (void)panelButtonClicked{
	// choose direction
	int stepToScroll = 0;
	if(self.height == FilterPanelView_indent_iphone){
		stepToScroll = 4;
	}else{
		stepToScroll = -30;
	}
	double animationSpeed = 1/30 ;
	if(stepToScroll>0){ // go down. should be slower
		animationSpeed = 1/15;
	}
	
	// start scrolling
	RELEASE_TIMER_SAFELY(_scrollTimer);
	_scrollTimer = [NSTimer timerWithTimeInterval:animationSpeed
										   target:self
										 selector:@selector(_scrollCategory:)
										 userInfo:[NSNumber numberWithInt:stepToScroll]
										  repeats:YES];
	[[NSRunLoop mainRunLoop] addTimer:_scrollTimer forMode:NSRunLoopCommonModes];
}
- (void)_scrollCategory:(NSTimer *)sender{
	
	int indent = [sender.userInfo intValue];
	
	if(_handlerView.top + indent < FilterPanelView_indent_iphone-_handlerView.height){
		indent = FilterPanelView_indent_iphone-_handlerView.height-_handlerView.top;
	}
	if(_handlerView.top + indent > 0){
		indent = 0-_handlerView.top;
	}
	self.height += indent;
	_handlerView.top += indent;
	
	// rotate arrow
	float angleToRotate = (self.height-FilterPanelView_indent_iphone)/(_handlerView.height-FilterPanelView_indent_iphone);
	_arrowImageView.transform = CGAffineTransformMakeRotation(M_PI * angleToRotate);
	
	if(_handlerView.top <= FilterPanelView_indent_iphone-_handlerView.height ||
	   _handlerView.top >= 0){
		RELEASE_TIMER_SAFELY(_scrollTimer);
	}
}
- (void)dragArrow:(UIPanGestureRecognizer *)gestureRecognizer{
	UIView *piece = [gestureRecognizer view];
	
	
	if ([gestureRecognizer state] == UIGestureRecognizerStateBegan){
		_previousMoveTop = 0;
		
		// set active images for panel and arrow
		//_bottomPanelImageView.image = [UIImage imageNamed:@"drop_down_panel_pushed.png"];
		//_arrowImageView.image = [UIImage imageNamed:@"drop_down_arrow_down_pushed.png"];
		
		// stop scroller if it's run
		if(_scrollTimer){
			RELEASE_TIMER_SAFELY(_scrollTimer);
		}
	}
	if (	[gestureRecognizer state] == UIGestureRecognizerStateChanged) {
		
		CGPoint translation = [gestureRecognizer translationInView:[piece superview]];
		int indent = translation.y - _previousMoveTop;
		_isTopDirection = indent < 0;
		
		// make top amd bottom frames for scrolling
		if(_handlerView.top + indent < FilterPanelView_indent_iphone-_handlerView.height){
			indent = FilterPanelView_indent_iphone-_handlerView.height-_handlerView.top;
		}
		if(_handlerView.top + indent > 0){
			indent = 0-_handlerView.top;
		}
		
		// make scrolling. move content view and make bigger self view
		self.height += indent;
		_handlerView.top += indent;
		_previousMoveTop = translation.y;
		
		// rotate arrow
		float angleToRotate = (self.height-FilterPanelView_indent_iphone)/(_handlerView.height-FilterPanelView_indent_iphone);
		_arrowImageView.transform = CGAffineTransformMakeRotation(M_PI * angleToRotate);
	}
	if ([gestureRecognizer state] == UIGestureRecognizerStateCancelled ||
		[gestureRecognizer state] == UIGestureRecognizerStateEnded){
		
		// disactivate the images
		//_bottomPanelImageView.image = [UIImage imageNamed:@"drop_down_panel.png"];
		//_arrowImageView.image = [UIImage imageNamed:@"drop_down_arrow_down.png"];
		
		// if scroller already at the final position - do nothing
		if(_handlerView.top == FilterPanelView_indent_iphone-_handlerView.height ||
		   _handlerView.top == 0){
			return;
		}
		
		// determine to whitch direction we should scroll
		int stepToScroll = 0;
		if(!_isTopDirection){
			stepToScroll = 10;
		}else{
			stepToScroll = -10;
		}
		
		// run scroller timer
		_scrollTimer = [NSTimer timerWithTimeInterval:1/25
											   target:self
											 selector:@selector(_scrollCategory:)
											 userInfo:[NSNumber numberWithInt:stepToScroll]
											  repeats:YES];
		[[NSRunLoop mainRunLoop] addTimer:_scrollTimer forMode:NSRunLoopCommonModes];
	}
}
- (void)filterValueChanged:(UISegmentedControl *)sender{
	[_delegate filterChanged];
    
    NSString *eventName = [NSString stringWithFormat:@"%@: %@",kPB_FAEvent_MealTypeButtonClick,[sender titleForSegmentAtIndex:sender.selectedSegmentIndex]];
    trackAction(eventName);
}
- (void)switchValueChanged:(UISwitch *)sender{
    [_delegate filterChanged];
}
- (void)sliderValueChanged:(UISlider *)sender{
    if((int)sender.value==0){
        _sliderLabel.text = @"All";
    }else{
        _sliderLabel.text = [NSString stringWithFormat:@"%d mins",(int)sender.value];
    }
    if(_previousSliderValue != (int)sender.value){
        _previousSliderValue = (int)sender.value;
        [_delegate filterChanged];
    }
}
- (FilterRecipeType)getRecipeType {
    return _starredSwitch.isOn;
}
- (FilterMealType)getMealType {
    return _mealTypeSegment.selectedSegmentIndex;
}

- (int)getDuration{
    return _durationSlider.value;
}
@end
