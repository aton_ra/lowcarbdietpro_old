//
//  BPMainMenuSegmentControl.h
//  DukanDiet
//
//  Created by polar12 on 7/2/13.
//  Copyright (c) 2013 POLAR-B. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PBSegmentControl;

@interface PBMainMenuSegmentControl : UIControl

@property (nonatomic, retain) IBOutletCollection(UIButton) NSArray *segmentControlButtons;
@property (nonatomic, assign) int activeSegmentTag;

@end
