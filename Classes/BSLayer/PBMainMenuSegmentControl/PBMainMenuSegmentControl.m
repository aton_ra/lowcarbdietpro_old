//
//  BPMainMenuSegmentControl.m
//  DukanDiet
//
//  Created by polar12 on 7/2/13.
//  Copyright (c) 2013 POLAR-B. All rights reserved.
//

#import "PBMainMenuSegmentControl.h"

@interface PBMainMenuSegmentControl()

-(IBAction) selectIndexByTag:(id)sender;

@end

@implementation PBMainMenuSegmentControl

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


-(IBAction) selectIndexByTag:(UIView*)sender{
    //
    self.activeSegmentTag = sender.tag;
    //
    [self sendActionsForControlEvents:UIControlEventValueChanged];
    
}

-(void)setActiveSegmentTag:(int)activeSegmentTag{
    // set state
    for (UIButton *btn in _segmentControlButtons) {
        BOOL isActiveItem = (btn.tag == activeSegmentTag);
        [btn setUserInteractionEnabled:!isActiveItem];
        [btn setSelected:isActiveItem];
    }
    _activeSegmentTag = activeSegmentTag;
}



@end
