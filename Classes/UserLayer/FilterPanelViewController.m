//
//  FilterPanelViewController.m
//  PaleoDiet
//
//  Created by polar12 on 9/2/13.
//  Copyright (c) 2013 Polar-B. All rights reserved.
//

#import "FilterPanelViewController.h"
#import "RecipesViewController.h"

@interface FilterPanelViewController (){
    RecipesViewController *recipesViewController;
    NSArray *_dataSource;
    NSArray *_dataSourceArray;
    
    int currentDuration;
    FilterRecipeTypes _recipeTypes;
    FilterMealTypes _mealType;
    FilterMainIngredient _mainIngredient;
    FilterEffect _effect;
    FilterCuisine _cuisine;
    FilterCookingMethod _cookingMethod;
}
@end

@implementation FilterPanelViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _dataSourceArray = [[NSArray alloc] initWithObjects:
                        
                        [NSMutableDictionary dictionaryWithDictionary:@{
                         @"Title":@"Starred",
                         }],
                        [NSMutableDictionary dictionaryWithDictionary:@{
                         @"Title":@"Preparation Time",
                         }],
                        [NSMutableDictionary dictionaryWithDictionary:@{
                         @"Title":@"Meal Type",
                         @"Rows":[[NSArray alloc] initWithObjects:@"All", @"Breakfast", @"Dinner", @"Lunch", @"Snake", nil],
                         @"IsSelected":[[NSMutableArray alloc] initWithObjects:[NSNumber numberWithBool:YES],
                                                                        [NSNumber numberWithBool:NO],
                                                                        [NSNumber numberWithBool:NO],
                                                                        [NSNumber numberWithBool:NO],
                                                                        [NSNumber numberWithBool:NO],nil]
                         }],
                        [NSMutableDictionary dictionaryWithDictionary:@{
                         @"Title":@"Main ingredient",
                         @"Rows":[[NSArray alloc] initWithObjects:@"All", @"Pork", @"Fruit", @"Beef", @"Veg", @"Eggs", @"Nuts and Seeds", @"Chicken", @"Lamb", @"Fish and Seafood", @"Dressing", @"Duck", nil],
                         @"IsSelected":[[NSMutableArray alloc] initWithObjects:[NSNumber numberWithBool:YES],
                                                                        [NSNumber numberWithBool:NO],
                                                                        [NSNumber numberWithBool:NO],
                                                                        [NSNumber numberWithBool:NO],
                                                                        [NSNumber numberWithBool:NO],
                                                                        [NSNumber numberWithBool:NO],
                                                                        [NSNumber numberWithBool:NO],
                                                                        [NSNumber numberWithBool:NO],
                                                                        [NSNumber numberWithBool:NO],
                                                                        [NSNumber numberWithBool:NO],
                                                                        [NSNumber numberWithBool:NO],
                                                                        [NSNumber numberWithBool:NO],nil]
                         }],
                        [NSMutableDictionary dictionaryWithDictionary:@{
                         @"Title":@"Effect",
                         @"Rows":[[NSArray alloc] initWithObjects:@"All", @"Protein Kick", @"Calcium", @"Re-hydrate", @"Anti-oxidant", @"Indulge", @"Vitamin E", nil],
                         @"IsSelected":[[NSMutableArray alloc] initWithObjects:[NSNumber numberWithBool:YES],
                                                                        [NSNumber numberWithBool:NO],
                                                                        [NSNumber numberWithBool:NO],
                                                                        [NSNumber numberWithBool:NO],
                                                                        [NSNumber numberWithBool:NO],
                                                                        [NSNumber numberWithBool:NO],
                                                                        [NSNumber numberWithBool:NO],nil]
                         }],
                        [NSMutableDictionary dictionaryWithDictionary:@{
                         @"Title":@"Cuisine",
                         @"Rows":[[NSArray alloc] initWithObjects:@"All", @"Classic", @"Bistro", @"Mexican", @"American", @"Middle Eastern", @"Asian", nil],
                         @"IsSelected":[[NSMutableArray alloc] initWithObjects:[NSNumber numberWithBool:YES],
                                                                        [NSNumber numberWithBool:NO],
                                                                        [NSNumber numberWithBool:NO],
                                                                        [NSNumber numberWithBool:NO],
                                                                        [NSNumber numberWithBool:NO],
                                                                        [NSNumber numberWithBool:NO],
                                                                        [NSNumber numberWithBool:NO],nil]
                         }],
                        [NSMutableDictionary dictionaryWithDictionary:@{
                         @"Title":@"Cooking method",
                         @"Rows":[[NSArray alloc] initWithObjects:@"All", @"30 minute meal", @"Super Quick", @"No Cook", @"Slow Simmer", @"Fast Pan Fry", @"Quick Bake", @"Slow Bake", @"Grill", nil],
                         @"IsSelected":[[NSMutableArray alloc] initWithObjects:[NSNumber numberWithBool:YES],
                                                                        [NSNumber numberWithBool:NO],
                                                                        [NSNumber numberWithBool:NO],
                                                                        [NSNumber numberWithBool:NO],
                                                                        [NSNumber numberWithBool:NO],
                                                                        [NSNumber numberWithBool:NO],
                                                                        [NSNumber numberWithBool:NO],
                                                                        [NSNumber numberWithBool:NO],
                                                                        [NSNumber numberWithBool:NO],nil]
                         }]
                        ,nil];
    
    [__tableView setWidth:280];
    [self.view setWidth:280];
    [super.view setWidth:280];
   /* _filterDataSource = [NSArray arrayWithObjects:
                         [NSArray arrayWithObjects:              //is favorite section
                          [NSMutableDictionary dictionaryWithObjectsAndKeys:NSBool(NO), @"starred", @"starredCell", @"type", nil],
                          nil],
                         [NSArray arrayWithObjects:              //meal type section
                          [NSMutableDictionary dictionaryWithObjectsAndKeys:@"All", @"title", NSBool(YES), @"isSelected", @"filterCell", @"type", nil],
                          [NSMutableDictionary dictionaryWithObjectsAndKeys:@"Breakfast", @"title", NSBool(NO), @"isSelected", @"filterCell", @"type", nil],
                          [NSMutableDictionary dictionaryWithObjectsAndKeys:@"Lunch", @"title", NSBool(NO), @"isSelected", @"filterCell", @"type", nil],
                          [NSMutableDictionary dictionaryWithObjectsAndKeys:@"Dinner", @"title", NSBool(NO), @"isSelected", @"filterCell", @"type", nil],
                          [NSMutableDictionary dictionaryWithObjectsAndKeys:@"Snack", @"title", NSBool(NO), @"isSelected", @"filterCell", @"type", nil],
                          nil],
                         [NSArray arrayWithObjects:              //duration
                          [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:0], @"duration", @"durationCell", @"type", nil],
                          nil],
                         
                         nil] ;*/
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -
#pragma mark SASlideMenuDataSource
// The SASlideMenuDataSource is used to provide the initial segueid that represents the initial visibile view controller and to provide eventual additional configuration to the menu button

// This is the indexPath selected at start-up
-(NSIndexPath*) selectedIndexPath{
    return [NSIndexPath indexPathForRow:0 inSection:0];
}

-(NSString*) segueIdForIndexPath:(NSIndexPath *)indexPath{
    //if (indexPath.row == 0)
        //{
    return @"recipes";
    /*}else if (indexPath.row == 1){
        return @"light";
    }else{
        return @"shades";
    }*/
}

-(Boolean) allowContentViewControllerCachingForIndexPath:(NSIndexPath *)indexPath{
    return YES;
}
    
-(Boolean) disablePanGestureForIndexPath:(NSIndexPath *)indexPath{
    //if (indexPath.row ==0) {
        //return YES;
    //}
    return NO;
}

// This is used to configure the menu button. The beahviour of the button should not be modified
-(void) configureMenuButton:(UIButton *)menuButton{
    menuButton.frame = CGRectMake(0, 0, 40, 29);
    [menuButton setImage:[UIImage imageNamed:@"menuicon.png"] forState:UIControlStateNormal];
    [menuButton setBackgroundImage:[UIImage imageNamed:@"menu.png"] forState:UIControlStateNormal];
    [menuButton setBackgroundImage:[UIImage imageNamed:@"menuhighlighted.png"] forState:UIControlStateHighlighted];
    [menuButton setAdjustsImageWhenHighlighted:NO];
    [menuButton setAdjustsImageWhenDisabled:NO];
}

-(void) configureSlideLayer:(CALayer *)layer{
    layer.shadowColor = [UIColor blackColor].CGColor;
    layer.shadowOpacity = 0.3;
    layer.shadowOffset = CGSizeMake(-15, 0);
    layer.shadowRadius = 10;
    layer.masksToBounds = NO;
    layer.shadowPath =[UIBezierPath bezierPathWithRect:layer.bounds].CGPath;
}

-(CGFloat) leftMenuVisibleWidth{
    return 305;
}
-(void) prepareForSwitchToContentViewController:(UINavigationController *)content{
    UIViewController* controller = [content.viewControllers objectAtIndex:0];
    if ([controller isKindOfClass:[RecipesViewController class]]) {
        RecipesViewController* lightViewController = (RecipesViewController*)controller;
        lightViewController.menuViewController = self;
    }
}

#pragma mark -
#pragma mark SASlideMenuDelegate


-(void) slideMenuWillSlideIn:(UINavigationController *)selectedContent{
    UIViewController* controller = [selectedContent.viewControllers objectAtIndex:0];
    if ([controller isKindOfClass:[RecipesViewController class]]) {
        recipesViewController = (RecipesViewController*)controller;
        NSLog(@"Recipe!");
    }
}

-(void) slideMenuDidSlideIn:(UINavigationController *)selectedContent{
    NSLog(@"slideMenuDidSlideIn");
}

-(void) slideMenuWillSlideToSide:(UINavigationController *)selectedContent{
    UIViewController* controller = [selectedContent.viewControllers objectAtIndex:0];
    if ([controller isKindOfClass:[RecipesViewController class]]) {
        recipesViewController = (RecipesViewController*)controller;
        NSLog(@"Recipe!");
    }
    NSLog(@"slideMenuWillSlideToSide");
}
-(void) slideMenuDidSlideToSide:(UINavigationController *)selectedContent{
    NSLog(@"slideMenuDidSlideToSide");
}
-(void) slideMenuWillSlideOut:(UINavigationController *)selectedContent{
    NSLog(@"slideMenuWillSlideOut");
}
-(void) slideMenuDidSlideOut:(UINavigationController *)selectedContent{
    NSLog(@"slideMenuDidSlideOut");
}
-(void) slideMenuWillSlideToLeft:(UINavigationController *)selectedContent{
    NSLog(@"slideMenuWillSlideToLeft");
}
-(void) slideMenuDidSlideToLeft:(UINavigationController *)selectedContent{
    NSLog(@"slideMenuDidSlideToLeft");
}


#pragma mark -
#pragma mark TableView dataSource/delegate

/*- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    UITableViewCell* cell = nil;
    static NSString *cellIdentifier = @"filterCellIdentifier";
    
    NSString *title = [[[_dataSourceArray objectAtIndex:indexPath.section] objectForKey:@"Rows"] objectAtIndex:indexPath.row];
    
    
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(!cell){
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
    }

    cell.textLabel.text = title;
    return cell;
}*/

/*- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSDictionary *sectionDictionary = [_dataSourceArray  objectAtIndex:section];
    if([[sectionDictionary objectForKey:@"isCollapsed"] boolValue]){
        return 0;
    }else{
        return [[[_dataSourceArray objectAtIndex:section] valueForKey:@"Rows"] count];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [_dataSourceArray count];
}*/

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == 0)
        return 0;
    return 50;
}

/*- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if(section != 0){
        NSDictionary *sectionDictionary = [_dataSourceArray  objectAtIndex:section];
        
        UIView *headerView = [[[NSBundle mainBundle] loadNibNamed:@"SectionHeaderView" owner:self options:nil] lastObject];
        UILabel *title = (UILabel*)[headerView viewWithTag:1];
        
        title.text = [[_dataSourceArray objectAtIndex:section] valueForKey:@"Title"];
        
        UIImageView *indecatorImage = (UIImageView*)[headerView viewWithTag:2];
        indecatorImage.image = [UIImage imageNamed:[[sectionDictionary objectForKey:@"isCollapsed"] boolValue]?@"arrow_white_right.png":@"arrow_down_table.png"];
        UIButton *button = (UIButton*)[headerView viewWithTag:3];
        button.tag = section;
        UIImageView *backgroundImageView = (UIImageView*)[headerView viewWithTag:4];
        backgroundImageView.image = [[sectionDictionary objectForKey:@"isCollapsed"] boolValue]?[UIImage imageNamed:@"brown_bg_no_shadow_cut.png"]:[UIImage imageNamed:@"brown_bg_cut.png"];
        
        return headerView;
    }
    return nil;
}*/

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    /*UITableViewCell *thisCell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (thisCell.accessoryType == UITableViewCellAccessoryNone) {
        thisCell.accessoryType = UITableViewCellAccessoryCheckmark;
        
    }else{
    	thisCell.accessoryType = UITableViewCellAccessoryNone;
        
    }*/
    
    /*if (tableView == _filterTableView) {
        [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    }*/
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
       
   
    //[self refreshData];
}

-(void)refreshData{
    NSMutableString *sqlString = [NSMutableString stringWithString:@"SELECT * FROM RecipesNew"];
    
    /*if(_currentCategoryType != CategoryTypeAll){
     if([sqlString containsString:@" WHERE"])
     [sqlString appendString:@" AND"];
     else
     [sqlString appendString:@" WHERE"];
     
     if (_currentCategoryType == CategoryTypeOatBran)
     [sqlString appendString:@" Category = 'Oat Bran'"];
     if (_currentCategoryType == CategoryTypeSauces)
     [sqlString appendString:@" Category = 'Sauces'"];
     }*/
    NSLog(@"%d",[[[[_dataSourceArray objectAtIndex:2] objectForKey:@"IsSelected"] objectAtIndex:0] intValue]);
    if([[[[_dataSourceArray objectAtIndex:2] objectForKey:@"IsSelected"] objectAtIndex:0] intValue] == 0){
        if([sqlString containsString:@" WHERE("])
            [sqlString appendString:@" AND("];
        else
            [sqlString appendString:@" WHERE("];
        NSLog(@"%@",[[[_dataSourceArray objectAtIndex:2] objectForKey:@"IsSelected"] objectAtIndex:1]);
        NSLog(@"%@",[[[_dataSourceArray objectAtIndex:2] objectForKey:@"IsSelected"] objectAtIndex:2]);
        if ([[[[_dataSourceArray objectAtIndex:2] objectForKey:@"IsSelected"] objectAtIndex:1] intValue]){
            if([sqlString containsString:@" MealType LIKE"])
                [sqlString appendString:@" OR"];
            [sqlString appendString:@" MealType LIKE '%breakfast%'"];
        }
            
        if ([[[[_dataSourceArray objectAtIndex:2] objectForKey:@"IsSelected"] objectAtIndex:2] intValue]){
            if([sqlString containsString:@" MealType LIKE"])
                [sqlString appendString:@" OR"];
            [sqlString appendString:@" MealType LIKE '%lunch%'"];
        }
            
        if ([[[[_dataSourceArray objectAtIndex:2] objectForKey:@"IsSelected"] objectAtIndex:3] intValue]){
            if([sqlString containsString:@" MealType LIKE"])
                [sqlString appendString:@" OR"];
            [sqlString appendString:@" MealType LIKE '%dinner%'"];
        }
            
        if ([[[[_dataSourceArray objectAtIndex:2] objectForKey:@"IsSelected"] objectAtIndex:4] intValue]){
            if([sqlString containsString:@" MealType LIKE"])
                [sqlString appendString:@" OR"];
            [sqlString appendString:@" MealType LIKE '%snack%'"];
        }
        [sqlString appendString:@" )"];
    }

    if([[[[_dataSourceArray objectAtIndex:3] objectForKey:@"IsSelected"] objectAtIndex:0] intValue] == 0){
        if([sqlString containsString:@" WHERE("])
            [sqlString appendString:@" AND("];
        else
            [sqlString appendString:@" WHERE("];

        if ([[[[_dataSourceArray objectAtIndex:3] objectForKey:@"IsSelected"] objectAtIndex:1] intValue]){
            if([sqlString containsString:@" CategoryIngredient LIKE"])
                [sqlString appendString:@" OR"];
            [sqlString appendString:@" CategoryIngredient LIKE '%Pork%'"];
        }
            
        if ([[[[_dataSourceArray objectAtIndex:3] objectForKey:@"IsSelected"] objectAtIndex:2] intValue]){
            if([sqlString containsString:@" CategoryIngredient LIKE"])
                [sqlString appendString:@" OR"];
            [sqlString appendString:@" CategoryIngredient LIKE '%Fruit%'"];
        }
        if ([[[[_dataSourceArray objectAtIndex:3] objectForKey:@"IsSelected"] objectAtIndex:3] intValue]){
            if([sqlString containsString:@" CategoryIngredient LIKE"])
                [sqlString appendString:@" OR"];
            [sqlString appendString:@" CategoryIngredient LIKE '%Beef%'"];
        }
        if ([[[[_dataSourceArray objectAtIndex:3] objectForKey:@"IsSelected"] objectAtIndex:4] intValue]){
            if([sqlString containsString:@" CategoryIngredient LIKE"])
                [sqlString appendString:@" OR"];
            [sqlString appendString:@" CategoryIngredient LIKE '%Veg%'"];
        }
            
        if ([[[[_dataSourceArray objectAtIndex:3] objectForKey:@"IsSelected"] objectAtIndex:5] intValue]){
            if([sqlString containsString:@" CategoryIngredient LIKE"])
                [sqlString appendString:@" OR"];
            [sqlString appendString:@" CategoryIngredient LIKE '%Eggs%'"];
        }
        if ([[[[_dataSourceArray objectAtIndex:3] objectForKey:@"IsSelected"] objectAtIndex:6] intValue]){
            if([sqlString containsString:@" CategoryIngredient LIKE"])
                [sqlString appendString:@" OR"];
            [sqlString appendString:@" CategoryIngredient LIKE '%Nuts%'"];
        }
        if ([[[[_dataSourceArray objectAtIndex:3] objectForKey:@"IsSelected"] objectAtIndex:7] intValue]){
            if([sqlString containsString:@" CategoryIngredient LIKE"])
                [sqlString appendString:@" OR"];
            [sqlString appendString:@" CategoryIngredient LIKE '%Chicken%'"];
        }
        if ([[[[_dataSourceArray objectAtIndex:3] objectForKey:@"IsSelected"] objectAtIndex:8] intValue]){
            if([sqlString containsString:@" CategoryIngredient LIKE"])
                [sqlString appendString:@" OR"];
            [sqlString appendString:@" CategoryIngredient LIKE '%Lamb%'"];
        }
        if ([[[[_dataSourceArray objectAtIndex:3] objectForKey:@"IsSelected"] objectAtIndex:9] intValue]){
            if([sqlString containsString:@" CategoryIngredient LIKE"])
                [sqlString appendString:@" OR"];
            [sqlString appendString:@" CategoryIngredient LIKE '%Fish%'"];
        }
        if ([[[[_dataSourceArray objectAtIndex:3] objectForKey:@"IsSelected"] objectAtIndex:10] intValue]){
            if([sqlString containsString:@" CategoryIngredient LIKE"])
                [sqlString appendString:@" OR"];
            [sqlString appendString:@" CategoryIngredient LIKE '%Dressing%'"];
        }
        if ([[[[_dataSourceArray objectAtIndex:3] objectForKey:@"IsSelected"] objectAtIndex:11] intValue]){
            if([sqlString containsString:@" CategoryIngredient LIKE"])
                [sqlString appendString:@" OR"];
            [sqlString appendString:@" CategoryIngredient LIKE '%Duck%'"];
        }
        [sqlString appendString:@" )"];
    }
    
    if([[[[_dataSourceArray objectAtIndex:4] objectForKey:@"IsSelected"] objectAtIndex:0] intValue] == 0){
        if([sqlString containsString:@" WHERE("])
            [sqlString appendString:@" AND("];
        else
            [sqlString appendString:@" WHERE("];
        
        if ([[[[_dataSourceArray objectAtIndex:4] objectForKey:@"IsSelected"] objectAtIndex:1] intValue]){
            if([sqlString containsString:@" CategoryBenefit LIKE"])
                [sqlString appendString:@" OR"];
            [sqlString appendString:@" CategoryBenefit LIKE '%Protein%'"];
        }
            
        if ([[[[_dataSourceArray objectAtIndex:4] objectForKey:@"IsSelected"] objectAtIndex:2] intValue]){
            if([sqlString containsString:@" CategoryBenefit LIKE"])
                [sqlString appendString:@" OR"];
            [sqlString appendString:@" CategoryBenefit LIKE '%Calcium%'"];
        }
        if ([[[[_dataSourceArray objectAtIndex:4] objectForKey:@"IsSelected"] objectAtIndex:3] intValue]){
            if([sqlString containsString:@" CategoryBenefit LIKE"])
                [sqlString appendString:@" OR"];
            [sqlString appendString:@" CategoryBenefit LIKE '%Rehydrate%'"];
        }
        if ([[[[_dataSourceArray objectAtIndex:4] objectForKey:@"IsSelected"] objectAtIndex:4] intValue]){
            if([sqlString containsString:@" CategoryBenefit LIKE"])
                [sqlString appendString:@" OR"];
            [sqlString appendString:@" CategoryBenefit LIKE '%Anty%'"];
        }
        if ([[[[_dataSourceArray objectAtIndex:4] objectForKey:@"IsSelected"] objectAtIndex:5] intValue]){
            if([sqlString containsString:@" CategoryBenefit LIKE"])
                [sqlString appendString:@" OR"];
            [sqlString appendString:@" CategoryBenefit LIKE '%Indulge%'"];
        }
        if ([[[[_dataSourceArray objectAtIndex:4] objectForKey:@"IsSelected"] objectAtIndex:6] intValue]){
            if([sqlString containsString:@" CategoryBenefit LIKE"])
                [sqlString appendString:@" OR"];
            [sqlString appendString:@" CategoryBenefit LIKE '%Vitamin%'"];
        }
        [sqlString appendString:@" )"];
    }
    
    if([[[[_dataSourceArray objectAtIndex:5] objectForKey:@"IsSelected"] objectAtIndex:0] intValue] == 0){
        if([sqlString containsString:@" WHERE("])
            [sqlString appendString:@" AND("];
        else
            [sqlString appendString:@" WHERE("];
        
        if ([[[[_dataSourceArray objectAtIndex:5] objectForKey:@"IsSelected"] objectAtIndex:1] intValue]){
            if([sqlString containsString:@" CategoryType LIKE"])
                [sqlString appendString:@" OR"];
            [sqlString appendString:@" CategoryType LIKE '%American%'"];
        }
        if ([[[[_dataSourceArray objectAtIndex:5] objectForKey:@"IsSelected"] objectAtIndex:2] intValue]){
            if([sqlString containsString:@" CategoryType LIKE"])
                [sqlString appendString:@" OR"];
            [sqlString appendString:@" CategoryType LIKE '%Asian%'"];
        }
        if ([[[[_dataSourceArray objectAtIndex:5] objectForKey:@"IsSelected"] objectAtIndex:3] intValue]){
            if([sqlString containsString:@" CategoryType LIKE"])
                [sqlString appendString:@" OR"];
            [sqlString appendString:@" CategoryType LIKE '%Bistro%'"];
        }
        if ([[[[_dataSourceArray objectAtIndex:5] objectForKey:@"IsSelected"] objectAtIndex:4] intValue]){
            if([sqlString containsString:@" CategoryType LIKE"])
                [sqlString appendString:@" OR"];
            [sqlString appendString:@" CategoryType LIKE '%Classic%'"];
        }
            
        if ([[[[_dataSourceArray objectAtIndex:5] objectForKey:@"IsSelected"] objectAtIndex:5] intValue]){
            if([sqlString containsString:@" CategoryType LIKE"])
                [sqlString appendString:@" OR"];
            [sqlString appendString:@" CategoryType LIKE '%Mexican%'"];
        }
            
        if ([[[[_dataSourceArray objectAtIndex:5] objectForKey:@"IsSelected"] objectAtIndex:6] intValue]){
            if([sqlString containsString:@" CategoryType LIKE"])
                [sqlString appendString:@" OR"];
            [sqlString appendString:@" CategoryType LIKE '%Middle%'"];
        }
        [sqlString appendString:@" )"];
    }
    
    if([[[[_dataSourceArray objectAtIndex:6] objectForKey:@"IsSelected"] objectAtIndex:0] intValue] == 0){
        if([sqlString containsString:@" WHERE("])
            [sqlString appendString:@" AND("];
        else
            [sqlString appendString:@" WHERE("];
        
        if ([[[[_dataSourceArray objectAtIndex:6] objectForKey:@"IsSelected"] objectAtIndex:1] intValue]){
            if([sqlString containsString:@" CategoryMethod LIKE"])
                [sqlString appendString:@" OR"];
            [sqlString appendString:@" CategoryMethod LIKE '%30 minute meal%'"];
        }
            
        if ([[[[_dataSourceArray objectAtIndex:6] objectForKey:@"IsSelected"] objectAtIndex:2] intValue]){
            if([sqlString containsString:@" CategoryMethod LIKE"])
                [sqlString appendString:@" OR"];
            [sqlString appendString:@" CategoryMethod LIKE '%Fast Pan Fry%'"];
        }
        
        if ([[[[_dataSourceArray objectAtIndex:6] objectForKey:@"IsSelected"] objectAtIndex:3] intValue]){
            if([sqlString containsString:@" CategoryMethod LIKE"])
                [sqlString appendString:@" OR"];
            [sqlString appendString:@" CategoryMethod LIKE '%Grill%'"];
        }
        if ([[[[_dataSourceArray objectAtIndex:6] objectForKey:@"IsSelected"] objectAtIndex:4] intValue]){
            if([sqlString containsString:@" CategoryMethod LIKE"])
                [sqlString appendString:@" OR"];
            [sqlString appendString:@" CategoryMethod LIKE '%No cook%'"];
        }
        if ([[[[_dataSourceArray objectAtIndex:6] objectForKey:@"IsSelected"] objectAtIndex:5] intValue]){
            if([sqlString containsString:@" CategoryMethod LIKE"])
                [sqlString appendString:@" OR"];
            [sqlString appendString:@" CategoryMethod LIKE '%Quick Bake%'"];
        }
        if ([[[[_dataSourceArray objectAtIndex:6] objectForKey:@"IsSelected"] objectAtIndex:6] intValue]){
            if([sqlString containsString:@" CategoryMethod LIKE"])
                [sqlString appendString:@" OR"];
            [sqlString appendString:@" CategoryMethod LIKE '%Slow Bake%'"];
        }
            
        if ([[[[_dataSourceArray objectAtIndex:6] objectForKey:@"IsSelected"] objectAtIndex:7] intValue]){
            if([sqlString containsString:@" CategoryMethod LIKE"])
                [sqlString appendString:@" OR"];
            [sqlString appendString:@" CategoryMethod LIKE '%Slow Simmer%'"];
        }
        if ([[[[_dataSourceArray objectAtIndex:6] objectForKey:@"IsSelected"] objectAtIndex:8] intValue]){
            if([sqlString containsString:@" CategoryMethod LIKE"])
                [sqlString appendString:@" OR"];
            [sqlString appendString:@" CategoryMethod LIKE '%Super Quick%'"];
        }
        [sqlString appendString:@" )"];
    }
    
    /*if(_recipeTypes != FilterRecipeTypeAll){
        if([sqlString containsString:@" WHERE"])
            [sqlString appendString:@" AND"];
        else
            [sqlString appendString:@" WHERE"];
        
        if (_recipeTypes == FilterRecipeTypeFavorite)
            [sqlString appendString:@" IsFavorite IS 'TRUE'"];
        else
            [sqlString appendString:@" IsFavorite IS 'FALSE'"];
    }
    
    if(currentDuration != 0){
        if([sqlString containsString:@" WHERE"])
            [sqlString appendString:@" AND"];
        else
            [sqlString appendString:@" WHERE"];
        
        [sqlString appendFormat:@" CAST(PreparationTimeInMins AS INTEGER)<=%d",currentDuration];
    }*/
    
    
    /*NSString *searchString = [[[_filterDataSource objectAtIndex:0] objectAtIndex:0] valueForKey:@"searchText"];
    if (searchString.length > 0) {
        if([sqlString containsString:@" WHERE"])
            [sqlString appendString:@" AND"];
		else
            [sqlString appendString:@" WHERE"];
        
        [sqlString appendString:[NSString stringWithFormat:@" (DishName LIKE '%%%@%%' OR", searchString]];
        [sqlString appendString:[NSString stringWithFormat:@" Ingredients LIKE '%%%@%%')", searchString]];
    }
    
    if(currentDuration != 0){
        [sqlString appendString:[@" ORDER BY CAST(PreparationTimeInMins AS INTEGER) ASC" stringByAppendingString: searchString]];
    }*/
    if([sqlString containsString:@" AND( )"]){
         NSRange range = [sqlString rangeOfString:@" AND( )"];
        [sqlString replaceCharactersInRange:range withString:@""];
    }
    if([sqlString containsString:@"AND( )"]){
        NSRange range = [sqlString rangeOfString:@"AND()"];
        [sqlString replaceCharactersInRange:range withString:@""];
    }
    if([sqlString containsString:@"WHERE()"]){
        NSRange range = [sqlString rangeOfString:@"WHERE()"];
        [sqlString replaceCharactersInRange:range withString:@""];
    }
       
       // [sqlString deleteCharactersInRange:]
    NSLog(@"DATA STRING%@",sqlString);
    
	_dataSource = [[NSMutableArray alloc]initWithArray:[[PBDB instance].db lookupAllForSQL:sqlString]];
	/*NSInteger value = 0;
     
     for (NSDictionary *dic in _dataSourceArray) {
     if(value < [[dic valueForKey:@"PreparationTimeInMins"] intValue])
     value = [[dic valueForKey:@"PreparationTimeInMins"] intValue];
     }
     
     NSLog(@"MAX Preparation time in minuts: %d", value);*/
    
    [recipesViewController refreshRecipesViewWithDataSource:_dataSource];
}

/*- (IBAction)_sectionHeaderClicked:(id)sender {
    NSInteger sectionIndex = ((UIButton*)sender).tag;
    
    //collapse all uncollapsed sections
    for(int i=0; i<_dataSourceArray.count; i++){
        if((i != sectionIndex) && ![_dataSourceArray[i][@"isCollapsed"] boolValue]){
            _dataSourceArray[i][@"isCollapsed"] = [NSNumber numberWithBool:YES];
        }
    }
    [__tableView reloadData];
    
    //change state of selected section
    NSMutableDictionary *sectionDictionary = [_dataSourceArray  objectAtIndex:sectionIndex];
    BOOL isCollapsed = [[sectionDictionary objectForKey:@"isCollapsed"] boolValue];
    [sectionDictionary setObject:[NSNumber numberWithBool:!isCollapsed] forKey:@"isCollapsed"];
    [__tableView reloadSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
    
    //show user hint popup
    
    //track user activity
    if(isCollapsed){
        NSString *eventName = nil;
        switch (sectionIndex) {
            case 0: eventName = kPB_FAEvent_CleanseCalendarShown; break;
            case 1: eventName = kPB_FAEvent_IntroductionToPaleoDiet_Shown; break;
            case 2: eventName = kPB_FAEvent_WhatBenefitsCanIExpect_Shown; break;
            case 3: eventName = kPB_FAEvent_WhatIsPaleo_Shown; break;
            case 4: eventName = kPB_FAEvent_WhichFoodsShouldEliminate_Shown; break;
            case 5: eventName = kPB_FAEvent_WhichFoodsShouldEat_Shown; break;
            case 6: eventName = kPB_FAEvent_FoodLists_Shown; break;
            case 7: eventName = kPB_FAEvent_AboutSixWeekPlan_Shown; break;
        }
        
        trackAction(eventName);
    }
}*/

/*- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(![_dataSourceArray[indexPath.section][@"isCollapsed"] boolValue] && (indexPath.row == 0)){
        int64_t delayInSeconds = 0.3;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [__tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
        });
        
    }
}*/
@end
