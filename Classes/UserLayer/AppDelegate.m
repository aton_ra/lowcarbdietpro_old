//
//  AppDelegate.m
//
//
//  Created by polar12 on 12/4/12.
//  Copyright (c) 2012 Polar-B. All rights reserved.
//

#import "AppDelegate.h"
#import <PB/PB.h>
#import "Flurry.h"
#import "MKStoreManager.h"
#import <Parse/Parse.h>
#import "PBParsecomManager.h"
#import "Appirater.h"
#import "Appirater.h"

@implementation AppDelegate

- (void)_updateDataBase {
    //fill table on parse.com from csv
    
    CSVParser *parser = [[CSVParser alloc] init];
    NSString *csvFilePath = [[NSBundle mainBundle] pathForResource:@"RecipeBook" ofType:@"csv"];
    [parser openFile:csvFilePath];
    [parser setBufferSize:8192];
    [parser setEncoding:NSUTF8StringEncoding];
    [parser setDelimiter:','];
    NSMutableArray *csvContent = [parser parseFile];
  
    for (NSArray *record in csvContent) {

        NSArray *keysArray = [NSArray arrayWithObjects:@"DishName", @"Image", @"DetailDescription", @"Ingredients", @"Preparation", @"PreparationTimeInMins", @"ServingsCount", @"MealType", @"NutritionalInfo", nil];
        
        NSMutableDictionary *insertDictionary = [NSMutableDictionary dictionaryWithCapacity:14];
        for (int i = 0; i < [record count] && i < [keysArray count]; ++i) {
            [insertDictionary setObject:[[record objectAtIndex:i] stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"] forKey:[keysArray objectAtIndex:i]];
    
            if ([[insertDictionary valueForKey:@"PreparationTimeInMins"] containsString:@"."]) {
                NSString *newString = [[insertDictionary valueForKey:@"PreparationTimeInMins"] stringByReplacingOccurrencesOfString:@"." withString:@""];
                [insertDictionary setValue:newString forKey:@"PreparationTimeInMins"];
            }
            
            if ([[insertDictionary valueForKey:@"PreparationTimeInMins"] containsString:@"mins"]) {
                NSString *newString = [[insertDictionary valueForKey:@"PreparationTimeInMins"] stringByReplacingOccurrencesOfString:@"mins" withString:@""];
                [insertDictionary setValue:newString forKey:@"PreparationTimeInMins"];
            }
            
            if ([[insertDictionary valueForKey:@"PreparationTimeInMins"] containsString:@"minutes"]) {
                NSString *newString = [[insertDictionary valueForKey:@"PreparationTimeInMins"] stringByReplacingOccurrencesOfString:@"minutes" withString:@""];
                [insertDictionary setValue:newString forKey:@"PreparationTimeInMins"];
            }
            
            if ([[insertDictionary valueForKey:@"DetailDescription"] containsString:@"\n"]) {
                NSString *newString = [[insertDictionary valueForKey:@"DetailDescription"] stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                [insertDictionary setValue:newString forKey:@"DetailDescription"];
            }
            
            while ([[insertDictionary valueForKey:@"PreparationTimeInMins"] containsString:@" "]) {
                NSString *newString = [[insertDictionary valueForKey:@"PreparationTimeInMins"] stringByReplacingOccurrencesOfString:@" " withString:@""];
                [insertDictionary setValue:newString forKey:@"PreparationTimeInMins"];
            }
            if (![[insertDictionary valueForKey:@"Image"] containsString:@".jpg"]) {
                NSString *newString = [NSString stringWithFormat:@"%@.jpg", [insertDictionary valueForKey:@"Image"]];
                [insertDictionary setValue:newString forKey:@"Image"];
            }
        }
        
        if([[insertDictionary objectForKey:@"Image"] isEqual: @""]){
            [insertDictionary setObject:@"-" forKey:@"Image"];
        }
        if([[insertDictionary objectForKey:@"DetailDescription"] isEqual: @""]){
            [insertDictionary setObject:@"-" forKey:@"DetailDescription"];
        }
        if([[insertDictionary objectForKey:@"Ingredients"] isEqual: @""]){
            [insertDictionary setObject:@"-" forKey:@"Ingredients"];
        }
        if([[insertDictionary objectForKey:@"ServingsCount"] isEqual: @""]){
            [insertDictionary setObject:@"-" forKey:@"ServingsCount"];
        }
        if([[insertDictionary objectForKey:@"MealType"] isEqual: @""]){
            [insertDictionary setObject:@"-" forKey:@"MealType"];
        }
        
        UIImage *img = [UIImage imageNamed:[insertDictionary objectForKey:@"Image"]];
        NSData *imageData;
        PFFile *imageFile;
        if(img){
            if([[insertDictionary objectForKey:@"Image"] containsString:@".png"]){
                imageData = UIImagePNGRepresentation(img);
                imageFile = [PFFile fileWithName:[insertDictionary objectForKey:@"Image"] data:imageData];
            }
            
            else if([[insertDictionary objectForKey:@"Image"] containsString:@".jpg"]){
                imageData = UIImageJPEGRepresentation(img, 0.05f);
                imageFile = [PFFile fileWithName:[insertDictionary objectForKey:@"Image"] data:imageData];
            }
        }
        //Caramelized Onion, Smoked Turkey, Mushroom and Gruyere Quiche Wi
        //Slivered Celery Salad with Blue Cheese Dressing
        
        //if([[insertDictionary objectForKey:@"Image"] isEqualToString:@"caramelized onion mushroom and gruyere quiche with oat crust.jpg"]){
            
            PFObject *recipeItem = [PFObject objectWithClassName:@"Recipes"];
            [recipeItem setObject:[insertDictionary objectForKey:@"DishName"] forKey:@"DishName"];
            [recipeItem setObject:imageFile forKey:@"Image"];
            [recipeItem setObject:[insertDictionary objectForKey:@"DetailDescription"] forKey:@"DetailDescription"];
            [recipeItem setObject:[insertDictionary objectForKey:@"Ingredients"] forKey:@"Ingredients"];
            [recipeItem setObject:[insertDictionary objectForKey:@"NutritionalInfo"] forKey:@"NutritionalInfo"];
            [recipeItem setObject:[insertDictionary objectForKey:@"Preparation"] forKey:@"Preparation"];
            [recipeItem setObject:[insertDictionary objectForKey:@"PreparationTimeInMins"] forKey:@"PreparationTimeInMins"];
            [recipeItem setObject:[insertDictionary objectForKey:@"ServingsCount"] forKey:@"ServingsCount"];
            [recipeItem setObject:[insertDictionary objectForKey:@"MealType"] forKey:@"MealType"];
            [recipeItem save];
        //}
    }

    [parser closeFile];
    RELEASE_SAFELY(parser);
    
}

- (void)_checkDatabase {
    
    NSArray *dataArray = [[PBDB instance].db lookupAllForSQL:@"SELECT * FROM RecipesNew"];
    for (NSDictionary *item in dataArray) {
        UIImage *image = [UIImage imageNamed:[item objectForKey:@"Image"]];
        if (!image) {
            //test
            NSLog(@"%@", item);
        }
    }
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
    //*** turn off time zones ***
    //[NSTimeZone setDefaultTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    //*** charge image presenter (presents a set of fullscrean pictures) ***
    [[PBImagePresenter instance] chargeWithWindow:_window];
    
	//*** charge twitter (autologin) ***
	//[[FacebookTwitterHelper instance] chargeTwitter];
	
	//*** charge facebook (autologin) ***
	//[[FacebookTwitterHelper instance] chargeFacebook];
	
	//*** charge PBPopup's feature ***
	[[PBPopupManager instance] chargeWithWindow:_window];
	
	//*** charge SQLite data base ***
	[[PBDB instance] chargeWithFileName:@"RecipeBook.sqlite"];
    
    //init parse.com
    
    [Parse setApplicationId:[[PBSettings instance] app:kPBSettings_Public_ParseAppID]
                  clientKey:[[PBSettings instance] app:kPBSettings_Public_ParseClientKey]];
    
    // init PBParsecomManager
    [PBParsecomManager sharedManager];
    
    // fill parse.com class from csv
    //[self _updateDataBase];
    
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"HasLauchedOnce"]){//if first time started set default settings
        //mark, that we already run this app
        //set default app settings
        [[PBSettings instance] user:[NSNumber numberWithInt:0] key:kPBSettings_Public_NumberOfWatchedRecipes];
        [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:NO] forKey:@"HasLauchedOnce"];
        [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:YES] forKey:kPBSettings_Public_NeetShowRatePopup];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    //Flurry Analytics
     NSLog(@"Flurry: %@",[[PBSettings instance] app:kPBSettings_Public_FlurryKey]);
    [Flurry startSession:[[PBSettings instance] app:kPBSettings_Public_FlurryKey]];
    [Flurry setSessionReportsOnCloseEnabled:YES];
    [Flurry setSessionReportsOnPauseEnabled:YES];
    
    // init store manager
    [MKStoreManager sharedManager];
    // fill meal plan shopping list
    //[self fillShoppingList];
    
    //init appirater
    [Appirater chargeWithAppId:[[PBSettings instance]app:kPBSettings_Public_AppSoftwareId]];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    // just for test restoring button - remove all purchases ! ! !
    //[[MKStoreManager sharedManager] removeAllKeychainData];
    
    return YES;
}
- (void)application:(UIApplication *)application willChangeStatusBarOrientation:(UIInterfaceOrientation)newStatusBarOrientation duration:(NSTimeInterval)duration{
    [[PBPopupManager instance] rotateToOrientation:newStatusBarOrientation fromOrientation:[UIApplication sharedApplication].statusBarOrientation];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}
- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
}
- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
}
- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}
- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
}
- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}

@end





