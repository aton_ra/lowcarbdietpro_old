//
//  RegisterViewController.m
//  DukanDiet
//
//  Created by polar12 on 8/12/13.
//  Copyright (c) 2013 Polar-B. All rights reserved.
//

#import "RegisterViewController.h"

@interface RegisterViewController ()

@property (nonatomic, strong) ChimpKit *chimpKit;
@property (nonatomic, strong) NSString *listId;

@end

@implementation RegisterViewController

-(BOOL)prefersStatusBarHidden{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.listId = [[PBSettings instance] app:kPBSettings_Public_ChimpKitListId];
    
    ChimpKit *cKit = [[ChimpKit alloc] initWithDelegate:self andApiKey:[[PBSettings instance] app:kPBSettings_Public_ChimpKitApiKey]];
    self.chimpKit = cKit;
    
    self.doubleOptIn = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHideHandler:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [self.nameTextField setValue:[UIColor colorWithRed:255.0 green:255.0 blue:255.0 alpha:0.7] forKeyPath:@"_placeholderLabel.textColor"];
    [self.surnameTextField setValue:[UIColor colorWithRed:255.0 green:255.0 blue:255.0 alpha:0.7] forKeyPath:@"_placeholderLabel.textColor"];
    [self.emailTextField setValue:[UIColor colorWithRed:255.0 green:255.0 blue:255.0 alpha:0.7] forKeyPath:@"_placeholderLabel.textColor"];
    
    CGRect frameRect = _nameTextField.frame;
    frameRect.size.height = 42;
    _nameTextField.frame = frameRect;
    [self.nameTextField setValue:[UIFont italicSystemFontOfSize:22] forKeyPath:@"_placeholderLabel.font"];
    
    frameRect = _surnameTextField.frame;
    frameRect.size.height = 42;
    _surnameTextField.frame = frameRect;
    [self.surnameTextField setValue:[UIFont italicSystemFontOfSize:22] forKeyPath:@"_placeholderLabel.font"];
    
    frameRect = _emailTextField.frame;
    frameRect.size.height = 42;
    _emailTextField.frame = frameRect;
    [self.emailTextField setValue:[UIFont italicSystemFontOfSize:22] forKeyPath:@"_placeholderLabel.font"];
	// Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self.view setBackgroundColor:[UIColor clearColor]];
    [self.view.superview setBackgroundColor:[UIColor clearColor]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    if (UI_USER_INTERFACE_IDIOM()== UIUserInterfaceIdiomPad){
        return ((UIInterfaceOrientationLandscapeLeft == toInterfaceOrientation) || (UIInterfaceOrientationLandscapeRight == toInterfaceOrientation) || (UIInterfaceOrientationPortrait == toInterfaceOrientation));
    }
    return (UIInterfaceOrientationPortrait == toInterfaceOrientation);
}

#pragma mark - Control events
- (IBAction)buttonCloseClick:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)registerNowButtonClick:(id)sender {
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:self.listId forKey:@"id"];
    [params setValue:self.emailTextField.text forKey:@"email_address"];
    [params setValue:(self.doubleOptIn ? @"true" : @"false") forKey:@"double_optin"];
    
    NSMutableDictionary *mergeVars = [NSMutableDictionary dictionary];
    [mergeVars setValue:self.nameTextField.text forKey:@"FNAME"];
    [mergeVars setValue:self.surnameTextField.text forKey:@"LNAME"];
    [params setValue:mergeVars forKey:@"merge_vars"];
    
    [self.chimpKit callApiMethod:@"listSubscribe" withParams:params];
}

#pragma mark - Text field delegate

- (BOOL)textFieldShouldReturn:(UITextField *)aTextField
{
    if(isIphone){
        CGRect frame = self.view.frame;
        frame.origin.y = 0;
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration: 0.25];
        self.view.frame = frame;
        [UIView commitAnimations];
        [aTextField resignFirstResponder];
    }

    return NO;
}

- (void) keyboardWillHideHandler:(NSNotification *)notification {
    if(isIphone){
        CGRect frame = self.view.frame;
        frame.origin.y = 0;
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration: 0.25];
        self.view.frame = frame;
        [UIView commitAnimations];
    }else{
        if([[UIDevice currentDevice] orientation] == UIInterfaceOrientationLandscapeLeft || [[UIDevice currentDevice] orientation] == UIInterfaceOrientationLandscapeRight){
            CGRect frame = self.view.superview.frame;
            frame.origin.x = 200;
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration: 0.25];
            self.view.superview.frame = frame;
            [UIView commitAnimations];
        }
    }
}

- (IBAction)fieldDidBeginEditing:(id)sender {
    
    if(isIphone){
        CGRect frame = self.view.frame;
        frame.origin.y = - 130;
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration: 0.25];
        self.view.frame = frame;
        [UIView commitAnimations];
    }/*else{
        if([[UIDevice currentDevice] orientation] == UIInterfaceOrientationLandscapeLeft || [[UIDevice currentDevice] orientation] == UIInterfaceOrientationLandscapeRight){
            CGRect frame = self.view.superview.frame;
            frame.origin.x = - 20;
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration: 0.25];
            self.view.superview.frame = frame;
            [UIView commitAnimations];
        }
    }*/
}

- (void)showSubscribeError {
    UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:@"Subscription Failed"
                                                             message:@"We couldn't subscribe you to the list.  Please check your email address and try again."
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
    [errorAlertView show];
}

#pragma mark -
#pragma mark - Chimpkit delegate

- (void)ckRequestSucceeded:(ChimpKit *)ckRequest {
    if (![ckRequest.responseString isEqualToString:@"true"]) {
        [self showSubscribeError];
    }
    else{
        
        UIAlertView *successAlertView = [[UIAlertView alloc] initWithTitle:@"Subscription succeded"
                                                                   message:@"Your subscribing to the list was success.  Please check your email address to accept"
                                                                  delegate:nil
                                                         cancelButtonTitle:@"OK"
                                                         otherButtonTitles:nil];
        [successAlertView show];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)ckRequestFailed:(NSError *)error {
    [self showSubscribeError];
}


@end
