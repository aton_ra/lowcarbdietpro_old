//
//  ShoppingViewController.m
//  DukanDiet
//
//  Created by polar12 on 8/12/13.
//  Copyright (c) 2013 Polar-B. All rights reserved.
//

#import "ShoppingViewController.h"
#import "PopoverView.h"
#import "StoreViewController.h"

#define kPBSettings_Private_ShoppingListIsNotFirstTimeOpened  @"shopping list is not first time opened"

@interface ShoppingViewController (){
    StoreViewController *storeVC;
}

-(void) refreshDataSource;

@end

@implementation ShoppingViewController

-(BOOL)prefersStatusBarHidden{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(isIphone){
        [self.navigationItem setTitle:@"Shopping"];
        UIBarButtonItem *clearBtn = [[UIBarButtonItem alloc] initWithTitle:@"Clear" style:UIBarButtonItemStyleBordered target:self action:@selector(buttonClearClick)];
        clearBtn.tintColor  = [UIColor blackColor];
        self.parentViewController.navigationController.navigationItem.leftBarButtonItem = clearBtn;
    }
    
    [_emptyListLable setText:/*LocalizedString(@"Shopping List is empty", */@"Shopping List is empty"];
    [clearButton setBackgroundImage:[UIImage imageNamed:@"black_button.png"] forState:UIControlStateNormal];
    [clearButton setTitleColor:[UIColor whiteColor] forState: UIControlStateNormal];
    clearButton.titleLabel.font = [UIFont boldSystemFontOfSize:12];
    [self refreshDataSource];
    if(_dataSourceArray.count){
        [PopoverView showPopoverAtPoint:CGPointMake(0, (IsiOS7)?99:25) inView:self.view withText: @"Swipe left to delete" delegate:nil];
    }
    
    if(IsiOS7){
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"bg_nav_bar_cut.png"] forBarMetrics:UIBarMetricsDefault];
    }
    else{
        [self.navigationController.navigationBar setBackgroundColor:[UIColor blackColor]];
    }
    
	// Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
   
    [super viewWillAppear:animated];
    //apdate table datasource
    [self refreshDataSource];
    [_tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) refreshDataSource{
    
    _dataSourceArray = [[NSMutableArray alloc] initWithArray:[[PBDB instance].db lookupAllForSQL:@"SELECT * FROM Shopping"]];
    if(_dataSourceArray.count){
        _emptyListLable.alpha = 0;
        _tableView.alpha = 1;
    }else{
        _emptyListLable.alpha = 1;
        _tableView.alpha = 0;
    }
}

#pragma mark -
#pragma mark TableView delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataSourceArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[[NSBundle mainBundle] loadNibNamed:IsIpad?@"ShoppingItemCell-iPad":@"ShoppingItemCell" owner:self options:nil] lastObject];
    cell.frame = CGRectMake(0, 0, _tableView.width, cell.frame.size.height);
    UILabel *lable = (UILabel*)[cell.contentView viewWithTag:1];
    lable.text = [[_dataSourceArray objectAtIndex:indexPath.row] objectForKey:@"Name"];
    [lable setWidth:320];
    [lable sizeToFit];
    
    return lable.size.height + 22.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dataDictionary = [_dataSourceArray objectAtIndex:indexPath.row];
    static NSString *cellIdentifier = @"ShoppingItemCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(!cell){
        cell = [[[NSBundle mainBundle] loadNibNamed:IsIpad?@"ShoppingItemCell-iPad":@"ShoppingItemCell" owner:self options:nil] lastObject];
        UIView *selectedBackgroundView = [[UIView alloc] initWithFrame:cell.frame];
        selectedBackgroundView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.45];
        cell.selectedBackgroundView = selectedBackgroundView;
    }
    
    cell.size = CGSizeMake(_tableView.width, 44);
    cell.contentView.size = cell.size;
    UILabel *lable = (UILabel*)[cell.contentView viewWithTag:1];
    lable.size = CGSizeMake(cell.contentView.width-20, 22);
    lable.text = [dataDictionary objectForKey:@"Name"];
    [lable sizeToFit];
    lable.frame = CGRectMake(10, 11, cell.size.width - 20, lable.size.height);
    
    cell.size = CGSizeMake(cell.width,lable.height+22);
    cell.contentView.size = cell.size;
    
    [cell.contentView viewWithTag:2].frame = CGRectMake(lable.origin.x, lable.origin.y+lable.size.height+6, lable.size.width, 1);
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [[PBDB instance].db deleteWhere:[NSString stringWithFormat:@"Id = %@",[[_dataSourceArray objectAtIndex:indexPath.row] objectForKey:@"Id"]] forTable:@"Shopping"];
        [_dataSourceArray removeObjectAtIndex:indexPath.row];
        if(_dataSourceArray.count == 0){
            _emptyListLable.alpha = 1;
            _tableView.alpha = 0;
        }
        [_tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationRight];
    }
}

#pragma  mark -
#pragma mark Buttont events

-(IBAction)storeButtonClick{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:IsIpad?@"Storyboard_iPad":@"Storyboard_iPhone" bundle:nil];
    storeVC = (StoreViewController*) [mainStoryboard instantiateViewControllerWithIdentifier:@"StoreViewController"];
    //UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:termsVC];
    [self presentViewController:storeVC animated:YES completion:nil];
}

- (void)buttonClearClick{
    [[PBDB instance].db deleteWhere:@"1" forTable:@"Shopping"];
    [self refreshDataSource];
    [_tableView reloadData];
}

- (IBAction)buttonClearClick:(id)sender{
    [[PBDB instance].db deleteWhere:@"1" forTable:@"Shopping"];
    [self refreshDataSource];
    [_tableView reloadData];
}

@end
