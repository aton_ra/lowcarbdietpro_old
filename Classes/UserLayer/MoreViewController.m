//
//  MoreViewController.m
//  DukanDiet
//
//  Created by polar12 on 8/12/13.
//  Copyright (c) 2013 Polar-B. All rights reserved.
//

#import "MoreViewController.h"
#import "StoreViewController.h"

@interface MoreViewController ()

@end

@implementation MoreViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    if(isIphone){
        [self.navigationItem setTitle:@"More"];
    }
    
    if(IsiOS7){
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"bg_nav_bar_cut.png"] forBarMetrics:UIBarMetricsDefault];
    }
    else{
        [self.navigationController.navigationBar setBackgroundColor:[UIColor blackColor]];
    }
    
    [_activiityIndicator startAnimating];
    
    NSString *urlString = [NSString stringWithFormat:@"http://admin.fitkitapps.com/services/getPromotion/%@",[[PBSettings instance] app:kPBSettings_Public_Key]];
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button action

-(IBAction)storeButtonClick{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:IsIpad?@"Storyboard_iPad":@"Storyboard_iPhone" bundle:nil];
    StoreViewController *storeVC = (StoreViewController*) [mainStoryboard instantiateViewControllerWithIdentifier:@"StoreViewController"];
    [self presentViewController:storeVC animated:YES completion:nil];
}

#pragma mark -
#pragma mark WebView delegate
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [_activiityIndicator stopAnimating];
    
    [UIView beginAnimations:nil context:nil];
    _activiityIndicator.alpha = 0;
    _webView.alpha = 1;
    [UIView commitAnimations];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request   navigationType:(UIWebViewNavigationType)navigationType
{
    if ([request.URL.absoluteString startsWithString:@"http://"]) {
        if(![[[request URL] absoluteString] hasPrefix:@"http://admin.fitkitapps.com/services/getPromotion/"]){
            
            NSString *urlString = [[[[request URL] absoluteString] componentsSeparatedByString:@"&NameForFlurryAnalytics="] objectAtIndex:0];
            [self openReferralURL:[NSURL URLWithString:urlString]];
            
            NSString * javaScriptString = [NSString stringWithFormat:@"function foo(){for( i=0; i < document.links.length; i++ ){if(document.links[i].href == \"%@\"){return i+1}}} foo();",[[request URL] absoluteString]];
            NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[_webView stringByEvaluatingJavaScriptFromString:javaScriptString],@"Banner positions", nil];
            trackActionWithParameters(kPB_FAEvent_MoreAppsBannerClicked, params);
            
            return NO;
        }else{
            return YES;
        }
    }
    
    return YES;
}

// Process a LinkShare/TradeDoubler/DGM URL to something iPhone can handle
- (void)openReferralURL:(NSURL *)referralURL
{
    if([[referralURL absoluteString] startsWithString:@"http://itunes"]){//usual direction to itunes
        [[UIApplication sharedApplication] openURL:referralURL];
    }else{//silently redirects to itunes
        NSURLConnection *con = [[NSURLConnection alloc] initWithRequest:[NSURLRequest requestWithURL:referralURL] delegate:self startImmediately:YES];
        [con release];
    }
}

// Save the most recent URL in case multiple redirects occur
// "iTunesURL" is an NSURL property in your class declaration
- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)response
{
    _iTunesURL = [response URL];
    
    if( [_iTunesURL.host hasSuffix:@"itunes.apple.com"])
    {
        [connection cancel];
        [self connectionDidFinishLoading:connection];
        return nil;
    }
    else
    {
        return request;
    }
}

// No more redirects; use the last URL saved
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [[UIApplication sharedApplication] openURL:_iTunesURL];
}

@end
