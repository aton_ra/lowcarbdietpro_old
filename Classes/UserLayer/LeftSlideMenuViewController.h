//
//  PosesSlideMenuViewController.h
//  Yoga
//
//  Created by polar15 on 27.03.13.
//  Copyright (c) 2013 Polar-B. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LeftSlideMenuViewController;

@protocol LeftSlideMenuDelegate
@optional
-(void) valueDidChanged:(LeftSlideMenuViewController*)posesSlideMenu;
@end

@interface LeftSlideMenuViewController : UIViewController
@property(weak) NSObject<LeftSlideMenuDelegate> *delegate;

@property NSArray *menuDatasourceArray;
@property BOOL isFavorite;
@property int duration;
//@property PoseType type;
//@property DifficultyType difficulty;
//@property PoseFocusType focus;
@end



