//
//  TermsAndConditionsViewController.m
//  DukanDiet
//
//  Created by polar12 on 8/12/13.
//  Copyright (c) 2013 Polar-B. All rights reserved.
//

#import "TermsAndConditionsViewController.h"

@interface TermsAndConditionsViewController ()

@end

@implementation TermsAndConditionsViewController

#pragma mark - constructor

#pragma mark - Lifecycle

-(BOOL)prefersStatusBarHidden{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UILabel* titleLabel = [[UILabel alloc] init];
    titleLabel.font =  [UIFont fontWithName:@"Helvetica-Bold" size:20];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.text = @"Terms and conditions";
    [titleLabel sizeToFit];
    self.navigationItem.titleView = titleLabel;
    
    if(IsiOS7){
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"bg_nav_bar_cut.png"] forBarMetrics:UIBarMetricsDefault];
    }
    else{
        [self.navigationController.navigationBar setBackgroundColor:[UIColor blackColor]];
    }
    
    UIButton *closeBtn = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, IsIpad?70:55, IsIpad?37:30)];
    [closeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [closeBtn setTitle:@"Close" forState:UIControlStateNormal];
    closeBtn.titleLabel.font = [UIFont systemFontOfSize:isIphone?14:17];
    [closeBtn setBackgroundImage:[UIImage imageNamed:@"black_button.png"] forState:UIControlStateNormal];
    [closeBtn addTarget:self action:@selector(closeButtonClick) forControlEvents:(UIControlEventTouchDown)];
    
    UIBarButtonItem *closeButton = [[UIBarButtonItem alloc] initWithCustomView:closeBtn];
    
    [self.navigationItem setLeftBarButtonItem:closeButton];
    
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    
    NSString *infoText;
    if(IsIpad){
        infoText = [NSString stringWithFormat:@"<html><head><style>body{font-family:Helvetica;font-size:22;background-color:transparent;text-align:left;}</style></head><body>%@</body></html>",[[PBSettings instance] app:kPBSettings_Public_TextForVebView]];
    }else{
        infoText = [NSString stringWithFormat:@"<html><head><style>body{font-family:Helvetica;font-size:17;background-color:transparent;text-align:left;}</style></head><body>%@</body></html>",[[PBSettings instance] app:kPBSettings_Public_TextForVebView]];
    }
    
    [_aboutWebView loadHTMLString:infoText baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];
    [_aboutWebView setBackgroundColor:[UIColor clearColor]];
    [_aboutWebView setOpaque:NO];
    [_aboutWebView setOrigin:CGPointMake(0.0, (IsiOS7)?44:0.0)];
    
    _loadingView.alpha = 0;
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self willRotateToInterfaceOrientation:self.interfaceOrientation duration:0];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return YES;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    if(IsIpad){
        if(_backgroundImageView){
            if(UIInterfaceOrientationIsPortrait(toInterfaceOrientation)){
                _backgroundImageView.image = [UIImage imageNamed:@"Background_Portrait_iPad.png"];
            }else{
                _backgroundImageView.image = [UIImage imageNamed:@"Background_Landscape_iPad.png"];
            }
            
            if(UIInterfaceOrientationIsPortrait(toInterfaceOrientation)){
                _aboutWebView.height =  960 ;
                _aboutWebView.width = 768;
                _aboutWebView.top =  (IsiOS7)?44:0;
                _aboutWebView.left =  0;
            }else {
                _aboutWebView.height = 702;
                _aboutWebView.width = 1023;
                _aboutWebView.top = (IsiOS7)?44:0;
                _aboutWebView.left =  0;
            }
        }
        
        [UIView commitAnimations];
    }
}

#pragma mark - web view delegate
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    if(isIphone)
        [webView setWidth:320];
    
    [UIView beginAnimations:nil context:nil];
    _loadingView.alpha = 0;
    [UIView commitAnimations];
}

#pragma mark - button events
- (void) closeButtonClick{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
