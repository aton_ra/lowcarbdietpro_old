//
//  RecipesViewController.m
//  DukanDiet
//
//  Created by polar12 on 8/8/13.
//  Copyright (c) 2013 Polar-B. All rights reserved.
//

#import "RecipesViewController.h"
#import "RecipeDetailsViewController.h"
#import "RegisterViewController.h"
#import "StoreViewController.h"
#import "FilterPanelView.h"
#import "PopoverView.h"
#import "PBParsecomManager.h"
#import "JASidePanelController.h"
#import "Appirater.h"

CGSize CollectionViewCellSize = { .height = 170, .width = 200 };
CGSize imageSize = { .height = 131, .width = 188 };
NSString *CollectionViewCellIdentifier = @"SelectionDelegateExample";
BOOL isGridPresentation;

@interface RecipesViewController (){
    RegisterViewController *regVC;
    StoreViewController *storeVC;
    LeftSlideMenuViewController *filterSlideViewController;
}

@end

@implementation RecipesViewController

@synthesize delegate;
@synthesize _dataSourceArray;
@synthesize _gridView;
@synthesize _tableView;
@synthesize  _recipeSearchBar;
@synthesize gridContentView;
@synthesize listContentView;

@synthesize _tableViewCell;

#pragma mark - Initialization

- (void)createGridView {
    PSUICollectionViewFlowLayout *layout = [[PSUICollectionViewFlowLayout alloc] init];
    _gridView = [[PSUICollectionView alloc] initWithFrame:[self.view bounds] collectionViewLayout:layout];
    _gridView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _gridView.delegate = self;
    _gridView.dataSource = self;
    _gridView.backgroundColor = [UIColor clearColor];
    [_gridView registerClass:[ImageGridCell class] forCellWithReuseIdentifier:CollectionViewCellIdentifier];
    
    [self.gridContentView addSubview:_gridView];
    //[self.listContentView setHidden:YES];
    
    [self.gridContentView setHidden:YES];
    isGridPresentation = YES;
}

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
	// Do any additional setup after loading the view.
    [self createGridView];
    
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshFilterPanel) name:@"recipesWasPurchased" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dataBaseWasUpdated) name:@"dataBaseWasUpdated" object:nil];
    if(isIphone){
        
        [self.navigationItem setTitle:@"Recipes"];
        if(IsiOS7){
            [_recipeSearchBar setTintColor:[UIColor whiteColor]];
            [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"bg_nav_bar_cut.png"] forBarMetrics:UIBarMetricsDefault];
        }
        else{
            [_recipeSearchBar setBackgroundColor:[UIColor blackColor]];
            [self.navigationController.navigationBar setBackgroundColor:[UIColor blackColor]];
        }
    }
}


-(void) viewDidAppear:(BOOL)animated{
    
    if(isIphone){
        [self refreshDataSource];
    }
    
    int value = [[[PBSettings instance] user:kPBSettings_Public_NumberOfWatchedRecipes] intValue];
    if(value%10 == 0 && value != 0){
        [[PBSettings instance] user:[NSNumber numberWithInt:0] key:kPBSettings_Public_NumberOfWatchedRecipes];
        [Appirater showRatingPopup];
        NSLog(@"SHOW %@", [[PBSettings instance] user:kPBSettings_Public_NumberOfWatchedRecipes]);
    }
    NSLog(@"INT VALUE: %@", [[PBSettings instance] user:kPBSettings_Public_NumberOfWatchedRecipes]);
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"%c",[[NSUserDefaults standardUserDefaults] boolForKey:@"HasLauchedOnce"]);
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"HasLauchedOnce"]){
        
        if(!regVC){
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:IsIpad? @"Storyboard_iPad":@"Storyboard_iPhone" bundle:nil];
            regVC = (RegisterViewController*)[mainStoryboard instantiateViewControllerWithIdentifier:@"RegisterViewController"];
        }
        
        regVC.modalPresentationStyle = UIModalPresentationFormSheet;
        regVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self presentViewController:regVC animated:YES completion:nil];
        
        if(IsIpad){
            float x, y;
            
            CGRect screenBounds = [UIScreen mainScreen].bounds ;
            CGFloat width = CGRectGetWidth(screenBounds);
            CGFloat height = CGRectGetHeight(screenBounds);
            
            if(UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])){
                
                [regVC.view.superview setHeight:407];
                [regVC.view.superview setWidth:490];
                y  = (width - regVC.view.superview.size.width) / 2;
                x  = (height - regVC.view.superview.size.height) / 2;
                
                [regVC.view.superview setOrigin:CGPointMake(y, x)];
            }
            else{
                [regVC.view.superview setWidth:490];
                [regVC.view.superview setHeight:407];
                // in landscape height is width = 1024 and width is height = 768
                y  = (height - regVC.view.superview.size.width) / 2;
                x  = (width - regVC.view.superview.size.height) / 2;
                
                [regVC.view.superview setOrigin:CGPointMake(y, x)];
            }
            
            [regVC.view.superview setBackgroundColor:[UIColor clearColor]];
            [regVC.view.superview.layer setCornerRadius:14.5];
            [regVC.view.superview.layer setMasksToBounds:YES];
        }
        
        [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:YES] forKey:@"HasLauchedOnce"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSLog(@"%c",[[NSUserDefaults standardUserDefaults] boolForKey:@"HasLauchedOnce"]);
    }
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{

    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [_tableView reloadData];
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    if (UI_USER_INTERFACE_IDIOM()== UIUserInterfaceIdiomPad){
        return ((UIInterfaceOrientationLandscapeLeft == toInterfaceOrientation) || (UIInterfaceOrientationLandscapeRight == toInterfaceOrientation) || (UIInterfaceOrientationPortrait == toInterfaceOrientation));
    }
    return (UIInterfaceOrientationPortrait == toInterfaceOrientation);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*- (void)toggleAllowsMultipleSelection:(UIBarButtonItem *)item {
    _gridView.allowsMultipleSelection = !_gridView.allowsMultipleSelection;
    item.title = _gridView.allowsMultipleSelection ? @"Single-Select" : @"Multi-Select";
}*/

#pragma mark -
#pragma mark Collection View dataSource/delegates

- (NSString *)formatIndexPath:(NSIndexPath *)indexPath {
    return [NSString stringWithFormat:@"{%ld,%ld}", (long)indexPath.row, (long)indexPath.section];
}

- (PSUICollectionViewCell *)collectionView:(PSUICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    //imgView.layer.cornerRadius = 5.0;
    //imgView.layer.masksToBounds = YES;
    
    ImageGridCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CollectionViewCellIdentifier forIndexPath:indexPath];
    
    cell.label.text =[[_dataSourceArray objectAtIndex:indexPath.item] valueForKey:@"DishName"];
    
    UIImage *img = [UIImage imageNamed:[[_dataSourceArray objectAtIndex:indexPath.item] valueForKey:@"Image"]];
    if(!img){
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
        
        img = [UIImage imageWithContentsOfFile:[documentsPath stringByAppendingPathComponent:[[_dataSourceArray objectAtIndex:indexPath.item] valueForKey:@"Image"]]];
    }
    
    cell.image.image = img;
    
    //set white border for image
    cell.backImage.image = [UIImage imageNamed:@"frame_foto.png"];
    
    NSString *starImageFileNameString = ([[[_dataSourceArray objectAtIndex:indexPath.row] valueForKey:@"IsFavorite"] boolValue]) ? @"star_active_frame_foto.png" : @"star_frame_foto.png";
    [cell.starButton setImage:[UIImage imageNamed:starImageFileNameString] forState:UIControlStateNormal];
    [cell.starButton addTarget:self action:@selector(starButtonClicked:) forControlEvents:UIControlEventTouchDown];
    
    return cell;
}

- (CGSize)collectionView:(PSUICollectionView *)collectionView layout:(PSUICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CollectionViewCellSize;
}

- (NSInteger)collectionView:(PSUICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return [_dataSourceArray count];
}

- (void)collectionView:(PSTCollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{

    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Storyboard_iPad" bundle:nil];
    
    RecipeDetailsViewController *detailsVC = (RecipeDetailsViewController*) [mainStoryboard instantiateViewControllerWithIdentifier:@"RecipeDetailsViewController"];
    [detailsVC setRecipeData:[_dataSourceArray objectAtIndex:indexPath.row]];
    
    UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:detailsVC];
    
    [self presentViewController:navigation animated:YES completion:nil];
    
    NSDictionary *paramDictionary = [NSDictionary dictionaryWithObjectsAndKeys:[[_dataSourceArray objectAtIndex:indexPath.row] valueForKey:@"DishName"],@"Recipe Name", nil];
    trackActionWithParameters(kPB_FAEvent_RecipeOpened, paramDictionary);
}

#pragma mark  TableView dataSource/delegates

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 77;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"RecipeTableViewCellIdentifier";
    UITableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"RecipeTableViewCell" owner:self options:nil];
        if ([nib count] > 0) {
            cell = _tableViewCell;
        }
    }
    
    UILabel *recipeLabel = (UILabel *)[cell.contentView viewWithTag:1];
    recipeLabel.text = [NSString stringWithFormat:@"%@", [[_dataSourceArray objectAtIndex:indexPath.row] valueForKey:@"DishName"]];
    
    UIButton *starButton = (UIButton *)[cell.contentView viewWithTag:2];
    NSString *starImageFileNameString = ([[[_dataSourceArray objectAtIndex:indexPath.row] valueForKey:@"IsFavorite"] boolValue]) ? @"star_active_frame_foto.png" : @"star_frame_foto.png";
    [starButton setImage:[UIImage imageNamed:starImageFileNameString] forState:UIControlStateNormal];
    [starButton addTarget:self action:@selector(starButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *descriptionLabel = (UILabel *)[cell.contentView viewWithTag:3];
    descriptionLabel.text = [NSString stringWithFormat:@"%@", [[_dataSourceArray objectAtIndex:indexPath.row] valueForKey:@"DetailDescription"]];
    
    UIImageView *shadowUpImageView = (UIImageView *)[cell.contentView viewWithTag:5];
    UIImageView *shadowLeftUpImageView = (UIImageView *)[cell.contentView viewWithTag:7];
    if(indexPath.row == 0){
        shadowUpImageView.hidden = NO;
        shadowLeftUpImageView.hidden = NO;
    }else{
        shadowUpImageView.hidden = YES;
        shadowLeftUpImageView.hidden = YES;
    }
    
    UIImageView *shadowDownImageView = (UIImageView *)[cell.contentView viewWithTag:4];
    UIImageView *shadowLeftBottomImageView = (UIImageView *)[cell.contentView viewWithTag:6];
    
    if(indexPath.row == [_dataSourceArray count]-1){
        shadowDownImageView.hidden = NO;
        shadowLeftBottomImageView.hidden = NO;
    }else{
        shadowDownImageView.hidden = YES;
        shadowLeftBottomImageView.hidden = YES;
    }
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        if(UIInterfaceOrientationIsPortrait(self.interfaceOrientation)){
            recipeLabel.width = 483;
            //descriptionLabel.width = self.view.width-60;
        }else{
            recipeLabel.width = self.view.width;
            
        }
        descriptionLabel.width = self.view.width-100;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:IsIpad?@"Storyboard_iPad":@"Storyboard_iPhone" bundle:nil];
    
    RecipeDetailsViewController *detailsVC = (RecipeDetailsViewController*) [mainStoryboard instantiateViewControllerWithIdentifier:@"RecipeDetailsViewController"];
    [detailsVC setRecipeData:[_dataSourceArray objectAtIndex:indexPath.row]];
    
    UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:detailsVC];
    
    [self presentViewController:navigation animated:YES completion:nil];
    
    NSDictionary *paramDictionary = [NSDictionary dictionaryWithObjectsAndKeys:[[_dataSourceArray objectAtIndex:indexPath.row] valueForKey:@"DishName"],@"Recipe Name", nil];
    trackActionWithParameters(kPB_FAEvent_RecipeOpened, paramDictionary);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_dataSourceArray count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return nil;
}

#pragma mark - Button action

- (IBAction)onMenuButtonClick:(id)sender {
    /*if(self.sidePanelController.state == JASidePanelCenterVisible){
        [self.sidePanelController showRightPanelAnimated:YES];
    }else{
        [self.sidePanelController showCenterPanelAnimated:YES];
    }*/
}

- (void) starButtonClicked:(UIButton *)sender {
    int neededIndex;
    if(IsIpad){
        if(isGridPresentation){
            neededIndex = [_gridView indexPathForCell:(ImageGridCell *)sender.superview.superview].row;
        }else{
            if(IsiOS7){
                neededIndex = [_tableView indexPathForCell:((UITableViewCell *)sender.superview.superview.superview)].row;
            }
            else{
                neededIndex = [_tableView indexPathForCell:((UITableViewCell *)sender.superview.superview)].row;
            }
        }
    }else{
        
        NSLog(@"Kind of class: %@", [sender.superview.superview class]);
        if(IsiOS7){
            neededIndex = [_tableView indexPathForCell:((UITableViewCell *)sender.superview.superview.superview)].row;
        }
        else{
            neededIndex = [_tableView indexPathForCell:((UITableViewCell *)sender.superview.superview)].row;
        }
    }
    
    [[[PBDB instance] db] updateSQL:[NSString stringWithFormat:@"UPDATE Recipes SET IsFavorite = '%@' WHERE Recipes.DishName LIKE '%%%@%%'",
                                     (([[[_dataSourceArray objectAtIndex:neededIndex] valueForKey:@"IsFavorite"] boolValue]) ? @"FALSE" : @"TRUE"),
                                     [[_dataSourceArray objectAtIndex:neededIndex] valueForKey:@"DishName"]]
                           forTable:@"Recipes"];
    
    NSDictionary *paramDictionary = [NSDictionary dictionaryWithObjectsAndKeys:[[_dataSourceArray objectAtIndex:neededIndex] valueForKey:@"DishName"],@"Recipe Name", nil];
    if([[[_dataSourceArray objectAtIndex:neededIndex] valueForKey:@"IsFavorite"] boolValue]){
        trackActionWithParameters(kPB_FAEvent_RemoveFromFavorite, paramDictionary);
    }else{
        trackActionWithParameters(kPB_FAEvent_AddedToFavorite, paramDictionary);
    }
    if(IsIpad)
        [self.delegate starButtonWasClicked];
    else{
        [self refreshDataSource];
    }
}


-(IBAction)storeButtonClick{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:IsIpad?@"Storyboard_iPad":@"Storyboard_iPhone" bundle:nil];
    storeVC = (StoreViewController*) [mainStoryboard instantiateViewControllerWithIdentifier:@"StoreViewController"];
    [self presentViewController:storeVC animated:YES completion:nil];
}

-(IBAction)showFilterButtonClick:(id)sender{
    [((JASidePanelController*)self.navigationController.parentViewController) showRightPanelAnimated:YES];
}

#pragma mark - Notification methods

-(void)dataBaseWasUpdated{
    if(isIphone){
        [self refreshDataSource];
    }
}


#pragma mark - Refresh table method

- (void)refreshRecipesViewWithDataSource:(NSArray*)dataSource{
   
    RELEASE_SAFELY(_dataSourceArray);
	_dataSourceArray = [[NSMutableArray alloc] initWithArray:dataSource];
    if(_dataSourceArray.count == 0 || !_dataSourceArray){
        _noRecordsLabel.hidden = NO;
        //_shadowImageView.hidden = YES;
    }else{
        _noRecordsLabel.hidden = YES;
        //_shadowImageView.hidden = NO;
    }
    [self._gridView reloadData];
    [self._tableView reloadData];
}

-(void) setPresentationTypeByIndex:(NSInteger)index{
    switch (index) {
        case 1:
            // list
            [self.listContentView setHidden:NO];
            [self.gridContentView setHidden:YES];
            isGridPresentation = NO;
            [self._tableView reloadData];
            break;
        case 2:
            // grid
            [self.listContentView setHidden:YES];
            [self.gridContentView setHidden:NO];
            isGridPresentation = YES;
            [self._gridView reloadData];
            break;
        default:
            break;
    }
}

#pragma mark SearchBar delegates

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self refreshDataSource];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    searchBar.text= @"";
    [searchBar resignFirstResponder];
    [self refreshDataSource];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

#pragma mark FilterPanelView delegate
- (void)filterChanged{
    //[self refreshData];
}

#pragma mark TextField delegates
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if ([string isEqualToString:@"\n"])
        [textField resignFirstResponder];
    return YES;
}

-(IBAction)tap:(id)sender{
    [self.menuViewController selectContentAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] scrollPosition:UITableViewScrollPositionTop];
}

-(IBAction) leftMenu:(id)sender{
    [self.menuViewController revealLeftMenu];
}

#pragma mark-
#pragma mark PosesSlideMenuDelegate
-(void)refreshDataSource{
    
    NSMutableString *sqlString = [NSMutableString stringWithString:@"SELECT * FROM Recipes"];
    
    if(filterSlideViewController){
        for(int i = 2; i < [filterSlideViewController.menuDatasourceArray count]; ++i){
            NSDictionary *selectedSectionDict = filterSlideViewController.menuDatasourceArray[i];
            if(i == 2){
                if([selectedSectionDict[@"rows"][0][@"isSelected"] intValue] == 0){
                    if([sqlString containsString:@" WHERE("])
                        [sqlString appendString:@" AND("];
                    else
                        [sqlString appendString:@" WHERE("];
                    
                    if([selectedSectionDict[@"rows"][1][@"isSelected"]intValue] == 1){
                        if([sqlString containsString:@" MealType LIKE"])
                            [sqlString appendString:@" OR"];
                        [sqlString appendString:@" MealType LIKE '%breakfast%'"];
                    }
                    
                    if([selectedSectionDict[@"rows"][2][@"isSelected"]intValue] == 1){
                        if([sqlString containsString:@" MealType LIKE"])
                            [sqlString appendString:@" OR"];
                        [sqlString appendString:@" MealType LIKE '%dinner%'"];
                    }
                    
                    if([selectedSectionDict[@"rows"][3][@"isSelected"]intValue] == 1){
                        if([sqlString containsString:@" MealType LIKE"])
                            [sqlString appendString:@" OR"];
                        [sqlString appendString:@" MealType LIKE '%lunch%'"];
                    }
                    
                    if([selectedSectionDict[@"rows"][4][@"isSelected"]intValue] == 1){
                        if([sqlString containsString:@" MealType LIKE"])
                            [sqlString appendString:@" OR"];
                        [sqlString appendString:@" MealType LIKE '%Snack%'"];
                    }
                    
                    [sqlString appendString:@" )"];
                }
            }
            
            if(i == 3){
                if([selectedSectionDict[@"rows"][0][@"isSelected"]intValue] == 0){
                    if([sqlString containsString:@" WHERE("])
                        [sqlString appendString:@" AND("];
                    else
                        [sqlString appendString:@" WHERE("];
                    
                    if([selectedSectionDict[@"rows"][1][@"isSelected"]intValue] == 1){
                        if([sqlString containsString:@" CategoryIngredient LIKE"])
                            [sqlString appendString:@" OR"];
                        [sqlString appendString:@" CategoryIngredient LIKE '%Pork%'"];
                    }
                    
                    if([selectedSectionDict[@"rows"][2][@"isSelected"]intValue] == 1){
                        if([sqlString containsString:@" CategoryIngredient LIKE"])
                            [sqlString appendString:@" OR"];
                        [sqlString appendString:@" CategoryIngredient LIKE '%Fruit%'"];
                    }
                    
                    if([selectedSectionDict[@"rows"][3][@"isSelected"]intValue] == 1){
                        if([sqlString containsString:@" CategoryIngredient LIKE"])
                            [sqlString appendString:@" OR"];
                        [sqlString appendString:@" CategoryIngredient LIKE '%Beef%'"];
                    }
                    
                    if([selectedSectionDict[@"rows"][4][@"isSelected"]intValue] == 1){
                        if([sqlString containsString:@" CategoryIngredient LIKE"])
                            [sqlString appendString:@" OR"];
                        [sqlString appendString:@" CategoryIngredient LIKE '%Veg%'"];
                    }
                    
                    if([selectedSectionDict[@"rows"][5][@"isSelected"]intValue] == 1){
                        if([sqlString containsString:@" CategoryIngredient LIKE"])
                            [sqlString appendString:@" OR"];
                        [sqlString appendString:@" CategoryIngredient LIKE '%Eggs%'"];
                    }
                    if([selectedSectionDict[@"rows"][6][@"isSelected"]intValue] == 1){
                        if([sqlString containsString:@" CategoryIngredient LIKE"])
                            [sqlString appendString:@" OR"];
                        [sqlString appendString:@" CategoryIngredient LIKE '%Nuts and Seeds%'"];
                    }
                    if([selectedSectionDict[@"rows"][7][@"isSelected"]intValue] == 1){
                        if([sqlString containsString:@" CategoryIngredient LIKE"])
                            [sqlString appendString:@" OR"];
                        [sqlString appendString:@" CategoryIngredient LIKE '%Chicken%'"];
                    }
                    if([selectedSectionDict[@"rows"][8][@"isSelected"]intValue] == 1){
                        if([sqlString containsString:@" CategoryIngredient LIKE"])
                            [sqlString appendString:@" OR"];
                        [sqlString appendString:@" CategoryIngredient LIKE '%Lamb%'"];
                    }
                    if([selectedSectionDict[@"rows"][9][@"isSelected"]intValue] == 1){
                        if([sqlString containsString:@" CategoryIngredient LIKE"])
                            [sqlString appendString:@" OR"];
                        [sqlString appendString:@" CategoryIngredient LIKE '%Fish and Seafood%'"];
                    }
                    if([selectedSectionDict[@"rows"][10][@"isSelected"]intValue] == 1){
                        if([sqlString containsString:@" CategoryIngredient LIKE"])
                            [sqlString appendString:@" OR"];
                        [sqlString appendString:@" CategoryIngredient LIKE '%Dressing%'"];
                    }
                    if([selectedSectionDict[@"rows"][11][@"isSelected"]intValue] == 1){
                        if([sqlString containsString:@" CategoryIngredient LIKE"])
                            [sqlString appendString:@" OR"];
                        [sqlString appendString:@" CategoryIngredient LIKE '%Duck%'"];
                    }
                    [sqlString appendString:@" )"];
                }
            }
            
            if(i == 4){
                if([selectedSectionDict[@"rows"][0][@"isSelected"]intValue] == 0){
                    if([sqlString containsString:@" WHERE("])
                        [sqlString appendString:@" AND("];
                    else
                        [sqlString appendString:@" WHERE("];
                    
                    if([selectedSectionDict[@"rows"][1][@"isSelected"]intValue] == 1){
                        if([sqlString containsString:@" CategoryType LIKE"])
                            [sqlString appendString:@" OR"];
                        [sqlString appendString:@" CategoryType LIKE '%Protein Kick%'"];
                    }
                    
                    if([selectedSectionDict[@"rows"][2][@"isSelected"]intValue] == 1){
                        if([sqlString containsString:@" CategoryType LIKE"])
                            [sqlString appendString:@" OR"];
                        [sqlString appendString:@" CategoryType LIKE '%Calcium%'"];
                    }
                    
                    if([selectedSectionDict[@"rows"][3][@"isSelected"]intValue] == 1){
                        if([sqlString containsString:@" CategoryType LIKE"])
                            [sqlString appendString:@" OR"];
                        [sqlString appendString:@" CategoryType LIKE '%Re-hydrate%'"];
                    }
                    
                    if([selectedSectionDict[@"rows"][4][@"isSelected"]intValue] == 1){
                        if([sqlString containsString:@" CategoryType LIKE"])
                            [sqlString appendString:@" OR"];
                        [sqlString appendString:@" CategoryType LIKE '%Anti-oxidant%'"];
                    }
                    if([selectedSectionDict[@"rows"][5][@"isSelected"]intValue] == 1){
                        if([sqlString containsString:@" CategoryType LIKE"])
                            [sqlString appendString:@" OR"];
                        [sqlString appendString:@" CategoryType LIKE '%Indulge%'"];
                    }
                    if([selectedSectionDict[@"rows"][6][@"isSelected"]intValue] == 1){
                        if([sqlString containsString:@" CategoryType LIKE"])
                            [sqlString appendString:@" OR"];
                        [sqlString appendString:@" CategoryType LIKE '%Vitamin E%'"];
                    }
                    
                    [sqlString appendString:@" )"];
                }
            }
            
            if(i == 5){
                if([selectedSectionDict[@"rows"][0][@"isSelected"]intValue] == 0){
                    if([sqlString containsString:@" WHERE("])
                        [sqlString appendString:@" AND("];
                    else
                        [sqlString appendString:@" WHERE("];
                    
                    if([selectedSectionDict[@"rows"][1][@"isSelected"]intValue] == 1){
                        if([sqlString containsString:@" CategoryBenefit LIKE"])
                            [sqlString appendString:@" OR"];
                        [sqlString appendString:@" CategoryBenefit LIKE '%Classic%'"];
                    }
                    
                    if([selectedSectionDict[@"rows"][2][@"isSelected"]intValue] == 1){
                        if([sqlString containsString:@" CategoryBenefit LIKE"])
                            [sqlString appendString:@" OR"];
                        [sqlString appendString:@" CategoryBenefit LIKE '%Bistro%'"];
                    }
                    
                    if([selectedSectionDict[@"rows"][3][@"isSelected"]intValue] == 1){
                        if([sqlString containsString:@" CategoryBenefit LIKE"])
                            [sqlString appendString:@" OR"];
                        [sqlString appendString:@" CategoryBenefit LIKE '%Mexican%'"];
                    }
                    
                    if([selectedSectionDict[@"rows"][4][@"isSelected"]intValue] == 1){
                        if([sqlString containsString:@" CategoryBenefit LIKE"])
                            [sqlString appendString:@" OR"];
                        [sqlString appendString:@" CategoryBenefit LIKE '%American%'"];
                    }
                    if([selectedSectionDict[@"rows"][5][@"isSelected"]intValue] == 1){
                        if([sqlString containsString:@" CategoryBenefit LIKE"])
                            [sqlString appendString:@" OR"];
                        [sqlString appendString:@" CategoryBenefit LIKE '%Middle Eastern%'"];
                    }
                    if([selectedSectionDict[@"rows"][6][@"isSelected"]intValue] == 1){
                        if([sqlString containsString:@" CategoryBenefit LIKE"])
                            [sqlString appendString:@" OR"];
                        [sqlString appendString:@" CategoryBenefit LIKE '%Asian%'"];
                    }
                    
                    [sqlString appendString:@" )"];
                    
                }
            }
            if(i == 6){
                if([selectedSectionDict[@"rows"][0][@"isSelected"]intValue] == 0){
                    if([sqlString containsString:@" WHERE("])
                        [sqlString appendString:@" AND("];
                    else
                        [sqlString appendString:@" WHERE("];
                    
                    if([selectedSectionDict[@"rows"][1][@"isSelected"]intValue] == 1){
                        if([sqlString containsString:@" CategoryMethod LIKE"])
                            [sqlString appendString:@" OR"];
                        [sqlString appendString:@" CategoryMethod LIKE '%30 minute meal%'"];
                    }
                    
                    if([selectedSectionDict[@"rows"][2][@"isSelected"]intValue] == 1){
                        if([sqlString containsString:@" CategoryMethod LIKE"])
                            [sqlString appendString:@" OR"];
                        [sqlString appendString:@" CategoryMethod LIKE '%Super Quick%'"];
                    }
                    
                    if([selectedSectionDict[@"rows"][3][@"isSelected"]intValue] == 1){
                        if([sqlString containsString:@" CategoryMethod LIKE"])
                            [sqlString appendString:@" OR"];
                        [sqlString appendString:@" CategoryMethod LIKE '%No Cook%'"];
                    }
                    
                    if([selectedSectionDict[@"rows"][4][@"isSelected"]intValue] == 1){
                        if([sqlString containsString:@" CategoryMethod LIKE"])
                            [sqlString appendString:@" OR"];
                        [sqlString appendString:@" CategoryMethod LIKE '%Slow Simmer%'"];
                    }
                    if([selectedSectionDict[@"rows"][5][@"isSelected"]intValue] == 1){
                        if([sqlString containsString:@" CategoryMethod LIKE"])
                            [sqlString appendString:@" OR"];
                        [sqlString appendString:@" CategoryMethod LIKE '%Fast Pan Fry%'"];
                    }
                    if([selectedSectionDict[@"rows"][6][@"isSelected"]intValue] == 1){
                        if([sqlString containsString:@" CategoryMethod LIKE"])
                            [sqlString appendString:@" OR"];
                        [sqlString appendString:@" CategoryMethod LIKE '%Quick Bake%'"];
                    }
                    if([selectedSectionDict[@"rows"][7][@"isSelected"]intValue] == 1){
                        if([sqlString containsString:@" CategoryMethod LIKE"])
                            [sqlString appendString:@" OR"];
                        [sqlString appendString:@" CategoryMethod LIKE '%Slow Bake%'"];
                    }
                    
                    [sqlString appendString:@" )"];
                }
            }
        }
    }
    
    if (filterSlideViewController.isFavorite){
        if([sqlString containsString:@" WHERE"])
            [sqlString appendString:@" AND"];
        else
            [sqlString appendString:@" WHERE"];
        
        [sqlString appendString:@" IsFavorite IS 'TRUE'"];
    }
    
    /*else
     [sqlString appendString:@" IsFavorite IS 'FALSE'"];*/
    
    
    if(filterSlideViewController.duration != 0){
        if([sqlString containsString:@" WHERE"])
            [sqlString appendString:@" AND"];
        else
            [sqlString appendString:@" WHERE"];
        
        [sqlString appendFormat:@" CAST(PreparationTimeInMins AS INTEGER)<=%d",filterSlideViewController.duration];
    }
    
    NSString *searchString = _recipeSearchBar.text;
    
    if (searchString.length > 0) {
        if([sqlString containsString:@" WHERE"])
            [sqlString appendString:@" AND"];
		else
            [sqlString appendString:@" WHERE"];
        [sqlString appendString:[NSString stringWithFormat:@" (DishName LIKE '%%%@%%' OR", searchString]];
        [sqlString appendString:[NSString stringWithFormat:@" Ingredients LIKE '%%%@%%')", searchString]];
    }
    
    if(filterSlideViewController.duration != 0){
        if(!searchString)
            searchString = @"";
        
        [sqlString appendString:[NSString stringWithFormat:@" ORDER BY CAST(PreparationTimeInMins AS INTEGER) ASC"]];
    }
    NSLog(@"%@",sqlString);
    _dataSourceArray = [[NSMutableArray alloc]initWithArray:[[PBDB instance].db lookupAllForSQL:sqlString]];
    [_tableView reloadData];
}

-(void) valueDidChanged:(LeftSlideMenuViewController*)posesSlideMenu{
    
    filterSlideViewController = posesSlideMenu;
    [self refreshDataSource];
}

@end
