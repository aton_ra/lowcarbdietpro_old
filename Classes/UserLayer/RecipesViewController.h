//
//  RecipesViewController.h
//  DukanDiet
//
//  Created by polar12 on 8/8/13.
//  Copyright (c) 2013 Polar-B. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageGridCell.h"
#import "FilterPanelView.h"
#import "SASlideMenuViewController.h"
#import "LeftSlideMenuViewController.h"

@protocol RecipesViewControllerDelegate <NSObject>
//- (void)selectItemAtIndex:(NSInteger *)index;
- (void)starButtonWasClicked;

@end

@interface RecipesViewController : UIViewController<PSTCollectionViewDataSource, PSTCollectionViewDelegate, UITableViewDataSource, UITableViewDelegate, LeftSlideMenuDelegate>{
    IBOutlet UILabel *_noRecordsLabel;
}
@property (nonatomic, assign) id <RecipesViewControllerDelegate> delegate;
@property (nonatomic, strong) NSMutableArray *_dataSourceArray;
@property (nonatomic, strong) PSUICollectionView *_gridView;
@property (strong, nonatomic) IBOutlet UIView *gridContentView;
@property (strong, nonatomic) IBOutlet UIView *listContentView;
@property (strong, nonatomic) IBOutlet UITableView *_tableView;
@property (strong, nonatomic) IBOutlet UISearchBar *_recipeSearchBar;
@property (strong, nonatomic) IBOutlet UITableViewCell *_tableViewCell;

@property (nonatomic,strong) SASlideMenuViewController* menuViewController;

- (void)refreshRecipesViewWithDataSource:(NSArray*)dataSource;
- (void)setPresentationTypeByIndex:(NSInteger)index;
- (void)createGridView;

-(IBAction)showFilterButtonClick:(id)sender;
-(IBAction)tap:(id)sender;
-(IBAction) leftMenu:(id)sender;


/*- (IBAction)buttonCloseClick:(id)sender;
- (IBAction)searchPopupMenuHide:(id)sender;
- (IBAction)searchTextFieldChanged:(id)sender;*/

@end
