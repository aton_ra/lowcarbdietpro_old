//
//  AboutViewController.h
//  DukanDiet
//
//  Created by polar12 on 8/12/13.
//  Copyright (c) 2013 Polar-B. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface AboutViewController : UIViewController<UIWebViewDelegate, MFMailComposeViewControllerDelegate>{
    
    UIImageView * _backgroundImageView;
    IBOutlet UITableView *_tableView;
    NSArray *_datasourceArray;
    //AdWhirlView *_bannerAdWhirlView;
    IBOutlet UIView *_ADView;
    IBOutlet UILabel *styledLabel;
    BOOL _AdReceived;
}

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIWebView *webView;


@property (strong, nonatomic) IBOutlet UIButton *mealTypeButton;
- (UITableView*) getTableView;

@end
