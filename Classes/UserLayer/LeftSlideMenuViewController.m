//
//  PosesSlideMenuViewController.m
//  Yoga
//
//  Created by polar15 on 27.03.13.
//  Copyright (c) 2013 Polar-B. All rights reserved.
//

#import "LeftSlideMenuViewController.h"
#import "UIViewController+JASidePanel.h"

@interface LeftSlideMenuViewController (){
    
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation LeftSlideMenuViewController

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){

        _isFavorite = NO;
        self.menuDatasourceArray = @[
                                     
                                     @{@"sectionTitle":@"Favorite"
                                      
                                       },
                                     @{@"sectionTitle":@"Preparation Time"
                                      
                                       },
                                     
                                     @{@"sectionTitle":@"Meal Type",
                                       @"rows":@[
                                                    [@{@"Title":@"All",@"Value":[NSNumber numberWithInt:0],@"isSelected":[NSNumber numberWithBool:YES]} mutableCopy],
                                                    [@{@"Title":@"Breakfast",@"Value":[NSNumber numberWithInt:1],@"isSelected":[NSNumber numberWithBool:NO]} mutableCopy],
                                                    [@{@"Title":@"Dinner",@"Value":[NSNumber numberWithInt:2],@"isSelected":[NSNumber numberWithBool:NO]} mutableCopy],
                                                    [@{@"Title":@"Lunch",@"Value":[NSNumber numberWithInt:3],@"isSelected":[NSNumber numberWithBool:NO]} mutableCopy],
                                                    [@{@"Title":@"Snack",@"Value":[NSNumber numberWithInt:4],@"isSelected":[NSNumber numberWithBool:NO]} mutableCopy]
                                                ]
                                       }
                                    ];
    }
    return self;
}

-(void)viewDidLoad{
    
    [super viewDidLoad];
}

#pragma mark-
#pragma mark  TableView delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.menuDatasourceArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 25;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UILabel* titleLabel = [[UILabel alloc] init];
    titleLabel.width = self.tableView.size.width;
    titleLabel.height = 25;
    titleLabel.font = [UIFont boldSystemFontOfSize:15];
    titleLabel.textAlignment = UITextAlignmentCenter;
    titleLabel.textColor = [UIColor darkGrayColor];
    titleLabel.backgroundColor = [UIColor clearColor];
    if(section != 0){
        titleLabel.text = self.menuDatasourceArray[section][@"sectionTitle"];
    }else{
        titleLabel.text = @"";
    }
    return titleLabel;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 0 || section == 1)
        return 1;
    return ((NSArray*)self.menuDatasourceArray[section][@"rows"]).count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = nil;
    if(indexPath.section == 0){ 
        cell = [tableView dequeueReusableCellWithIdentifier:@"switch_cell_id"];
        UILabel* label = (UILabel*)[cell.contentView viewWithTag:1];
        label.text = @"Favorite";
        UISwitch *swith = (UISwitch*)[cell.contentView viewWithTag:2];
        [swith addTarget:self action:@selector(starredValueChanged:) forControlEvents:UIControlEventValueChanged];
        swith.selected = _isFavorite;
        
    }else if(indexPath.section == 1){
        
        cell = [tableView dequeueReusableCellWithIdentifier:@"slider_cell_id"];
        UILabel* label = (UILabel*)[cell.contentView viewWithTag:1];
        label.text = @"All";
        
        NSInteger maxPreparationTime = 30;
        
        UISlider *durationSlider = (UISlider*)[cell.contentView viewWithTag:2];
        durationSlider.maximumValue = maxPreparationTime;
        durationSlider.minimumValue = 0;
        [durationSlider addTarget:self action:@selector(durationValueChanged:) forControlEvents:UIControlEventValueChanged];
        durationSlider.selected = _isFavorite;

    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:@"button_cell_id"];
        UILabel* label = (UILabel*)[cell.contentView viewWithTag:1];
        label.text = self.menuDatasourceArray[indexPath.section][@"rows"][indexPath.row][@"Title"];
        if([(NSNumber*)self.menuDatasourceArray[indexPath.section][@"rows"][indexPath.row][@"isSelected"] intValue] == 1){
            [self.tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
        }else{
            [tableView deselectRowAtIndexPath:indexPath animated:NO];
        }
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *selectedSectionDict = self.menuDatasourceArray[indexPath.section];
    
    selectedSectionDict[@"rows"][0][@"isSelected"] = [NSNumber numberWithInt:0];
    for(int i = 0; i<((NSArray*)selectedSectionDict[@"rows"]).count; i++){
        if(i == indexPath.row){
            if(i == indexPath.row){
                selectedSectionDict[@"rows"][i][@"isSelected"] = [NSNumber numberWithInt:0];
            }
        }
    }
    
    if(indexPath.row == 0){
        selectedSectionDict[@"rows"][0][@"isSelected"] = [NSNumber numberWithInt:1];
        for(int i=1; i<((NSArray*)selectedSectionDict[@"rows"]).count; i++){
            selectedSectionDict[@"rows"][i][@"isSelected"] = [NSNumber numberWithInt:0];
        }
    }
    
    int val = 0;
    // if all row in section are deselected - select 0 row
    for(int i=0; i<((NSArray*)selectedSectionDict[@"rows"]).count; i++){
        if( [selectedSectionDict[@"rows"][i][@"isSelected"] intValue] == 1){
            val++;
        }
    }
    if(val == 0){
        selectedSectionDict[@"rows"][0][@"isSelected"] = [NSNumber numberWithInt:1];
    }
    
    [self.tableView reloadData];
    if([self.delegate respondsToSelector:@selector(valueDidChanged:)]){
        [self.delegate valueDidChanged:self];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
        NSDictionary *selectedSectionDict = self.menuDatasourceArray[indexPath.section];
        selectedSectionDict[@"rows"][0][@"isSelected"] = [NSNumber numberWithInt:0];
    
        for(int i = 0; i<((NSArray*)selectedSectionDict[@"rows"]).count; i++){
            if(i == indexPath.row){
                if(i == indexPath.row){
                    selectedSectionDict[@"rows"][i][@"isSelected"] = [NSNumber numberWithInt:1];
                }
            }
        }
    
        if(indexPath.row == 0){
            selectedSectionDict[@"rows"][0][@"isSelected"] = [NSNumber numberWithInt:1];
            for(int i=1; i<((NSArray*)selectedSectionDict[@"rows"]).count; i++){
               selectedSectionDict[@"rows"][i][@"isSelected"] = [NSNumber numberWithInt:0];
            }
        }
        
        int val = 0;
        // if all row in section are deselected - select 0 row
        for(int i=0; i<((NSArray*)selectedSectionDict[@"rows"]).count; i++){
            if( [selectedSectionDict[@"rows"][i][@"isSelected"] intValue] == 1){
                val++;
            }
        }
        if(val == 0){
            selectedSectionDict[@"rows"][0][@"isSelected"] = [NSNumber numberWithInt:1];
        }
        
        [self.tableView reloadData];        
        if([self.delegate respondsToSelector:@selector(valueDidChanged:)]){
            [self.delegate valueDidChanged:self];
        }
    
}

#pragma mark filter table view elements

- (void)starredValueChanged:(UISwitch *)sender{
    
    if(sender.isOn){
        _isFavorite = YES;
       // _currentUserType = FilterRecipeTypeFavorite;
        trackAction(kPB_FAEvent_FavoriteFilterActivated);
    }else{
        _isFavorite = NO;
        //_currentUserType = FilterRecipeTypeAll;
        trackAction(kPB_FAEvent_FavoriteFilterDeActivated);
    }
    [self.tableView reloadData];
    if([self.delegate respondsToSelector:@selector(valueDidChanged:)]){
        [self.delegate valueDidChanged:self];
    }
//[self refreshData];
}

- (void)durationValueChanged:(UISlider *)sender{
    if(_duration != (int)sender.value){
        _duration = (int)sender.value;
        //[self refreshData];
        _duration = sender.value;
        UILabel *sliderLabel = (UILabel *)[sender.superview viewWithTag:1];
        if(_duration == 0){
            sliderLabel.text = @"All";
        }else{
            sliderLabel.text = [NSString stringWithFormat:@"%d mins",_duration];
        }
        
        //[self.tableView reloadData];
        if([self.delegate respondsToSelector:@selector(valueDidChanged:)]){
            [self.delegate valueDidChanged:self];
        }
    }
}

@end
