//
//  RegisterViewController.h
//  DukanDiet
//
//  Created by polar12 on 8/12/13.
//  Copyright (c) 2013 Polar-B. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChimpKit.h"

@interface RegisterViewController : UIViewController<ChimpKitDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *_backgroundPicture;

- (IBAction)buttonCloseClick:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UITextField *surnameTextField;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
- (IBAction)registerNowButtonClick:(id)sender;
- (void)showSubscribeError;

@property (nonatomic) BOOL doubleOptIn;

@end
