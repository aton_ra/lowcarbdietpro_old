//
//  ShoppingViewController.h
//  DukanDiet
//
//  Created by polar12 on 8/12/13.
//  Copyright (c) 2013 Polar-B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShoppingViewController : UIViewController{

IBOutlet UIImageView *_backgroundImageView;
IBOutlet UITableView *_tableView;
NSMutableArray *_dataSourceArray;
IBOutlet UILabel *_emptyListLable;
IBOutlet UIButton *clearButton;

}
- (IBAction)buttonClearClick:(id)sender;
@end
