//
//  PosesPaperFoldNavigationController.h
//  Yoga
//
//  Created by polar15 on 27.03.13.
//  Copyright (c) 2013 Polar-B. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JASidePanelController.h"
#import "RecipesViewController.h"
@interface LeftSidePanelController : JASidePanelController
@property RecipesViewController *posesViewController;
@end
