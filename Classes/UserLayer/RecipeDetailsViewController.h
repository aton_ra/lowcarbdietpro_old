//
//  RecipeDetailsViewController.h
//  DukanDiet
//
//  Created by polar12 on 8/12/13.
//  Copyright (c) 2013 Polar-B. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface RecipeDetailsViewController : UIViewController<MFMailComposeViewControllerDelegate>{
    PBHandlerView *_allPBHandlerView;
    IBOutlet UIImageView *_backgroundImageView;
    IBOutlet UIScrollView *_detailScrollView;
    IBOutlet UIImageView *_firstSeparatorImageView;
    IBOutlet UIImageView *_dishImageView;
    IBOutlet UIView *_detailTimeServingsView;
    IBOutlet UILabel *_timeLabel;
    IBOutlet UILabel *_servingsLabel;
    IBOutlet UIButton *addToShopListButton;
    IBOutlet UILabel *_titleLabel;
    IBOutlet UILabel *_phaseLabel;
    IBOutlet UILabel *dishNameLabel;
    IBOutlet UILabel *_mealType1Label;
    IBOutlet UILabel *_detailDescriptionLabel;
    //IBOutlet UIView *_nutritionalInfoHeaderView;
    //IBOutlet UILabel *_nutritionalInfoDescriptionLabel;
    IBOutlet UIView *_preparationViewHeader;
    IBOutlet UILabel *_preparationLabelDescription;
    IBOutlet UIView *_ingredientsHeaderView;
    IBOutlet UILabel *_ingredientsLabelDescriptionInfo;
    
    IBOutlet UILabel *_nutritionalLabelDescriptionInfo;
    IBOutlet UIView *_nutritionalHeaderView;
    
    IBOutlet UINavigationBar *navigationMenu;
    IBOutlet UIImageView *segmentControlMenuImage;
    
    IBOutlet UIView *_detailView;
    IBOutlet UIView *_detailContentView;
    IBOutlet UIView *_nutritionView;    // Used another way on iPad
    IBOutlet UIView *_ingredientsView; // Used another way on iPad
    IBOutlet UIView *_preparationView; // Used another way on iPad
    
    //IBOutlet UIButton *emailToFriendButton;
    IBOutlet UILabel *_detailsDescriptionLabel;
    
    IBOutlet UIScrollView *_ingredientsScrollView;
    IBOutlet UIView *_ingredientsContentView;
    
    IBOutlet UITextView *_nutritionTextView;
    IBOutlet UITextView *_ingredientsTextView;
    IBOutlet UITextView *_preparationTextView;
    //IBOutlet UILabel *_dishNameLabel;
    IBOutlet UILabel *_mealTypeLabel;
    
    IBOutlet UISegmentedControl *_detailsSegmentControl;
}

- (IBAction) addToShoppingListButtonTouch:(id)sender;
- (void) closeButtonClick;
-(void)setRecipeData:(NSDictionary*)recipeData;

@end
