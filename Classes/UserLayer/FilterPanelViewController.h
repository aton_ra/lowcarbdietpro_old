//
//  FilterPanelViewController.h
//  PaleoDiet
//
//  Created by polar12 on 9/2/13.
//  Copyright (c) 2013 Polar-B. All rights reserved.
//

#import "SASlideMenuViewController.h"

@interface FilterPanelViewController : SASlideMenuViewController<SASlideMenuDataSource , SASlideMenuDelegate, UITableViewDataSource, UITableViewDelegate>


typedef enum{
	FilterRecipeAll = 0,
	FilterRecipeFavorite = 1,
	FilterRecipeNotFavorite = 2
}FilterRecipeTypes;

typedef enum{
	FilterMealAll = 0,
	FilterMealBreakfast = 1,
    FilterMealLunch = 2,
   	FilterMealDinner = 3,
   	FilterMealSnack = 4,
}FilterMealTypes;

typedef enum{
	MainIngredientAll = 0,
	MainIngredientPork = 1,
    MainIngredientFruit = 2,
   	MainIngredientBeef = 3,
   	MainIngredientVeg = 4,
    MainIngredientEggs = 5,
    MainIngredientNutsAndSeeds = 6,
    MainIngredientChicken = 7,
    MainIngredientLamb = 8,
    MainIngredientFishAndSeafood = 9,
    MainIngredientDressing = 10,
    MainIngredientDuck = 11,
}FilterMainIngredient;

typedef enum{
	EffectAll = 0,
	EffectProteinKick = 1,
    EffectCalcium = 2,
   	EffectRehydrate = 3,
   	EffectAntyOxidant = 4,
    EffectIndulge = 5,
    EffectVitaminE = 6,
}FilterEffect;

typedef enum{
	CuisineAll = 0,
	CuisineClassic = 1,
    CuisineBistro = 2,
   	CuisineMexican = 3,
   	CuisineAmerican = 4,
    CuisineMiddleEastern = 5,
    CuisineAsian = 6,
}FilterCuisine;

typedef enum{
    CookingMethodAll = 0,
	CookingMethod30MinMeal = 1,
	CookingMethodSuperQuick = 2,
    CookingMethodNoCook = 3,
   	CookingMethodSlowSimmer = 4,
    CookingMethodFastPanFry = 5,
    CookingMethodQuickBake = 6,
    CookingMethodSlowBake = 7,
    CookingMethodGrill = 8,
}FilterCookingMethod;

@property (strong, nonatomic) IBOutlet UITableView *_tableView;

@end
