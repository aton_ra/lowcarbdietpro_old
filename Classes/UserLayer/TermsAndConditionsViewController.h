//
//  TermsAndConditionsViewController.h
//  DukanDiet
//
//  Created by polar12 on 8/12/13.
//  Copyright (c) 2013 Polar-B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermsAndConditionsViewController : UIViewController<UIWebViewDelegate>{
    
    UIImageView * _backgroundImageView;
    NSArray *_datasourceArray;
    BOOL _AdReceived;
    IBOutlet UIActivityIndicatorView *_loadingView;
    IBOutlet UIWebView *_aboutWebView;
}
@end
