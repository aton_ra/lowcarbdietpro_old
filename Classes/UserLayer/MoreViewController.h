//
//  MoreViewController.h
//  DukanDiet
//
//  Created by polar12 on 8/12/13.
//  Copyright (c) 2013 Polar-B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoreViewController : UIViewController<NSURLConnectionDelegate>{
    IBOutlet UIWebView *_webView;
    IBOutlet UIActivityIndicatorView *_activiityIndicator;
    IBOutlet UIImageView * _navigationBarImageView;
    NSURL *_iTunesURL;
}
@end
