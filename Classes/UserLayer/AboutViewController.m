//
//  AboutViewController.m
//  DukanDiet
//
//  Created by polar12 on 8/12/13.
//  Copyright (c) 2013 Polar-B. All rights reserved.
//

#import "AboutViewController.h"
#import "RegisterViewController.h"
#import "TermsAndConditionsViewController.h"
#import "StoreViewController.h"
#import "PBParsecomManager.h"
#import "Appirater.h"

@interface AboutViewController (){
    
    RegisterViewController *regVC;
}
@end

@implementation AboutViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"%@",[[PBSettings instance] app:kPBSettings_Public_About]);
    NSMutableString *aboutText = [[NSMutableString alloc] initWithString:[[PBSettings instance] app:kPBSettings_Public_About]];
    if(isIphone){
        NSRange range = [aboutText rangeOfString:@"23"];
        if(range.location != NSNotFound){
            [aboutText replaceCharactersInRange:range withString:@"17"];
        }
    }
    
    [_webView loadHTMLString:/*[[PBSettings instance] app:kPBSettings_Public_About] baseURL:nil]*/aboutText baseURL:nil];
    _webView.opaque = NO;
    _webView.scrollView.scrollEnabled = NO;
    _webView.scrollView.bounces = NO;
    _webView.backgroundColor = [UIColor clearColor];
    
    _tableView.backgroundView = nil;
    _tableView.backgroundColor = [UIColor clearColor];

    [_tableView setScrollEnabled:NO];
    
    
    _datasourceArray = [[NSArray alloc] initWithObjects:[[NSArray alloc] initWithObjects:@"Write review" ,@"Tell a friend", nil],
                                                        [[NSArray alloc] initWithObjects:@"Contact us" , @"Sign up for updates", nil],
                                                        [[NSArray alloc] initWithObjects:@"Update recipes" ,@"Terms and conditions", nil],
                                                        nil];
    if(isIphone){
        [self.navigationItem setTitle:@"About"];
    }
    
    if(IsiOS7){
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"bg_nav_bar_cut.png"] forBarMetrics:UIBarMetricsDefault];
    }
    else{
        [self.navigationController.navigationBar setBackgroundColor:[UIColor blackColor]];
    }
	// Do any additional setup after loading the view.
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    if(IsIpad){
        [_webView sizeToFit];
        
        if(fromInterfaceOrientation != UIInterfaceOrientationLandscapeLeft && fromInterfaceOrientation != UIInterfaceOrientationLandscapeRight){
            [_tableView setOrigin:CGPointMake(_tableView.origin.x, _webView.frame.origin.y + _webView.frame.size.height - 20.0 )];
        }else{
            [_tableView setOrigin:CGPointMake(_tableView.origin.x, _webView.frame.origin.y + _webView.frame.size.height - 20.0 )];

        }
        
        [_scrollView setContentSize:CGSizeMake(_scrollView.contentSize.width, _tableView.origin.y + _tableView.height + 10)];
        //[_tableView reloadData];
    }
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [_tableView reloadData];

}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    if (UI_USER_INTERFACE_IDIOM()== UIUserInterfaceIdiomPad){
        return ((UIInterfaceOrientationLandscapeLeft == toInterfaceOrientation) || (UIInterfaceOrientationLandscapeRight == toInterfaceOrientation) || (UIInterfaceOrientationPortrait == toInterfaceOrientation));
    }
    return (UIInterfaceOrientationPortrait == toInterfaceOrientation);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[_datasourceArray objectAtIndex:section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell* cell = nil;
    static NSString* cellIdentifier = @"CellIdentifier";
    
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.font = [UIFont boldSystemFontOfSize:IsIpad?22:17];
    }
    
    //CGRect frame = cell.frame;
    //cell.frame = frame;
    [cell setWidth:tableView.frame.size.width];
    cell.textLabel.text = [[_datasourceArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if(IsiOS7){
        
        UIView *cellBack = [[UIView alloc] initWithFrame:cell.bounds];
        [cellBack setBackgroundColor:[UIColor whiteColor]];
        [cell setBackgroundColor:[UIColor clearColor]];
        
        if(indexPath.row % 2 == 1){
            UIView *backView = [self roundCornersOnView:cellBack onTopLeft:NO topRight:NO bottomLeft:YES bottomRight:YES radius:5.0];
            [cell setBackgroundView:backView];
        }else{
            UIView *backView = [self roundCornersOnView:cellBack onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:5.0];
            [cell setBackgroundView:backView];
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == 2){
        if(indexPath.row == 0){
            if([Reachability reachabilityForInternetConnection].currentReachabilityStatus != NotReachable){
                //[self showLoading];
                [[PBParsecomManager sharedManager] updateDataFromParse:@"Recipes"
                 
                                                              onComplete:^(BOOL success, NSString *message){
                                                                  
                                                                  if(success){
                                                                      //[self hideLoading];
                                                                      //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                                                      //[alert show];
                                                                      [Appirater showRatingPopup];
                                                                      [[NSNotificationCenter defaultCenter] postNotificationName:@"dataBaseWasUpdated" object:nil];
                                                                  }
                                                                  else{
                                                                      //[self hideLoading];
                                                                     // UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error update data base" message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                                                      //[alert show];
                                                                      //NSLog(@"Error: %@", message);
                                                                  }
                                                              }];
                
            }else{
                //[self hideLoading];
                //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Checking for updates failed" message:@"No internet connection" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                //[alert show];
            }
            
            trackAction(kPB_FAEvent_UpdateRecipesClicked);
        }else{
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:IsIpad?@"Storyboard_iPad":@"Storyboard_iPhone" bundle:nil];
            TermsAndConditionsViewController *termsVC = (TermsAndConditionsViewController*) [mainStoryboard instantiateViewControllerWithIdentifier:@"TermsAndConditionsViewController"];
            
            UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:termsVC];
            [self presentViewController:navigation animated:YES completion:nil];
            trackAction(kPB_FAEvent_TermsAndConditionsClicked);
        }
    }
    else if(indexPath.section == 1){
        if(indexPath.row == 0){
            NSString *device = [[UIDevice currentDevice] model];
            if([device isEqual: @"Unknown iOS device"]){
                device = [[UIDevice currentDevice] systemVersion];
            }
            NSString *debugInfo = [NSString stringWithFormat:@"\n\n\n--------------------\nApp version - %@\nOS version - %@\nDevice type - %@\n--------------------",[[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey],[[UIDevice currentDevice] systemVersion],device];
            
            [self _sendEmailTo:@"support@fitkitapps.com"
                   withSubject:@"Low Carb Diet Pro - Comments and Suggestions"
                      withBody:debugInfo];
            
            trackAction(kPB_FAEvent_ContactUsClicked);
        }else{
            if(!regVC){
                
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:IsIpad? @"Storyboard_iPad":@"Storyboard_iPhone" bundle:nil];
                regVC = (RegisterViewController*)[mainStoryboard instantiateViewControllerWithIdentifier:@"RegisterViewController"];
            }
            
            
            /*if(IsiOS7)
                regVC.modalTransitionStyle*/
            regVC.modalPresentationStyle = UIModalPresentationFormSheet;
            regVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            [self presentViewController:regVC animated:YES completion:nil];
            
            if(IsIpad){
                float x, y;
                
                CGRect screenBounds = [UIScreen mainScreen].bounds ;
                CGFloat width = CGRectGetWidth(screenBounds);
                CGFloat height = CGRectGetHeight(screenBounds);
                
                if(UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])){
                    
                    [regVC.view.superview setHeight:407];
                    [regVC.view.superview setWidth:490];
                    y  = (width - regVC.view.superview.size.width) / 2;
                    x  = (height - regVC.view.superview.size.height) / 2;
                    
                    [regVC.view.superview setOrigin:CGPointMake(y, x)];
                }
                else{
                    [regVC.view.superview setWidth:490];
                    [regVC.view.superview setHeight:407];
                    // in landscape height is width = 1024 and width is height = 768
                    y  = (height - regVC.view.superview.size.width) / 2;
                    x  = (width - regVC.view.superview.size.height) / 2;
                    
                    [regVC.view.superview setOrigin:CGPointMake(y, x)];
                }
                
                [regVC.view.superview setBackgroundColor:[UIColor clearColor]];
                [regVC.view.superview.layer setCornerRadius:14.5];
                [regVC.view.superview.layer setMasksToBounds:YES];
            }
            
            trackAction(kPB_FAEvent_SignUpForUpdatesClicked);
        }
        
    }
    else if(indexPath.section == 0){
        if(indexPath.row == 0){
            NSString *templateReviewURL = @"http://itunes.apple.com/app/diabetes-diet/idAPP_ID?mt=8";
            NSString *reviewURL = [templateReviewURL stringByReplacingOccurrencesOfString:@"APP_ID" withString:[[PBSettings instance] app:kPBSettings_Public_AppSoftwareId]];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:reviewURL]];
            trackAction(kPB_FAEvent_WriteReviewLinkClicked);
        }else{
            [self _sendEmailTo:@""
                   withSubject:@""
                      withBody:[[PBSettings instance] app:kPBSettings_Public_MyBody]];
            trackAction(kPB_FAEvent_TellFriendClicked);
        }
        
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [_datasourceArray count];
}


-(UITableView*) getTableView{
    return _tableView;
}

#pragma mark - Send mail methods
- (void)_sendEmailTo:(NSString *)to withSubject:(NSString *)subject withBody:(NSString *)body {
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
	if (mailClass != nil) {
		// We must always check whether the current device is configured for sending emails
		if ([mailClass canSendMail]){
			MFMailComposeViewController *mailSender = [[MFMailComposeViewController alloc] init];
			mailSender.mailComposeDelegate = self;
			mailSender.navigationBar.tintColor = [UIColor blackColor];
			
            [mailSender setToRecipients:[NSArray arrayWithObject:to]];
            [mailSender setSubject:subject];
			[mailSender setMessageBody:body isHTML:NO];
			[self presentViewController:mailSender animated:YES completion:nil];
		}
        else {
            ShowShortMessage(@"Error", @"No mail accounts found");
        }
	}
    else {
        ShowShortMessage(@"Error", @"Your device do not supports EMails");
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    
    NSString *msg;
    NSString *title;
    if(result ==  MFMailComposeResultCancelled) {
        [controller dismissViewControllerAnimated:YES completion:nil];
        return;
    }else if(result ==  MFMailComposeResultSaved){
        msg = @"message  is Saved pat";
        title = @"";
    }else if(result ==  MFMailComposeResultSent){
        msg = @"message is Sent pat";
        title = @"Sent button ";

    }else if(result ==  MFMailComposeResultFailed){
        msg = @"failed";
        title = @"";

    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:msg
                  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    
    [controller dismissViewControllerAnimated:YES completion:nil];
}

-(UIView *)roundCornersOnView:(UIView *)view onTopLeft:(BOOL)tl topRight:(BOOL)tr bottomLeft:(BOOL)bl bottomRight:(BOOL)br radius:(float)radius {
    
    UIRectCorner corner; //holds the corner
    
    
    //Determine which corner(s) should be changed
    if (tl) {
        corner = UIRectCornerTopLeft;
    }
    if (tr) {
        corner = UIRectCornerTopRight;
    }
    if (bl) {
        corner = UIRectCornerBottomLeft;
    }
    if (br) {
        corner = UIRectCornerBottomRight;
    }
    if (tl && tr) { //top
        corner = UIRectCornerTopRight | UIRectCornerTopLeft;
    }
    if (bl && br) { //bottom
        corner = UIRectCornerBottomLeft | UIRectCornerBottomRight;
    }
    if (tl && bl) { //left
        corner = UIRectCornerTopLeft | UIRectCornerBottomLeft;
    }
    if (tr && br) { //right
        corner = UIRectCornerTopRight | UIRectCornerBottomRight;
    }
    
    if (tl & tr & bl & br) {
        corner = UIRectCornerAllCorners;
    }
    
    UIView *roundedView = view;
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:roundedView.bounds byRoundingCorners:corner cornerRadii:CGSizeMake(radius, radius)];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = roundedView.bounds;
    maskLayer.path = maskPath.CGPath;
    roundedView.layer.mask = maskLayer;
    
    [roundedView setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
    [roundedView setTag:15];
    
    return roundedView;
}

#pragma mark - Button action

-(IBAction)storeButtonClick{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:IsIpad?@"Storyboard_iPad":@"Storyboard_iPhone" bundle:nil];
    StoreViewController *storeVC = (StoreViewController*) [mainStoryboard instantiateViewControllerWithIdentifier:@"StoreViewController"];
    [self presentViewController:storeVC animated:YES completion:nil];
}

#pragma mark - Web view delegate

- (void)webViewDidFinishLoad:(UIWebView *)aWebView {
    CGRect frame = aWebView.frame;
    frame.size.height = 1;
    aWebView.frame = frame;
    CGSize fittingSize = [aWebView sizeThatFits:CGSizeZero];
    frame.size = fittingSize;
    aWebView.frame = frame;
        
    if(isIphone){
        [aWebView setOrigin:CGPointMake(aWebView.origin.x, aWebView.origin.y + 10)];
        [aWebView setHeight:584];
    }
    [_tableView setHeight:_tableView.contentSize.height];
    
    [_tableView setOrigin:CGPointMake(_tableView.origin.x, aWebView.frame.size.height + (isIphone && !(IsiOS7)?50:0))];
    [_scrollView setContentSize:CGSizeMake(_scrollView.contentSize.width, aWebView.frame.size.height + (isIphone && !(IsiOS7)?70:20) + _tableView.height)];
}

@end
