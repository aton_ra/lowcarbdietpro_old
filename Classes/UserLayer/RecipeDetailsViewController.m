//
//  RecipeDetailsViewController.m
//  DukanDiet
//
//  Created by polar12 on 8/12/13.
//  Copyright (c) 2013 Polar-B. All rights reserved.
//

#import "RecipeDetailsViewController.h"
#import "RegexKitLite.h"

@interface RecipeDetailsViewController ()

@end

@implementation RecipeDetailsViewController

NSDictionary *_dataDictionary;
MFMailComposeViewController *mc;

#pragma mark - Initialization

-(void)setRecipeData:(NSDictionary*)recipeData{
    
    _dataDictionary = [[NSDictionary alloc] initWithDictionary:recipeData];
}

- (void)setDetailView {
    
    UIImage *image = [UIImage imageNamed:[_dataDictionary valueForKey:@"Image"]];
    if (!image) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
        NSLog(@"PATH: %@",[documentsPath stringByAppendingPathComponent:[_dataDictionary valueForKey:@"Image"]]);
        image = [UIImage imageWithContentsOfFile:[documentsPath stringByAppendingPathComponent:[_dataDictionary valueForKey:@"Image"]]];
    }
    
    CGFloat width = 0.0;
    CGFloat height = 0.0;
    
    if(image.size.width > image.size.height){
        width = self.view.width - 100.0;
        height = width*image.size.height/image.size.width;
    }else{
        height = IsIpad?350.0:150.0;
        width = height*image.size.width/image.size.height;
    }
    
    UIImage *sizeFixImage = nil;
    
    sizeFixImage = [UIImage imageWithImage:image scaledToSize:CGSizeMake(width, height)];
    
    addToShopListButton.hidden = YES;
    
    if(isIphone){
        [_dishImageView setImage:sizeFixImage];
        
        if([[_dataDictionary valueForKey:@"Image"]  isEqualToString:@"-"]){
            [_dishImageView setHidden:YES];
            [_dishImageView setHeight:0];
        }else{
            [_dishImageView setSize:sizeFixImage.size];
            _dishImageView.centerX = self.view.centerX;
            [_dishImageView setImage:sizeFixImage];
            _dishImageView.layer.cornerRadius = 5.0;
            _dishImageView.layer.masksToBounds = YES;
            _dishImageView.frame = CGRectMake(_dishImageView.left, _dishImageView.top, sizeFixImage.size.width, sizeFixImage.size.height);
        }
        
        _detailTimeServingsView.top = _dishImageView.bottom;
        
        NSMutableString *preparatTime;

        preparatTime = [[NSMutableString alloc] initWithString:[_dataDictionary valueForKey:@"PreparationTimeInMins"]];
        [preparatTime appendString:@" min"];
        
        
        [_timeLabel setText:/*[NSString stringWithFormat:@"%@", [_dataDictionary valueForKey:@"PreparationTimeInMins"]]*/preparatTime];
        [_timeLabel sizeToFit];
        [_servingsLabel setText:[NSString stringWithFormat:@"%@", [_dataDictionary valueForKey:@"ServingsCount"]]];
        
        dishNameLabel.text =[_dataDictionary valueForKey:@"DishName"];
        [dishNameLabel setTop:_detailTimeServingsView.bottom + 15];
        [dishNameLabel sizeToFit];
        
        _mealTypeLabel.text =[_dataDictionary valueForKey:@"MealType"];
        [_mealTypeLabel setTop:dishNameLabel.bottom + 7];
        [_mealTypeLabel sizeToFit];
        
        [_detailsDescriptionLabel setTop:_mealTypeLabel.bottom + 7];
        [_detailsDescriptionLabel setText:[_dataDictionary valueForKey:@"DetailDescription"]];
        [_detailsDescriptionLabel sizeToFit];
        
        [_detailContentView setHeight:_detailsDescriptionLabel.bottom + 7];
        [_detailScrollView setContentSize:CGSizeMake(_detailScrollView.width, _detailContentView.bottom+10 + 44.0)];
    }else{
        
        UIImage *sizeFixImage =nil;
        if (image.size.height > _dishImageView.height || image.size.width > _dishImageView.width) {
            sizeFixImage = [UIImage imageWithImage:image scaledToSize:_dishImageView.size];
        } else {
            sizeFixImage = image;
        }
        [_dishImageView setSize:sizeFixImage.size];
        _dishImageView.centerX = self.view.centerX;
        [_dishImageView setImage:sizeFixImage];
        _dishImageView.layer.cornerRadius = 5.0;
        _dishImageView.layer.masksToBounds = YES;
        _dishImageView.frame = CGRectMake(_dishImageView.left, _dishImageView.top + 10, sizeFixImage.size.width, sizeFixImage.size.height);
        
        /*[_dishImageView setSize:sizeFixImage.size];
        _dishImageView.centerX = self.view.centerX;
        [_dishImageView setImage:sizeFixImage];
        _dishImageView.layer.cornerRadius = 5.0;
        _dishImageView.layer.masksToBounds = YES;
        _dishImageView.frame = CGRectMake(([[UIScreen mainScreen] bounds].size.width - _dishImageView.width)/2, _dishImageView.top + 10, sizeFixImage.size.width, sizeFixImage.size.height);*/
        
        _detailTimeServingsView.top = _dishImageView.bottom + 13;
        [_timeLabel setText:[NSString stringWithFormat:@"%@", [_dataDictionary valueForKey:@"PreparationTimeInMins"]]];
        [_servingsLabel setText:[NSString stringWithFormat:@"%@", [_dataDictionary valueForKey:@"ServingsCount"]]];
        dishNameLabel.text = [_dataDictionary valueForKey:@"DishName"];
        [dishNameLabel setOrigin:CGPointMake(dishNameLabel.origin.x, _detailTimeServingsView.bottom + 10)];
        [dishNameLabel sizeToFit];
        
        if([[_dataDictionary valueForKey:@"MealType"] isEqualToString:@"-"]){
            [_mealType1Label setHidden:YES];
            [_mealType1Label setUserInteractionEnabled:NO];
            [_mealType1Label setHeight:1];
            _mealType1Label.top = dishNameLabel.bottom+5;
        }else{
            _mealType1Label.text = [_dataDictionary valueForKey:@"MealType"];
            _mealType1Label.top = dishNameLabel.bottom + 10;
            [_mealType1Label sizeToFit];
        }
        
        if([[_dataDictionary valueForKey:@"Phase"]  isEqualToString:@"-"]){
            [_phaseLabel setHidden:YES];
            [_phaseLabel setUserInteractionEnabled:NO];
            _phaseLabel.top = _mealType1Label.top+5;
        }else{
            _phaseLabel.text = [NSString stringWithFormat:@"Phase: %@", [_dataDictionary objectForKey:@"Phase"]];
            [_phaseLabel setTop:_mealType1Label.bottom + 10];
            [_phaseLabel sizeToFit];
        }
        
        if([[_dataDictionary valueForKey:@"DetailDescription"]  isEqualToString:@"-"]){
            [_detailDescriptionLabel setHidden:YES];
            [_detailDescriptionLabel setHeight:1];
            _detailDescriptionLabel.top = _mealType1Label.bottom+5;
        }else{
            _detailDescriptionLabel.top = _mealType1Label.bottom + 5;
            _detailDescriptionLabel.text = [_dataDictionary valueForKey:@"DetailDescription"];
            [_detailDescriptionLabel sizeToFit];
        }
        
        if([[_dataDictionary valueForKey:@"Ingredients"]  isEqualToString:@"-"]){
            [_ingredientsHeaderView setHidden:YES];
            [_ingredientsHeaderView setHeight:1];
            _ingredientsHeaderView.top = _detailDescriptionLabel.bottom;
            [_ingredientsLabelDescriptionInfo setHidden:YES];
            [_ingredientsLabelDescriptionInfo setHeight:1];
            _ingredientsLabelDescriptionInfo.top = _ingredientsHeaderView.bottom;
        }else{
            _ingredientsHeaderView.top = _detailDescriptionLabel.bottom + 15;
            _ingredientsLabelDescriptionInfo.top = _ingredientsHeaderView.bottom + 7;
            _ingredientsLabelDescriptionInfo.text = [_dataDictionary valueForKey:@"Ingredients"];
            [_ingredientsLabelDescriptionInfo sizeToFit];
        }
        
        [addToShopListButton setTop:_ingredientsLabelDescriptionInfo.bottom + 7];
        
        if(isShoppingCardPurchased){
            UIImage *resizedImage = [[UIImage imageNamed:@"black_button"] stretchableImageWithLeftCapWidth:[UIImage imageNamed:@"black_button"].size.width/2 topCapHeight:[UIImage imageNamed:@"black_button"].size.height/2];
            [addToShopListButton setBackgroundImage:resizedImage forState:UIControlStateNormal];
            addToShopListButton.hidden = NO;
            [addToShopListButton setCenterX:self.view.width/2];
        }
        
        
        if([[_dataDictionary valueForKey:@"NutritionalInfo"]  isEqualToString:@"-"]){
            [_nutritionalHeaderView setHidden:YES];
            _nutritionalHeaderView.top = _ingredientsLabelDescriptionInfo.bottom;
            [_nutritionalLabelDescriptionInfo setHidden:YES];
            _nutritionalLabelDescriptionInfo.top = _ingredientsHeaderView.top;
        }else{
            _nutritionalHeaderView.top = addToShopListButton.hidden? _ingredientsLabelDescriptionInfo.bottom + 10:addToShopListButton.bottom + 10;
            _nutritionalLabelDescriptionInfo.top = _nutritionalHeaderView.bottom;
            _nutritionalLabelDescriptionInfo.text = [_dataDictionary valueForKey:@"NutritionalInfo"];
            [_nutritionalLabelDescriptionInfo sizeToFit];
        }

        if([[_dataDictionary valueForKey:@"Preparation"]  isEqualToString:@"-"]){
            [_preparationViewHeader setHidden:YES];
            _preparationViewHeader.top = _nutritionalLabelDescriptionInfo.bottom;
            [_preparationLabelDescription setHidden:YES];
            _preparationLabelDescription.top = _nutritionalHeaderView.top;
        }else{
            _preparationViewHeader.top = _nutritionalLabelDescriptionInfo.bottom + 10;
            _preparationLabelDescription.top = _preparationViewHeader.bottom;
            _preparationLabelDescription.text = [_dataDictionary valueForKey:@"Preparation"];
            [_preparationLabelDescription sizeToFit];
        }
        
        [_detailScrollView setContentSize:CGSizeMake(_detailScrollView.width, _preparationLabelDescription.bottom+90)];
    }
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(recipePictureTap)];
    [_dishImageView addGestureRecognizer:tap];

}

-(void)setNutritionView{
    [_nutritionTextView setText:[_dataDictionary valueForKey:@"NutritionalInfo"]];
}

- (void)setIngredientsView {
    
    [_ingredientsScrollView setScrollEnabled:YES];
    [_ingredientsTextView setText:[_dataDictionary valueForKey:@"Ingredients"]];
    [_ingredientsTextView setScrollEnabled:NO];
    [_ingredientsTextView setSize:CGSizeMake(320, _ingredientsTextView.contentSize.height)];
    [addToShopListButton setTop:_ingredientsTextView.contentSize.height + 7];
    [_ingredientsContentView setHeight:_ingredientsTextView.bottom + 7];
    [_ingredientsScrollView setContentSize:CGSizeMake(320, _ingredientsTextView.bottom + 7 + 10)];
    addToShopListButton.hidden = YES;
    
    if(isShoppingCardPurchased){
        
        UIImage *resizedImage = [[UIImage imageNamed:@"black_button"] stretchableImageWithLeftCapWidth:[UIImage imageNamed:@"black_button"].size.width/2 topCapHeight:[UIImage imageNamed:@"black_button"].size.height/2];
        [addToShopListButton setBackgroundImage:resizedImage forState:UIControlStateNormal];
        
        [addToShopListButton setTop:_ingredientsTextView.contentSize.height + 7];
        [_ingredientsContentView setHeight:addToShopListButton.bottom + 7];
        [_ingredientsScrollView setContentSize:CGSizeMake(320, addToShopListButton.bottom + 7 + 10)];
        addToShopListButton.hidden = NO;
    }
}

- (void)setPreparationView {
    
    [_preparationTextView setText:[_dataDictionary valueForKey:@"Preparation"]];
}

#pragma mark - Lyfecicle

-(BOOL)prefersStatusBarHidden{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    int value = [[[PBSettings instance] user:kPBSettings_Public_NumberOfWatchedRecipes] intValue];
    ++value;
    [[PBSettings instance] user:[NSNumber numberWithInt:value] key:kPBSettings_Public_NumberOfWatchedRecipes];
    
	[self setDetailView];
    
    //set navigation title
    UILabel* titleLabel = [[UILabel alloc] init];
    titleLabel.font =  [UIFont fontWithName:@"Helvetica-Bold" size:20];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.text = @"LowCarb Diet";
    [titleLabel sizeToFit];
    self.navigationItem.titleView = titleLabel;
    
    UIButton *closeBtn = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, IsIpad?70:55, IsIpad?37:30)];
    [closeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [closeBtn setTitle:@"Close" forState:UIControlStateNormal];
    closeBtn.titleLabel.font = [UIFont systemFontOfSize:isIphone?14:17];
    [closeBtn setBackgroundImage:[UIImage imageNamed:@"black_button.png"] forState:UIControlStateNormal];
    [closeBtn addTarget:self action:@selector(closeButtonClick) forControlEvents:(UIControlEventTouchDown)];
    
    UIBarButtonItem *closeButton = [[UIBarButtonItem alloc] initWithCustomView:closeBtn];
    
    [self.navigationItem setLeftBarButtonItem:closeButton];
    
    if(IsIpad){
        if(IsiOS7){
            
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"bg_nav_bar_cut.png"] forBarMetrics:UIBarMetricsDefault];
        }
        else{
            self.navigationController.navigationBar.tintColor = [UIColor blackColor];
            self.navigationController.navigationBar.backgroundColor = [UIColor blackColor];
        }
    }
    
    if(isIphone){
        
        if(IsiOS7){
             [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"bg_nav_bar_cut.png"] forBarMetrics:UIBarMetricsDefault];
        }
        else{
            self.navigationController.navigationBar.tintColor = [UIColor blackColor];
            self.navigationController.navigationBar.backgroundColor = [UIColor blackColor];
        }
        
        _detailsSegmentControl = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"Details",@"Nutritional", @"Ingredients", @"Preparation", nil]];
        _detailsSegmentControl.segmentedControlStyle = UISegmentedControlStyleBar;
        _detailsSegmentControl.selectedSegmentIndex = 0;
        
        [_detailsSegmentControl setWidth:77 forSegmentAtIndex:0];
        [_detailsSegmentControl setWidth:77 forSegmentAtIndex:1];
        [_detailsSegmentControl setWidth:77 forSegmentAtIndex:2];
        [_detailsSegmentControl setWidth:77 forSegmentAtIndex:3];
        
        _detailsSegmentControl.frame = CGRectMake(5, (IsiOS7)?50:7, 308, 29);
        _detailsSegmentControl.userInteractionEnabled = YES;
        if(IsiOS7){
            [_detailsSegmentControl setSegmentedControlStyle:UISegmentedControlStyleBar];
            [segmentControlMenuImage setImage:nil];
            [navigationMenu setBarTintColor:[UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:1]];
            [_detailsSegmentControl setBackgroundColor:[UIColor colorWithRed:30.0/255.0 green:30.0/255.0 blue:30.0/255.0 alpha:1]];
            [_detailsSegmentControl setTintColor:[UIColor whiteColor]];
        }else{
            [_detailsSegmentControl setTintColor:[UIColor blackColor]];
            _detailsSegmentControl.segmentedControlStyle = UISegmentedControlStyleBar;
        }
        [_detailsSegmentControl addTarget:self action:@selector(detailsSegmentValueChanged:) forControlEvents:UIControlEventValueChanged];
        [self.view addSubview:_detailsSegmentControl];
        [self.view bringSubviewToFront:_detailsSegmentControl];
        
        [_detailsSegmentControl setHeight:30];
        
        _allPBHandlerView = [[PBHandlerView alloc] initWithFrame:self.view.frame];
        
        //[_allPBHandlerView setFrame:CGRectMake(0, 0.0, 320, 400)];
        [_allPBHandlerView setBackgroundColor:[UIColor whiteColor]];
        _allPBHandlerView.top = (IsiOS7)? 87:44;
        // _allPBHandlerView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:_allPBHandlerView];
        
        [self setPreparationView];
        [self setIngredientsView];
        [self setNutritionView];
        
        [_allPBHandlerView pushView:_preparationView animationType:PBHandlerView_AnimationType_None];
        [_allPBHandlerView pushView:_detailView animationType:PBHandlerView_AnimationType_None];
        
        
        if(isIphone5){
            [_detailView setFrame:CGRectMake(0, 0, 320, 525)];
            [_detailScrollView setFrame:CGRectMake(0, 0, 320, 525)];
           
            [_nutritionTextView setFrame:CGRectMake(0, 0, 320, (IsiOS7)?524:525)];
            [_nutritionTextView setFrame:CGRectMake(0, 0, 320, (IsiOS7)?524:525)];
            
            [_preparationView setFrame:CGRectMake(0, 0, 320, (IsiOS7)?524:525)];
            [_preparationTextView setFrame:CGRectMake(0, 0, 320, (IsiOS7)?524:525)];
            
            
            [_ingredientsView setFrame:CGRectMake(0, 0, 320, 525)];
            [_ingredientsContentView setHeight:_ingredientsTextView.contentSize.height + 50];
            [_ingredientsScrollView setContentSize:CGSizeMake(320, _ingredientsContentView.height)];
            [_allPBHandlerView setFrame:CGRectMake(0, (IsiOS7)?87:44, 320, 525)];
            [_backgroundImageView setFrame:CGRectMake(0, 0, 320, 568)];
            
        }
    }
    
    if(isEmailRecipePurchased){
        
        UIButton *sendRecipeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [sendRecipeButton setBackgroundImage:[UIImage imageNamed:@"mail_button.png"] forState:UIControlStateNormal];
        sendRecipeButton.size = CGSizeMake(32, 32);
        [sendRecipeButton addTarget:self action:@selector(emailReceptButtonTouchDown:) forControlEvents:UIControlEventTouchUpInside];
        NSMutableArray *buttons = [NSMutableArray arrayWithArray:self.navigationItem.rightBarButtonItems];
        [buttons addObject:[[UIBarButtonItem alloc] initWithCustomView:sendRecipeButton]];
        [self.navigationItem setRightBarButtonItems:buttons];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        //
        UIInterfaceOrientation interfaceOrientation = [UIApplication sharedApplication].statusBarOrientation;
        [self updatePositionsToInterfaceOrientation:interfaceOrientation];
        if(UIInterfaceOrientationIsPortrait(interfaceOrientation)){
            _backgroundImageView.image = [UIImage imageNamed:@"Background_Portrait_iPad.png"];
        }else{
            _backgroundImageView.image = [UIImage imageNamed:@"Background_Landscape_iPad.png"];
        }
    }
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updatePositionsToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    if (UIInterfaceOrientationIsPortrait(interfaceOrientation)) {
        
    }
    else {
        
    }
    
    _titleLabel.frame = _firstSeparatorImageView.frame;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willRotateToInterfaceOrientation: toInterfaceOrientation duration: duration];
    
    if(UIInterfaceOrientationIsPortrait(toInterfaceOrientation)){
        _backgroundImageView.image = [UIImage imageNamed:@"Background_Portrait_iPad.png"];
    }else{
        _backgroundImageView.image = [UIImage imageNamed:@"Background_Landscape_iPad.png"];
    }
    
    if (UI_USER_INTERFACE_IDIOM()== UIUserInterfaceIdiomPad) {
        if (self.view) {
            [self updatePositionsToInterfaceOrientation:toInterfaceOrientation];
        }
    }
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)orientation duration:(NSTimeInterval)duration {
    if(IsIpad){
        
    }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [super didRotateFromInterfaceOrientation: fromInterfaceOrientation ];
    if (UI_USER_INTERFACE_IDIOM()== UIUserInterfaceIdiomPad) {
        if (self.view) {
            UIInterfaceOrientation newInterfaceOrientaion = (fromInterfaceOrientation == UIInterfaceOrientationPortrait ||fromInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) ?UIInterfaceOrientationLandscapeRight:UIInterfaceOrientationPortrait;
            [self updatePositionsToInterfaceOrientation:newInterfaceOrientaion];
        }
    }
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    if (UI_USER_INTERFACE_IDIOM()== UIUserInterfaceIdiomPad){
        return ((UIInterfaceOrientationLandscapeLeft == toInterfaceOrientation) || (UIInterfaceOrientationLandscapeRight == toInterfaceOrientation) || (UIInterfaceOrientationPortrait == toInterfaceOrientation));
    }
    return (UIInterfaceOrientationPortrait == toInterfaceOrientation);
}

#pragma mark - Details SegmentControl action
- (IBAction) detailsSegmentValueChanged:(UISegmentedControl *)sender {
    switch (sender.selectedSegmentIndex) {
        case 0:
            [_allPBHandlerView pushView:_detailView animationType:PBHandlerView_AnimationType_Flip subType:PBHandlerView_AnimationSubType_FromLeft];
            break;
        case 1:
            [_allPBHandlerView pushView:_nutritionView animationType:PBHandlerView_AnimationType_Flip subType:PBHandlerView_AnimationSubType_FromLeft];
            break;
        case 2:
            [_allPBHandlerView pushView:_ingredientsView animationType:PBHandlerView_AnimationType_Flip subType:PBHandlerView_AnimationSubType_FromLeft];
            break;
        case 3:
            [_allPBHandlerView pushView:_preparationView animationType:PBHandlerView_AnimationType_Flip subType:PBHandlerView_AnimationSubType_FromLeft];
            break;
    }
}

#pragma mark - Buttons click handlers

- (void) closeButtonClick{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void) addToShoppingListButtonTouch:(id)sender
{
    if(isShoppingCardPurchased)
    {
        NSString *ingredientsString = [_dataDictionary valueForKey:@"Ingredients"];
        NSArray *ingredientsArray = [ingredientsString componentsSeparatedByRegex:@"([\n\r]++[ \f\t\v]*+)++"];
        
        for(NSString *ingredient in ingredientsArray){
            if([ingredient rangeOfString:@"[^ \f\n\r\t\v]"].location == NSNotFound){
                if(![ingredient containsString:@"\n"])
                    [[PBDB instance].db insertDictionary:[NSDictionary dictionaryWithObjectsAndKeys:ingredient,@"Name", nil] forTable:@"Shopping"];
            }
        }
        ShowShortMessage(nil,@"Ingredients added to Shopping List");
    }

    trackAction(kPB_FAEvent_AddToShoppingListButtonClicked);
}

-(void)emailReceptButtonTouchDown:(id)sender
{
    if([MFMailComposeViewController canSendMail]){
        mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        
        NSString *emailBody = [[NSMutableString alloc] initWithContentsOfFile:[NSString bundlePathByAppendingPath:@"email_to_friends.html"] encoding:NSUTF8StringEncoding error:nil];
        NSLog(@"%@", emailBody);
        NSLog(@"%@", _dataDictionary);
        emailBody = [[[[[[[[[[[[[[[[[emailBody stringByReplacingOccurrencesOfString:@"[title]" withString:[_dataDictionary valueForKey:@"DishName"]]
                                    stringByReplacingOccurrencesOfString:@"[preparation time]" withString:[_dataDictionary valueForKey:@"PreparationTimeInMins"]]
                                   stringByReplacingOccurrencesOfString:@"[servings]" withString:[_dataDictionary valueForKey:@"ServingsCount"]]
                                  stringByReplacingOccurrencesOfString:@"[meal type]" withString:[_dataDictionary valueForKey:@"MealType"]]
                                 
                                 stringByReplacingOccurrencesOfString:@"[description]" withString:[[_dataDictionary valueForKey:@"DetailDescription"] stringByReplacingOccurrencesOfRegex:@"([\n\r]++[ \f\t\v]*+)++" withString:@"<br>"]]
                                
                               stringByReplacingOccurrencesOfString:@"[ingredients]" withString:[[_dataDictionary valueForKey:@"Ingredients"] stringByReplacingOccurrencesOfRegex:@"([\n\r]++[ \f\t\v]*+)++" withString:@"<br>"]]
                              stringByReplacingOccurrencesOfString:@"[nutritional]" withString:[[_dataDictionary valueForKey:@"NutritionalInfo"] stringByReplacingOccurrencesOfRegex:@"([\n\r]++[ \f\t\v]*+)++" withString:@"<br>"]]
                              stringByReplacingOccurrencesOfString:@"[preparation]" withString:[[_dataDictionary valueForKey:@"Preparation"] stringByReplacingOccurrencesOfRegex:@"([\n\r]++[ \f\t\v]*+)++" withString:@"<br>"]]
                             stringByReplacingOccurrencesOfString:@"[download app]" withString:@"For additional LowCarb recipes - download the LowCarb diet app at"]
                            stringByReplacingOccurrencesOfString:@"[download link]" withString:[[PBSettings  instance] app:kPBSettings_AppLink]]
                           
                           stringByReplacingOccurrencesOfString:@"[time title]" withString:@"Time"]
                          stringByReplacingOccurrencesOfString:@"[servings title]" withString: @"Servings"]
                         stringByReplacingOccurrencesOfString:@"[meal type title]" withString:@"Meal type"]
                        stringByReplacingOccurrencesOfString:@"[description title]" withString:@"Description"]
                       stringByReplacingOccurrencesOfString:@"[nutritional title]" withString: @"Nutritional"]
                      stringByReplacingOccurrencesOfString:@"[ingredients title]" withString:@"Ingredients"]
                     stringByReplacingOccurrencesOfString:@"[preparation title]" withString:@"Preparation"];
        
        [mc setToRecipients:[NSArray arrayWithObject:@""]];
        [mc setSubject:@"LowCarb diet recipe"];
        [mc setMessageBody:emailBody isHTML:YES];
        [self  presentViewController:mc animated:YES completion:nil];
        trackAction(kPB_FAEvent_SendEmailRecipeButtonClicked);
    }
    else{
        
        UIAlertView *alert;
        alert = [[UIAlertView alloc]
                 initWithTitle:@"Error"
                 message:@"No mail accounts found"
                 delegate: self
                 cancelButtonTitle:@"Ok"
                 otherButtonTitles: nil];
        [alert show];
    }
    
}

#pragma mark - MailComposeController delegate
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    
    //if result is possible
	if(result == MFMailComposeResultSent || result == MFMailComposeResultSaved || result == MFMailComposeResultCancelled)
    {
        UIAlertView *alert;
        
        switch (result) {
            case MFMailComposeResultCancelled:
                /*alert = [[UIAlertView alloc]
                 initWithTitle:@"Information"
                 message:@"Operation was cancelled"
                 delegate: self
                 cancelButtonTitle:@"Ok"
                 otherButtonTitles: nil];*/
                break;
            case MFMailComposeResultSaved:
                alert = [[UIAlertView alloc]
                         initWithTitle:@"Information"
                         message:@"Mail was saved"
                         delegate: self
                         cancelButtonTitle:@"Ok"
                         otherButtonTitles: nil];
                [alert show];
                break;
                //message was sent
            case MFMailComposeResultSent:
                alert = [[UIAlertView alloc]
                         initWithTitle:@"Information"
                         message:@"Mail was sent"
                         delegate: self
                         cancelButtonTitle:@"Ok"
                         otherButtonTitles: nil];
                [alert show];
                break;
            case MFMailComposeResultFailed:
                alert = [[UIAlertView alloc]
                         initWithTitle:@"Information"
                         message:@"Operation was failed!"
                         delegate: self
                         cancelButtonTitle:@"Ok"
                         otherButtonTitles: nil];
                [alert show];
                break;
            default:
                break;
        }
    }
    
    //else exists error
    else if(error != nil){
        //show error
        //[self makeAlert:[error localizedDescription]];
	}
    
    //dismiss view
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark TapGestureRecognizer handler

-(void) recipePictureTap{
    
    UIImage *image = [UIImage imageNamed:[_dataDictionary valueForKey:@"Image"]];
    if (!image) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
        NSLog(@"PATH: %@",[documentsPath stringByAppendingPathComponent:[_dataDictionary valueForKey:@"Image"]]);
        image = [UIImage imageWithContentsOfFile:[documentsPath stringByAppendingPathComponent:[_dataDictionary valueForKey:@"Image"]]];
        [[PBImagePresenter instance] presentImages:[NSArray arrayWithObject:image] activeImageNumber:0 fromView:_dishImageView];
    }else{
        [[PBImagePresenter instance] presentImages:[NSArray arrayWithObject:image] activeImageNumber:0 fromView:_dishImageView];
    }
}

@end
