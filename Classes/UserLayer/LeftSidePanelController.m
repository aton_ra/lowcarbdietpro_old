//
//  PosesPaperFoldNavigationController.m
//  Yoga
//
//  Created by polar15 on 27.03.13.
//  Copyright (c) 2013 Polar-B. All rights reserved.
//

#import "LeftSidePanelController.h"
#import "LeftSlideMenuViewController.h"

@interface LeftSidePanelController ()
@end

@implementation LeftSidePanelController

-(void) awakeFromNib
{
    self.leftFixedWidth = 255;
    
    UINavigationController *recipeNavigationViewController = (UINavigationController*)[self.storyboard instantiateViewControllerWithIdentifier:@"recipeNavigationViewController"];
    self.posesViewController = (RecipesViewController*)[[recipeNavigationViewController viewControllers] objectAtIndex:0];
    
    [self setCenterPanel:recipeNavigationViewController];
    
    LeftSlideMenuViewController * rightPanel = [self.storyboard instantiateViewControllerWithIdentifier:@"PosesSlideMenuViewController"];
    rightPanel.delegate = self.posesViewController;
    [self setRightPanel:rightPanel];
    
    //[self showRightPanelAnimated:YES];
    
}

@end
