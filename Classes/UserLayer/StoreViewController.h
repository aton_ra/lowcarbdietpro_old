//
//  StoreViewController.h
//  DukanDiet
//
//  Created by polar12 on 6/20/13.
//  Copyright (c) 2013 POLAR-B. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MKStoreManager.h"
#import "PBCarouselHandler.h"

@class StoreViewController;

@protocol StoreViewControllerDelegate <NSObject>

- (void)featureWasPurchasedWithIndex:(NSInteger *)index isRestored:(BOOL)isRestored;
- (void)storeViewControllerDidClosed;
@end

@interface StoreViewController : UIViewController<iCarouselDataSource, iCarouselDelegate, PBCarouselHandlerControlDelegate>{
    
    NSArray *_purchaseDescriptionDataSource;
}

@property (nonatomic, assign) id <StoreViewControllerDelegate> delegate;
@property (nonatomic, retain) IBOutlet iCarousel *_carousel;
@property (nonatomic, retain) IBOutlet UIImageView *backImageView;
@property (nonatomic, retain) IBOutlet UIButton *buttonPurchase;
@property (nonatomic, retain) IBOutlet UIImageView *backItemImageView;
@property (nonatomic, retain) NSDictionary * dukanDietRulesDictionary;
@property (retain, nonatomic) IBOutlet UIButton *closeButton;
@property (nonatomic, retain) UIViewController *parent;
@property (retain, nonatomic) IBOutlet UIButton *buyButton;
@property (retain, nonatomic) IBOutlet UILabel *storeTitleLabel;

@property (retain, nonatomic) IBOutlet UIButton *restoreButton;
- (IBAction)restoreButtonClick:(id)sender;

- (IBAction)buyButtonClick:(id)sender;

- (IBAction)closeButtonClick:(id)sender;

@property (retain, nonatomic) IBOutlet PBCarouselHandler *carouselPaginator;
@property (retain, nonatomic) IBOutlet UITextView *purchaseDescriptionTextView;

@end
