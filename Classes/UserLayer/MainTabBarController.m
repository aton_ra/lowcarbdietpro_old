//
//  MainTabBarController.m
//  DukanDiet
//
//  Created by polar21 on 8/8/13.
//  Copyright (c) 2013 Polar-B. All rights reserved.
//

#import "MainTabBarController.h"
#import "PBMainMenuSegmentControl.h"
#import "PBSegmentControl.h"
#import "StoreViewController.h"
#import "ShoppingViewController.h"
#import "PBParsecomManager.h"
#import "Appirater.h"

StoreViewController *storeVC;
ShoppingViewController *shoppingVC;

@interface MainTabBarController (){
    NSInteger ativeSegmentTag;
    
    UISlider *filterSlider;
    UISwitch *filterSwitch;
    
    int flag;
}

- (IBAction)selectedTabChanged:(PBMainMenuSegmentControl *)sender;
- (IBAction)allRecipesButtonClicked;
- (IBAction)segmentControlSelectedChanged:(PBSegmentControl *)sender;


@property (strong, nonatomic) IBOutlet PBSegmentControl *switchSegmentControl;
@property (strong, nonatomic) IBOutlet PBMainMenuSegmentControl *tabsSegmetControl;
@end


@implementation MainTabBarController
@synthesize _searchTextField;
@synthesize tabBarControl;


#pragma mark - Life cicle

-(BOOL)prefersStatusBarHidden{
    return YES;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    flag = 1;
  
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didGetNotification)
                                                 name:@"ShoppingCardWasPurchased"
                                               object:nil];
    
    if(!isShoppingCardPurchased)
        [((UIButton*)[tabBarControl viewWithTag:4]) setHidden:YES];
    
    [self setOffsetOfPBMainControlButtons];
    
    isFavorite = NO;
    menuDatasourceArray = @[
                                 
                                 @{@"sectionTitle":@"Favorite"
                                   
                                   },
                                 @{@"sectionTitle":@"Preparation Time"
                                   
                                   },
                                 
                                 @{@"sectionTitle":@"Meal Type",
                                   @"rows":@[
                                           [@{@"Title":@"All",@"Value":[NSNumber numberWithInt:0],@"isSelected":[NSNumber numberWithBool:YES]} mutableCopy],
                                           [@{@"Title":@"Breakfast",@"Value":[NSNumber numberWithInt:1],@"isSelected":[NSNumber numberWithBool:NO]} mutableCopy],
                                           [@{@"Title":@"Dinner",@"Value":[NSNumber numberWithInt:2],@"isSelected":[NSNumber numberWithBool:NO]} mutableCopy],
                                           [@{@"Title":@"Lunch",@"Value":[NSNumber numberWithInt:3],@"isSelected":[NSNumber numberWithBool:NO]} mutableCopy],
                                           [@{@"Title":@"Snack",@"Value":[NSNumber numberWithInt:4],@"isSelected":[NSNumber numberWithBool:NO]} mutableCopy]
                                           ]
                                   }
                                 ];
    
    ativeSegmentTag = 1;
    // create contents vc
    [self performSegueWithIdentifier:@"recipeList" sender:self];
    [self performSegueWithIdentifier:@"about" sender:self];
    [self performSegueWithIdentifier:@"more" sender:self];
    if(isIphone){
        [self performSegueWithIdentifier:@"shopping" sender:self];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if(flag){
        [self updateDataBase];
        flag = 0;
    }
}

-(void)viewWillAppear:(BOOL)animated{
    
    self.tabsSegmetControl.activeSegmentTag = ativeSegmentTag;
    [self selectedTabChanged:self.tabsSegmetControl];
    // set presentation type to grid
    self.switchSegmentControl.activeSegmentTag = 2;
    if(IsIpad){
        [(RecipesViewController*)self.recipeListViewController setPresentationTypeByIndex:2];
        ((RecipesViewController*)self.recipeListViewController).delegate = self;
        [self refreshData];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    if(self.aboutViewController.view.isHidden){
        [self.aboutViewController didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    }
    
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    if(self.aboutViewController.view.isHidden){
         [self.aboutViewController willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    }
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    if (UI_USER_INTERFACE_IDIOM()== UIUserInterfaceIdiomPad){
        return ((UIInterfaceOrientationLandscapeLeft == toInterfaceOrientation) || (UIInterfaceOrientationLandscapeRight == toInterfaceOrientation) || (UIInterfaceOrientationPortrait == toInterfaceOrientation));
    }
    return (UIInterfaceOrientationPortrait == toInterfaceOrientation);
}

#pragma mark - Segment control actions

- (IBAction)selectedTabChanged:(PBMainMenuSegmentControl *)sender {
    
    if(sender.activeSegmentTag != -1){
        trackAction(kPB_FAEvent_TabBarButtonClicked);
    }
    
    switch (sender.activeSegmentTag) {
        case 1:{
            self.recipeListViewController.view.hidden = NO;
            self.aboutViewController.view.hidden = YES;
            self.moreViewController.view.hidden = YES;
            self.rulesViewController.view.hidden = YES;
            self.shoppingViewController.view.hidden = YES;
            self.dietViewController.view.hidden = YES;
            ativeSegmentTag = 1;
            [self.navigationItem setTitle:@"LowCarb Diet Recipes"];
            break;
        }
            
        case 2:{
            self.recipeListViewController.view.hidden = YES;
            self.aboutViewController.view.hidden = NO;
            self.moreViewController.view.hidden = YES;
            self.rulesViewController.view.hidden = YES;
            self.shoppingViewController.view.hidden = YES;
            self.dietViewController.view.hidden = YES;
            ativeSegmentTag = 2;
            [self.navigationItem setTitle:@"About"];
            break;
        }
            
        case 3:{
            self.recipeListViewController.view.hidden = YES;
            self.aboutViewController.view.hidden = YES;
            self.moreViewController.view.hidden = NO;
            self.rulesViewController.view.hidden = YES;
            self.shoppingViewController.view.hidden = YES;
            self.dietViewController.view.hidden = YES;
            [self.navigationItem setTitle:@"More"];
            ativeSegmentTag = 3;
            break;
        }
        case 4:{
            if(!IsIpad){
                self.shoppingViewController.view.hidden = NO;
                self.recipeListViewController.view.hidden = YES;
                self.aboutViewController.view.hidden = YES;
                self.moreViewController.view.hidden = YES;
                self.rulesViewController.view.hidden = YES;
                self.dietViewController.view.hidden = YES;
                [self.navigationItem setTitle:@"Shopping"];
                ativeSegmentTag = 4;
            }
            break;
        }
        default:
            break;
    }
}

- (IBAction)allRecipesButtonClicked {
    self.tabsSegmetControl.activeSegmentTag = 1;
    [self selectedTabChanged:self.tabsSegmetControl];
    
    [self resetFilter];
    [_filterTableView reloadData];
    [self refreshData];
}

-(void)resetFilter{
    
    // deselect meal type section
    NSDictionary *selectedSectionDict = menuDatasourceArray[2];
    selectedSectionDict[@"rows"][0][@"isSelected"] = [NSNumber numberWithInt:1];
    for(int i = 1; i<((NSArray*)selectedSectionDict[@"rows"]).count; i++){
       selectedSectionDict[@"rows"][i][@"isSelected"] = [NSNumber numberWithInt:0];
    }
    
    [_searchTextField setText:@""];
    filterSlider.value = 0;
    [filterSwitch setOn:NO];
    isFavorite = NO;
    _duration = 0;
}

- (IBAction)segmentControlSelectedChanged:(PBSegmentControl *)sender {
    if(IsIpad){
        [self refreshData];
        [(RecipesViewController*)self.recipeListViewController setPresentationTypeByIndex:sender.activeSegmentTag];
    }
}

#pragma mark-
#pragma mark  TableView dataSource/Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return menuDatasourceArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 25;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UILabel* titleLabel = [[UILabel alloc] init];
    titleLabel.width = _filterTableView.size.width;
    titleLabel.height = 25;
    titleLabel.font = [UIFont boldSystemFontOfSize:15];
    titleLabel.textAlignment = UITextAlignmentCenter;
    titleLabel.textColor = [UIColor darkGrayColor];
    titleLabel.backgroundColor = [UIColor clearColor];
    if(section != 0){
        titleLabel.text = menuDatasourceArray[section][@"sectionTitle"];
    }else{
        titleLabel.text = @"";
    }
    
    return titleLabel;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 0 || section == 1)
        return 1;
    return ((NSArray*)menuDatasourceArray[section][@"rows"]).count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = nil;
    if(indexPath.section == 0){
        cell = [tableView dequeueReusableCellWithIdentifier:@"switch_cell_id"];
        UILabel* label = (UILabel*)[cell.contentView viewWithTag:1];
        label.text = @"Favorite";
        UISwitch *swith = (UISwitch*)[cell.contentView viewWithTag:2];
        [swith addTarget:self action:@selector(starredValueChanged:) forControlEvents:UIControlEventValueChanged];
        swith.selected = isFavorite;
        filterSwitch = swith;
        
    }else if(indexPath.section == 1){
        
        cell = [tableView dequeueReusableCellWithIdentifier:@"slider_cell_id"];
        UILabel* label = (UILabel*)[cell.contentView viewWithTag:1];
        label.text = @"All";
        
        NSInteger maxPreparationTime = 30;
        
        UISlider *durationSlider = (UISlider*)[cell.contentView viewWithTag:2];
        durationSlider.maximumValue = maxPreparationTime;
        durationSlider.minimumValue = 0;
        [durationSlider addTarget:self action:@selector(durationValueChanged:) forControlEvents:UIControlEventValueChanged];
        durationSlider.selected = isFavorite;
        filterSlider = durationSlider;
        
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:@"button_cell_id"];
        UILabel* label = (UILabel*)[cell.contentView viewWithTag:1];
        label.text = menuDatasourceArray[indexPath.section][@"rows"][indexPath.row][@"Title"];
        if([(NSNumber*)menuDatasourceArray[indexPath.section][@"rows"][indexPath.row][@"isSelected"] intValue] == 1){
            [_filterTableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
        }else{
            [_filterTableView deselectRowAtIndexPath:indexPath animated:NO];
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *selectedSectionDict = menuDatasourceArray[indexPath.section];
    selectedSectionDict[@"rows"][0][@"isSelected"] = [NSNumber numberWithInt:0];
    for(int i = 0; i<((NSArray*)selectedSectionDict[@"rows"]).count; i++){
        if(i == indexPath.row){
            if(i == indexPath.row){
                selectedSectionDict[@"rows"][i][@"isSelected"] = [NSNumber numberWithInt:0];
            }
        }
    }
    
    if(indexPath.row == 0){
        selectedSectionDict[@"rows"][0][@"isSelected"] = [NSNumber numberWithInt:1];
        for(int i=1; i<((NSArray*)selectedSectionDict[@"rows"]).count; i++){
            selectedSectionDict[@"rows"][i][@"isSelected"] = [NSNumber numberWithInt:0];
        }
    }
    
    int val = 0;
    // if all row in section are deselected - select 0 row
    for(int i=0; i<((NSArray*)selectedSectionDict[@"rows"]).count; i++){
        if( [selectedSectionDict[@"rows"][i][@"isSelected"] intValue] == 1){
            val++;
        }
    }
    if(val == 0){
        selectedSectionDict[@"rows"][0][@"isSelected"] = [NSNumber numberWithInt:1];
    }
    
    [_filterTableView reloadData];
    [self refreshData];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *selectedSectionDict = menuDatasourceArray[indexPath.section];
    
    selectedSectionDict[@"rows"][0][@"isSelected"] = [NSNumber numberWithInt:0];
    
    for(int i = 0; i<((NSArray*)selectedSectionDict[@"rows"]).count; i++){
        if(i == indexPath.row){
            if(i == indexPath.row){
                selectedSectionDict[@"rows"][i][@"isSelected"] = [NSNumber numberWithInt:1];
            }
        }
    }
    
    if(indexPath.row == 0){
        selectedSectionDict[@"rows"][0][@"isSelected"] = [NSNumber numberWithInt:1];
        for(int i=1; i<((NSArray*)selectedSectionDict[@"rows"]).count; i++){
            selectedSectionDict[@"rows"][i][@"isSelected"] = [NSNumber numberWithInt:0];
        }
    }
    
    int val = 0;
    // if all row in section are deselected - select 0 row
    for(int i=0; i<((NSArray*)selectedSectionDict[@"rows"]).count; i++){
        if( [selectedSectionDict[@"rows"][i][@"isSelected"] intValue] == 1){
            val++;
        }
    }
    
    if(val == 0){
        selectedSectionDict[@"rows"][0][@"isSelected"] = [NSNumber numberWithInt:1];
    }
    
    [_filterTableView reloadData];
    [self refreshData];
}

#pragma mark filter table view elements

- (void)starredValueChanged:(UISwitch *)sender{
    
    if(sender.isOn){
        isFavorite = YES;
        trackAction(kPB_FAEvent_FavoriteFilterActivated);
    }else{
        isFavorite = NO;
        trackAction(kPB_FAEvent_FavoriteFilterDeActivated);
    }
    [_filterTableView reloadData];

    [self refreshData];

}

- (void)durationValueChanged:(UISlider *)sender{
    if(_duration != (int)sender.value){
        _duration = (int)sender.value;

        _duration = sender.value;
        UILabel *sliderLabel = (UILabel *)[sender.superview viewWithTag:1];
        if(_duration == 0){
            sliderLabel.text = @"All";
        }else{
            sliderLabel.text = [NSString stringWithFormat:@"%d mins",_duration];
        }
        [self refreshData];
    }
}

#pragma mark Refresh table method

-(void) updateDataBase{
    //[self showLoading];
    if([Reachability reachabilityForInternetConnection].currentReachabilityStatus != NotReachable){
        [[PBParsecomManager sharedManager] updateDataFromParse:@"Recipes"
         
                                                      onComplete:^(BOOL success, NSString *message){
                                                          
                                                          if(success){
                                                              //[self hideLoading];
                                                              //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                                              //[alert show];
                                                              [Appirater showRatingPopup];
                                                              if(IsIpad)
                                                                  [self refreshData];
                                                          }
                                                          else{
                                                              //[self hideLoading];
                                                              //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error update data base" message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                                              //[alert show];
                                                              NSLog(@"Error: %@", message);
                                                          }
                                                      }];
    }else{
        //[self hideLoading];
        
        //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Checking for updates failed" message:@"No internet connection." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        //[alert show];
    }
    
}

- (void)refreshData {
    
	NSMutableString *sqlString = [NSMutableString stringWithString:@"SELECT * FROM Recipes"];
    
    for(int i = 2; i < [menuDatasourceArray count]; ++i){
        NSDictionary *selectedSectionDict = menuDatasourceArray[i];
        if(i == 2){
            if([selectedSectionDict[@"rows"][0][@"isSelected"] intValue] == 0){
                if([sqlString containsString:@" WHERE("])
                    [sqlString appendString:@" AND("];
                else
                    [sqlString appendString:@" WHERE("];
                
                if([selectedSectionDict[@"rows"][1][@"isSelected"]intValue] == 1){
                    if([sqlString containsString:@" MealType LIKE"])
                        [sqlString appendString:@" OR"];
                    [sqlString appendString:@" MealType LIKE '%breakfast%'"];
                }
                
                if([selectedSectionDict[@"rows"][2][@"isSelected"]intValue] == 1){
                    if([sqlString containsString:@" MealType LIKE"])
                        [sqlString appendString:@" OR"];
                    [sqlString appendString:@" MealType LIKE '%dinner%'"];
                }
                
                if([selectedSectionDict[@"rows"][3][@"isSelected"]intValue] == 1){
                    if([sqlString containsString:@" MealType LIKE"])
                        [sqlString appendString:@" OR"];
                    [sqlString appendString:@" MealType LIKE '%lunch%'"];
                }
                
                if([selectedSectionDict[@"rows"][4][@"isSelected"]intValue] == 1){
                    if([sqlString containsString:@" MealType LIKE"])
                        [sqlString appendString:@" OR"];
                    [sqlString appendString:@" MealType LIKE '%Snack%'"];
                }
                
                [sqlString appendString:@" )"];
            }
        }   
    }
    
    if (isFavorite){
        if([sqlString containsString:@" WHERE"])
            [sqlString appendString:@" AND"];
        else
            [sqlString appendString:@" WHERE"];
        
        [sqlString appendString:@" IsFavorite IS 'TRUE'"];
    }
    
    if(_duration != 0){
        if([sqlString containsString:@" WHERE"])
            [sqlString appendString:@" AND"];
        else
            [sqlString appendString:@" WHERE"];
        
        [sqlString appendFormat:@" CAST(PreparationTimeInMins AS INTEGER)<=%d",_duration];
    }
    
    NSString *searchString = _searchTextField.text;
    
    if (searchString.length > 0) {
        if([sqlString containsString:@" WHERE"])
            [sqlString appendString:@" AND"];
		else
            [sqlString appendString:@" WHERE"];
        [sqlString appendString:[NSString stringWithFormat:@" (DishName LIKE '%%%@%%' OR", searchString]];
        [sqlString appendString:[NSString stringWithFormat:@" Ingredients LIKE '%%%@%%')", searchString]];
    }

    if(_duration != 0){
        if(!searchString)
            searchString = @"";
        
        [sqlString appendString:[NSString stringWithFormat:@" ORDER BY CAST(PreparationTimeInMins AS INTEGER) ASC"]];
    }
    
    RELEASE_SAFELY(_dataSourceArray);
    _dataSourceArray = [[NSMutableArray alloc]initWithArray:[[PBDB instance].db lookupAllForSQL:sqlString]];
    
    [(RecipesViewController*)self.recipeListViewController refreshRecipesViewWithDataSource:_dataSourceArray];
}

#pragma mark - button actions

-(IBAction)storeButtonClick{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:IsIpad?@"Storyboard_iPad":@"Storyboard_iPhone" bundle:nil];
    StoreViewController *storeVC = (StoreViewController*) [mainStoryboard instantiateViewControllerWithIdentifier:@"StoreViewController"];
    //UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:termsVC];
    [self presentViewController:storeVC animated:YES completion:nil];
}

-(IBAction)dietButtonClick{
    if(IsIpad){
        [self presentStoreViewControllerAsModal];
    }
}

#pragma mark - RecipesViewController delegate
- (void)starButtonWasClicked{
    [self refreshData];
}

#pragma mark - Present store VC
- (void)presentStoreViewControllerAsModal{
    
    if(!storeVC){
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Storyboard_iPad" bundle:nil];
        storeVC = (StoreViewController*)[mainStoryboard instantiateViewControllerWithIdentifier:@"StoreViewController"];
    }
    
    storeVC.delegate = self;
    storeVC.modalPresentationStyle = UIModalPresentationFormSheet;
    if(IsiOS7)
        storeVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:storeVC animated:YES completion:nil];
    
    float x, y;
    
    CGRect screenBounds = [UIScreen mainScreen].bounds ;
    CGFloat width = CGRectGetWidth(screenBounds);
    CGFloat height = CGRectGetHeight(screenBounds);
    
    if(self.interfaceOrientation == UIInterfaceOrientationPortrait || self.interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown){
        
        [storeVC.view.superview setHeight:500];
        [storeVC.view.superview setWidth:670];
        y  = (width - storeVC.view.superview.size.width) / 2;
        x  = (height - storeVC.view.superview.size.height) / 2;
        
        //[storeVC.view setOrigin:CGPointMake(0, 0)];
        [storeVC.view.superview setOrigin:CGPointMake(y, x)];
    }
    else{
        [storeVC.view.superview setWidth:670];
        [storeVC.view.superview setHeight:500];
        // in landscape height is width = 1024 and width is height = 768
        x  = (height - storeVC.view.superview.size.width) / 2;
        y  = (width - storeVC.view.superview.size.height) / 2;
        
        //[storeVC.view setOrigin:CGPointMake(0, 0)];
        [storeVC.view.superview setOrigin:CGPointMake(x, y)];
    }
    
    [storeVC.view.superview setBackgroundColor:[UIColor clearColor]];
    [storeVC.view.superview.layer setCornerRadius:14.5];
    [storeVC.view.superview.layer setMasksToBounds:YES];
    
    
}

#pragma mark - StoreViewController delegate
//iPhone
- (void)didGetNotification {
    if(isShoppingCardPurchased){
        [((UIButton*)[tabBarControl viewWithTag:4]) setHidden:NO];
        [self setOffsetOfPBMainControlButtons];
    }
}
 //iPad
- (void)featureWasPurchasedWithIndex:(NSInteger *)index isRestored:(BOOL)isRestored{
    
    if(isShoppingCardPurchased){
        [((UIButton*)[tabBarControl viewWithTag:4]) setHidden:NO];
        [self setOffsetOfPBMainControlButtons];
    }
}

- (void)storeViewControllerDidClosed{
    self.tabsSegmetControl.activeSegmentTag = ativeSegmentTag;
}

#pragma mark - popover delegate

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    self.tabsSegmetControl.activeSegmentTag = ativeSegmentTag;
}

#pragma mark - segues delegates

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"shoppingSegue"]) {
        UIPopoverController *dest = ((UIStoryboardPopoverSegue*)segue).popoverController;
        dest.delegate = self;
    }
}

#pragma mark - Search textField delegates

- (IBAction)searchTextFieldChanged:(id)sender {
    [self refreshData];
}

#pragma mark - refresh PBMainMenuControl buttons

-(void)setOffsetOfPBMainControlButtons{
    
    int cnt = 0;
    if(IsIpad){
        
            [[tabBarControl viewWithTag:1] setOrigin:CGPointMake(0, 0)];
            [[tabBarControl viewWithTag:2] setOrigin:CGPointMake(65 + 1, 0)];
            [[tabBarControl viewWithTag:3] setOrigin:CGPointMake(65*2 + 1, 0)];
            [[tabBarControl viewWithTag:4] setOrigin:CGPointMake(65*3 + 2, 0)];
            NSLog(@"VErsion: %f", [[[UIDevice currentDevice] systemVersion]doubleValue]);
            if(!(IsiOS7)){
                [(UIButton*)[tabBarControl viewWithTag:3] setImageEdgeInsets:UIEdgeInsetsMake(0.0, 0.0, 0.0, 1.0)];
            }
    }else{
        for (UIButton *btn in tabBarControl.segmentControlButtons) {
            if(btn.isHidden == NO){
                
                if(isShoppingCardPurchased){
                    [btn setWidth:79.0];
                    if(btn.tag == 3){
                        [btn setImageEdgeInsets:UIEdgeInsetsMake(-2.0, 9.0, 0.0, 0.0)];
                        [btn setTitleEdgeInsets:UIEdgeInsetsMake(btn.titleEdgeInsets.top, btn.titleEdgeInsets.left + 2, btn.titleEdgeInsets.bottom, btn.titleEdgeInsets.right)];
                    }
                    
                    [btn setOrigin:CGPointMake(cnt * 80.0 + cnt * 1, 0)];
                }
                else{
                    [btn setWidth:106];
                    if(btn.tag == 3){
                        [btn setImageEdgeInsets:UIEdgeInsetsMake(-2.0, (IsiOS7)?23.0:23.0, 0.0, (IsiOS7)?0.0:3.0)]; // top left bottom right
                    }
                    [btn setOrigin:CGPointMake(cnt * 106 + cnt * 1, 0)];
                }
                ++cnt;
            }
        }
    }
}
@end

@implementation TabBarSegue

-(void)perform{
    MainTabBarController *sourceViewController = (MainTabBarController*)[self sourceViewController];
    UIVideoEditorController *destinationController = [self destinationViewController];
    [sourceViewController addChildViewController:destinationController];
    destinationController.view.size = sourceViewController.contentView.size;
    destinationController.view.top = 0;
    destinationController.view.left = 0;
    destinationController.view.hidden = YES;
    destinationController.view.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    [sourceViewController.contentView addSubview:destinationController.view];
    //
    // relations
    //
    [sourceViewController.navigationController.navigationBar setHeight:44.0];
    [sourceViewController.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"bg_nav_bar_cut.png"] forBarMetrics:UIBarMetricsDefault];
    
    if([self.identifier isEqualToString:@"recipeList"]){
        sourceViewController.recipeListViewController = destinationController;
    }
    else if([self.identifier isEqualToString:@"about"]){
        sourceViewController.aboutViewController = destinationController;
    }
    else if([self.identifier isEqualToString:@"more"]){
        sourceViewController.moreViewController = destinationController;
    }
    else if([self.identifier isEqualToString:@"rules"]){
        sourceViewController.rulesViewController = destinationController;
    }
    else if([self.identifier isEqualToString:@"shopping"] && isIphone){
        sourceViewController.shoppingViewController = destinationController;
    }
    else if([self.identifier isEqualToString:@"diet"] && isIphone){
        sourceViewController.dietViewController = destinationController;
    }
}

@end