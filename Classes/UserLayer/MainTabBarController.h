//
//  MainTabBarController.h
//  DukanDiet
//
//  Created by polar21 on 8/8/13.
//  Copyright (c) 2013 Polar-B. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilterPanelView.h"
#import "RecipesViewController.h"
#import "StoreViewController.h"
#import "PBMainMenuSegmentControl.h"


@interface MainTabBarController : UIViewController <RecipesViewControllerDelegate, StoreViewControllerDelegate, UIPopoverControllerDelegate>{
    FilterRecipeType _currentUserType;
    FilterMealType _currentMealType;
    //FilterPhase _currentPhase;
    //FilterCategoryType _currentCategoryType;
    int _currentDuration;
    NSMutableArray *_dataSourceArray;
    
     NSArray *menuDatasourceArray;
     BOOL isFavorite;
     int _duration;
}

@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UITableView *filterTableView;
@property (strong, nonatomic) IBOutlet UITextField *_searchTextField;
@property (strong, nonatomic) IBOutlet UIButton *mealPlanButton;
@property (strong, nonatomic) IBOutlet UIButton *mealButtonIphone;

@property (strong, nonatomic) IBOutlet PBMainMenuSegmentControl *tabBarControl;



//
// Tabs view controllers
//
@property (strong, nonatomic) UIViewController *recipeListViewController;
@property (strong, nonatomic) UIViewController *aboutViewController;
@property (strong, nonatomic) UIViewController *moreViewController;
@property (strong, nonatomic) UIViewController *rulesViewController;
@property (strong, nonatomic) UIViewController *shoppingViewController;
@property (strong, nonatomic) UIViewController *dietViewController;

- (void)refreshData;
- (IBAction)searchTextFieldChanged:(id)sender;
- (IBAction)dietButtonClick;
- (IBAction)storeButtonClick;
@end


@interface TabBarSegue : UIStoryboardSegue

@end