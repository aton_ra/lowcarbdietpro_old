//
//  StorePopUp.m
//  DukanDiet
//
//  Created by polar12 on 6/20/13.
//  Copyright (c) 2013 POLAR-B. All rights reserved.
//

#import "StoreViewController.h"


@interface StoreViewController() {
    NSArray *arrayWithPurchases;
    NSInteger selectedFeature;
}

@end

@implementation StoreViewController

@synthesize _carousel;
@synthesize backImageView;
@synthesize backItemImageView;
@synthesize dukanDietRulesDictionary;
@synthesize closeButton;
@synthesize parent;
@synthesize purchaseDescriptionTextView;

#pragma mark Life cicle methods

-(BOOL)prefersStatusBarHidden{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    arrayWithPurchases = [[NSArray alloc] initWithObjects:kPB_PurchaseProductIdEmailRecipe, kPB_PurchaseProductIdShoppingCart,  nil];
    
    NSString *fname = [[NSBundle mainBundle] pathForResource:@"Store" ofType:@"strings"];
    dukanDietRulesDictionary = [NSDictionary dictionaryWithContentsOfFile:fname];
    
    _purchaseDescriptionDataSource = [NSArray arrayWithObjects:[dukanDietRulesDictionary valueForKey:@"Email IAP"],[dukanDietRulesDictionary valueForKey:@"Shopping Cart"], nil] ;

    [purchaseDescriptionTextView setEditable:NO];
    
    //configure carousel
    _carousel.type = iCarouselTypeCoverFlow2;
    selectedFeature = 0;
    
    if(isIphone5){
        [_carousel setOrigin:CGPointMake(_carousel.origin.x, _carousel.origin.y + 40)];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self.view setBackgroundColor:[UIColor clearColor]];
    [self.view.superview setBackgroundColor:[UIColor clearColor]];
    _carouselPaginator.delegate = self;
    
    [purchaseDescriptionTextView setText:[_purchaseDescriptionDataSource objectAtIndex:0]];
    
    if([self isFeaturePurchasedWithIndex:0]){
        [_buyButton setTitle:@"Purchased" forState:UIControlStateNormal];
        [_buyButton setEnabled:NO];
    }else{
        [_buyButton setTitle:@"Buy $0.99" forState:UIControlStateNormal];
        [_buyButton setEnabled:YES];
    }
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    if (UI_USER_INTERFACE_IDIOM()== UIUserInterfaceIdiomPad){
        return ((UIInterfaceOrientationLandscapeLeft == toInterfaceOrientation) || (UIInterfaceOrientationLandscapeRight == toInterfaceOrientation) || (UIInterfaceOrientationPortrait == toInterfaceOrientation));
    }
    return (UIInterfaceOrientationPortrait == toInterfaceOrientation);
}

#pragma mark CarouselControl delegate

- (void)selectItemAtIndex:(NSUInteger)index{
    
    if(selectedFeature != index){
        [_carousel scrollToItemAtIndex:index animated:YES];
        selectedFeature = index;
    }
}

#pragma mark -
#pragma mark iCarousel methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel{
    return 2;
}

- (void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel{
    
    [_carouselPaginator selectItemAtIndex:carousel.currentItemIndex];
    selectedFeature = carousel.currentItemIndex;

    [purchaseDescriptionTextView setText:[_purchaseDescriptionDataSource objectAtIndex:selectedFeature]];
   
    if([self isFeaturePurchasedWithIndex:selectedFeature]){
        [((UIButton*)[[carousel currentItemView] viewWithTag:2]) setImage:[UIImage imageNamed:@"check_store.png"] forState:UIControlStateNormal];
        [((UIButton*)[[carousel currentItemView] viewWithTag:2]) setUserInteractionEnabled:NO];
        [((UIImageView*)[[carousel currentItemView] viewWithTag:3]) setUserInteractionEnabled:NO];
        
        [_buyButton setTitle:@"Purchased" forState:UIControlStateNormal];
        [_buyButton setEnabled:NO];
    }else{
      
        [((UIButton*)[[carousel currentItemView] viewWithTag:2]) setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [((UIButton*)[[carousel currentItemView] viewWithTag:2]) setUserInteractionEnabled:YES];
        [_buyButton setTitle:@"Buy $0.99" forState:UIControlStateNormal];
        [_buyButton setEnabled:YES];
        
    }
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view{
    UITapGestureRecognizer *tap;
    if (!view){
        view = [[[NSBundle mainBundle] loadNibNamed:isIpad? @"carouselStoreItem_iPad": @"carouselItem" owner:self options:nil] lastObject];
    }
    
    switch (index) {
        case 0:
            ((UIImageView*)[view viewWithTag:3]).image = [UIImage imageNamed:@"cover_3.png"];
            break;
        case 1:
            ((UIImageView*)[view viewWithTag:3]).image = [UIImage imageNamed:@"cover_4.png"];
            break;
        default:
            break;
    }
    
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(buyFeatureTap)];
    [((UIImageView*)[view viewWithTag:3]) addGestureRecognizer:tap];
    [view addGestureRecognizer:tap];
    
    if([self isFeaturePurchasedWithIndex:index]){
        [((UIButton*)[view viewWithTag:2]) setImage:[UIImage imageNamed:@"check_store.png"] forState:UIControlStateNormal];
        [((UIButton*)[view viewWithTag:2]) setUserInteractionEnabled:NO];
        [((UIImageView*)[view viewWithTag:3]) setUserInteractionEnabled:NO];
        
    }else{
        [((UIButton*)[view viewWithTag:2]) setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [((UIButton*)[view viewWithTag:2]) setUserInteractionEnabled:YES];
        [((UIImageView*)[view viewWithTag:3]) setUserInteractionEnabled:YES];
    }
    
    return view;
}

#pragma mark -
#pragma mark Control events

-(IBAction)restoreButtonClick:(id)sender{
    if([Reachability reachabilityForInternetConnection].currentReachabilityStatus != NotReachable){
        if([SKPaymentQueue canMakePayments]){
            [self showLoading];
            //[[SKPaymentQueue defaultQueue] addTransactionObserver:self];
            //[[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
            [[MKStoreManager sharedManager] restorePreviousTransactionsOnComplete:^(void){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information" message:@"Your previous purchases have been restored" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                [self hideLoading];
                [_carousel reloadData];
                if(IsIpad)
                    [self.delegate featureWasPurchasedWithIndex:0 isRestored:YES];
                else
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"ShoppingCardWasPurchased" object:nil];
                
            } onError:^(NSError *error) {
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"There was an error when restoring features." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                [self hideLoading];
            }];
        }
    }
}

-(void)buyFeatureTap{
    
    if([Reachability reachabilityForInternetConnection].currentReachabilityStatus != NotReachable){
        if(![MKStoreManager isFeaturePurchased:[[PBSettings instance] app:[arrayWithPurchases objectAtIndex:selectedFeature]]]){
            [self showLoading];
            NSLog(@"%@",[[PBSettings instance] app:kPB_PurchaseProductIdEmailRecipe]);
            [[MKStoreManager sharedManager] buyFeature:[[PBSettings instance] app:[arrayWithPurchases objectAtIndex:selectedFeature]]
             
                                            onComplete:^(NSString* purchasedFeature, NSData *sendDate, NSArray* arr){
                                                [_carousel reloadData];
                                                [_buyButton setTitle:@"Purchased" forState:UIControlStateNormal];
                                                [_buyButton setEnabled:NO];
                                                [self hideLoading];
                                                if(IsIpad)
                                                    [self.delegate featureWasPurchasedWithIndex:selectedFeature isRestored:NO];
                                                else
                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"ShoppingCardWasPurchased" object:nil];
                                                
                                            }
                                           onCancelled:^{
                                               [self hideLoading];
                                           }];
        }
    }
}

- (IBAction)buyButtonClick:(id)sender {
    if([Reachability reachabilityForInternetConnection].currentReachabilityStatus != NotReachable){
        if(![MKStoreManager isFeaturePurchased:[[PBSettings instance] app:[arrayWithPurchases objectAtIndex:selectedFeature]]]){
            [self showLoading];
            [[MKStoreManager sharedManager] buyFeature:[[PBSettings instance] app:[arrayWithPurchases objectAtIndex:selectedFeature]]
             
                                            onComplete:^(NSString* purchasedFeature, NSData *sendDate, NSArray* arr){
                                                [_carousel reloadData];
                                                [_buyButton setTitle:@"Purchased" forState:UIControlStateNormal];
                                                [_buyButton setEnabled:NO];
                                                [self hideLoading];
                                                if(IsIpad)
                                                    [self.delegate featureWasPurchasedWithIndex:selectedFeature isRestored:NO];
                                                else
                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"ShoppingCardWasPurchased" object:nil];
                                            }
                                           onCancelled:^{
                                               [self hideLoading];
                                           }];
        }
    }
}

- (IBAction)closeButtonClick:(id)sender {
    if(IsIpad)
        [self.delegate storeViewControllerDidClosed];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark In-App purchase methods

-(BOOL)isFeaturePurchasedWithIndex:(NSInteger)index{
    switch (index) {
        case 0:
                return isEmailRecipePurchased;
            break;
        case 1:
                return isShoppingCardPurchased;
            break;
        default:
            break;
    }
    
    return NO;
}
@end
